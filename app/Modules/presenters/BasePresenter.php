<?php

namespace App\Presenters;

use \Nette,
	\Nette\Forms\Controls,
	\App\Models;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	/**
	 * @inject
	 * @var Models\RememberMe
	 */
	public $rememberMe;


	public function startup()
	{
		parent::startup();

		$this->user->getStorage()->setNamespace('FrontModule');

		try {
			if(!$this->user->isLoggedIn())
			{
				// Check if remember me is present
				if ($data = $this->rememberMe->auth())
				{
					$this->user->login($data['user']);
				}
			}
		} catch (\Exception $e) {
			//dump("#Error  %s\n", $e->getMessage());
		}


	}


	/**
	 * @param string
	 * @return Nette\ComponentModel\IComponent
	 */
	protected function createComponent($name)
	{
		$component = parent::createComponent($name);
		if ($component instanceof Nette\Application\UI\Form) {
			$this->bootstrapize($component);
		}
		return $component;
	}

    public function beforeRender()
    {
        parent::beforeRender();
		
		$this->template->version = $this->context->parameters['site']['version'];

        if ($this->user->getId() != null)
        {
            $uinfo = $this->profile->find()->where('id', $this->user->getId())->fetch();
			if ($uinfo)
			{
				$this->template->email = $uinfo['login'];


				if ($fainfo = $uinfo->related('invoice_detail')->fetch())
				{
					$this->template->meno = $fainfo['meno'] . ' ' . $fainfo['priezvisko'];
					//$this->template->vsymbol = $fainfo['variabilny_symbol'];
				}

				if ($tpinfo = $uinfo->related('training_profile')->fetch())
				{
					$this->template->premium = ($tpinfo['premium'] != NULL && $tpinfo['premium']) ? true : false;
					$this->template->pocet_skilliers = $tpinfo['pocet_kreditov'];
					$this->template->platnost_skilliers = $tpinfo['platnost_kreditov'];//->format('d.m.Y');
				}
				
				if ($stinfo = $uinfo->related('user_season_ticket')->fetch())
				{
					$this->template->season_ticket = $stinfo;
				}

			}
        }

    }

	/**
	 * Bootstrapizes the form.
	 * @param Nette\Application\UI\Form
	 * @return void
	 */
	private function bootstrapize(Nette\Application\UI\Form $form)
	{
		// setup form rendering
		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['control']['container'] = 'div class=col-sm-9';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		// make form and controls compatible with Twitter Bootstrap
		
		if($form->getElementPrototype()->getAttribute('class') === null)
			$form->getElementPrototype()->class('form-horizontal');
		// foreach ($form->getControls() as $control) {
		// 	if ($control instanceof Controls\Button) {
		// 		$control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
		// 		$usedPrimary = TRUE;
		// 	} elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox) {
		// 		$control->getControlPrototype()->addClass('form-control');
		// 	} elseif ($control instanceof Controls\Checkbox) {
		// 		$control->getControlPrototype()->addClass('toggle');
		// 		//$control->getSeparatorPrototype()->setName('div')->addClass('checkbox check-transparent');

		// 	} elseif ($control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
		// 		$control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
		// 	}
		// }
		$form->onRender[] = function ($form) {
			foreach ($form->getControls() as $control) {
				$type = $control->getOption('type');
				if ($type === 'button') {
					$control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
					$usedPrimary = TRUE;
				} elseif (in_array($type, ['text', 'textarea', 'select'], TRUE)) {
					$control->getControlPrototype()->addClass('form-control');
				} elseif (in_array($type, ['checkbox'], TRUE)) {
					$control->getControlPrototype()->addClass('toggle');
				} elseif ($control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
					$control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
				}
			}
		};
		//$form->setRenderer($renderer);
	}

}
