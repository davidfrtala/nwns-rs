<?php

namespace App\UserModule\Presenters;

use Nette,
    App\Models;

/**
 * Default presenter.
 */
class DefaultPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Treningy
     */
    public $treningy;

    public function actionDefault()
    {
        $this->redirect(":User:Profile:default");
    }
}
