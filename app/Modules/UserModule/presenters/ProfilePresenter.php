<?php

namespace App\UserModule\Presenters;

use Nette,
    Nette\Application\UI\Form,
    App\Models;


/**
 * Default presenter.
 */
class ProfilePresenter extends BasePresenter
{

    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->todays_training = $this->profile->getTodaysTrainingData($this->profile->getId());
        $this->template->next_trainings = $this->profile->getNextTrainingsData($this->profile->getId());

        $donutData = $this->profile->getDonutData($this->profile->getId());
        $this->template->training_colors = $donutData['colors'];
        $this->template->chart_data = $donutData['chart'];
        $this->template->training_stats = $donutData['stats'];

        $data = $this->profile->db()->table('view_user')->where("id", $this->profile->getId())->fetch();
        $this->template->data = $data;
        $this->template->title = "Profil";
    }

    public function createComponentEditProfileForm($name)
    {
        $form = new \Components\Forms\EditProfile($this, $name);

        $defaults = $this->profile->db()->fetch('select * from view_user where id = ?', $this->user->getId());
        $form->setDefaults($defaults);

        $form->onSuccess[] = [$this, 'formEditProfileSubmitted'];

        return $form;
    }
    public function formEditProfileSubmitted(Form $form)
    {
        $values = $form->getValues();
        try {
            $this->profile->update($values);

            $this->flashMessage('Údaje boli úspešne upravené!', "success");

            $this->redirect('default');
        } catch (\Exception $e) {
            $code = $e->getCode();
            if (!empty($code)) {
                $this->flashMessage("Nepodarilo sa vykonať zmenu. Opakujte neskôr prosím! ".$e->getMessage(), "danger");
            } else {
                throw $e;
            }
        }
    }


    public function createComponentChangePasswordForm()
    {
        $form = new \Components\Forms\ChangePassword($this, 'changePasswordForm');
        $form->onSuccess[] = [$this, 'formChangePasswordSubmitted'];

        return $form;
    }
    public function formChangePasswordSubmitted(Form $form)
    {
        $values = $form->getValues();
        try {
            $this->profile->changePassword($values);

            $this->flashMessage('Údaje boli úspešne upravené!', "success");

        } catch (\Exception $e) {
            $code = $e->getCode();
            if (!empty($code)) {
                $this->flashMessage("Nepodarilo sa vykonať zmenu. Opakujte neskôr prosím! ".$e->getMessage(), "danger");
            }
        }
    }

}
