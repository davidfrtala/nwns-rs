<?php

namespace App\UserModule\Presenters;

use \Nette,
	\App\Models;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \App\Presenters\BasePresenter
{
    /**
     * @inject
     * @var \App\Models\Profile
     */
    public $profile;

    public function startup()
    {
        parent::startup();

        if(!$this->user->isLoggedIn()) {
            $this->redirect(":Front:Authenticate:default");
        }
    }
}
