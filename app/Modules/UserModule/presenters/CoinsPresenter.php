<?php

namespace App\UserModule\Presenters;

use Nette,
	Nette\Application\UI\Form,
	App\Models;


/**
 * Default presenter.
 */
class CoinsPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Kredity
     */
    public $kredity;

    /**
     * @inject
     * @var Models\Orders
     */
    public $orders;

    /**
     * @inject
     * @var Models\PayPal
     */
    public $paypal;

    /**
     * @inject
     * @var Models\Transakcie
     */
    public $transakcie;
	
	/**
     * @inject
     * @var Models\UserSeasonTicket
     */
    public $user_season_ticket;

    /**
     * @inject
     * @var Models\TrainingProfile
     */
    public $training_profile;
	
	/**
     * @inject
     * @var Models\SendgridEmail
     */
    public $sendgrid_email;
    
    public function actionPaypalProcess($token, $payerid)
    {
        //Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
        if(isset($_GET["token"]) && isset($_GET["PayerID"]))
        {
            //we will be using these two variables to execute the "DoExpressCheckoutPayment"
            //Note: we haven't received any payment yet.
            $token = $_GET["token"];
            $payer_id = $_GET["PayerID"];
            $ItemName           = $_SESSION['itemName']; //Item Name
            $ItemPrice          = $_SESSION['itemprice']; //Item Price
            $ItemNumber         = $_SESSION['itemNo']; //Item Number
            $ItemQty            = $_SESSION['itemQTY']; // Item Quantity
            $paypal_query = $this->paypal->getDataQueryExecute($ItemNumber, $ItemName, $ItemPrice, $ItemPrice, $ItemQty,$token, $payer_id);

            // odpoved paypalu
            $paypal_response = $this->paypal->expressCheckout(Models\PayPal::METHOD_PAY, $paypal_query);
            // v pripade uspechu redirectnem na stránky paypalu
            if("SUCCESS" == strtoupper($paypal_response["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($paypal_response["ACK"])) {

                    if('Completed' == $paypal_response["PAYMENTINFO_0_PAYMENTSTATUS"])
                    {
                        $this->flashMessage('Platba prebehla úspešne', "success");
                    }
                    elseif('Pending' == $paypal_response["PAYMENTINFO_0_PAYMENTSTATUS"])
                    {
                        $this->flashMessage('Transakcia bola dokončená, ale platba ešte neprebehla. Musíte ju manuálne autorizovať na vašom paypal účte.', "success");
                    }

                    $this->orders->confirmOrder($_SESSION['order_id']);
                    $values = array('target_id' => $_SESSION['itemNo'], 'transaction_type_id' => 3, 'user_id' => $this->profile->getId());
                    $this->transakcie->saveData($values);
					
					// $kredity = $this->kredity->find()->wherePrimary($ItemNumber)->fetch();
					
					// //ak ide o permanentku, predlzim jej platnost
					// if ($kredity->season_ticket) {
						// $this->user_season_ticket->prolongDuration($this->profile->getId(), $kredity->season_ticket_duration);
					// } else { //inak pridam kredity
						// $this->training_profile->addKredity($this->profile->getId(), $this->kredity->find()->wherePrimary($ItemNumber)->fetch()->kredity);
					// }
					
					// $this->training_profile->prolongKredityExpiration($this->profile->getId());
					
            } else {
                // v pripade neuspechu zobrazim chybu
                throw new \Exception("Zlyhala komunikácia s PayPal serverom. Opakujte neskôr prosím!");
            }
            $this->redirect('default');
        } else {
            throw new \Exception("Chýba token");
        }
    }
    
    public function renderDefault()
    {
        $this->template->title = "Nákup";
    }

    public function actionPaypalError()
    {
        // v pripade neuspechu zobrazim chybu
        $this->flashMessage('Platbu sa nepodarilo realizovať! Opakujte neskôr prosím.', "danger");

        $this->redirect('default');
    }

    public function createComponentForm()
    {
    	$form = new Form;

    	$form->addSelect('produkt', 'Chcem si kúpiť', $this->kredity->fetchSkilliers($this->training_profile->isPremiumProfile($this->profile->getId()), $this->training_profile->isDiscountProfile($this->profile->getId())))
    			->setRequired('Produkt je povinný!');

    	$options = array(
		    'paypal' => 'Platba cez PayPal (platobnou kartou)',
		    'prevod' => 'Platba prevodom'
		);
		$form->addRadioList('uhrada', 'Forma úhrady', $options)
    			->setRequired('Forma úhrady je povinná!')
                ->setDefaultValue('paypal');

        $form->addCheckbox('obchodne_podmienky')
                ->setRequired('Musíte súhlasiť s obchodnými podmienkami');

        $form->addSubmit('submit', 'Potvrď');

        $form->onSuccess[] = [$this, 'formSubmitted'];

        return $form;
    }
    public function formSubmitted(Form $form)
    {
        $values = $form->getValues();
        try {
            $this->orders->db()->beginTransaction();

            $user = $this->profile->getByUserId();

            // ulozime platbu
            $order = $this->orders->checkout($user->id, $values->uhrada, $values->produkt);
            if ($values->uhrada == 'prevod')
            {
                $user_detail = $this->profile->getInvoiceDetail()->fetch();
                $this->flashMessage('Tvoja objednávka bola zaevidovaná pod variabilným symbolom '.$order->variabilny_symbol.' a bude spracovaná až po jej uhradení.', "success");


                // odoslat email
                $subject = "[NWNS Academy] Tvoja objednávka v NWNSA";

                $body =  'Ahoj '.$user_detail->meno.'<br />
                            Tvoja objednávka bola zaevidovaná v našom systéme. <br /><br /> Momentálne sú u nás zaevidované tieto tvoje objednávky: <br /><br />';

                // vyber objednavky
                $user_orders = $this->orders->find()->where(array('user_id' => $this->user->getId(), 'date_confirmed' => null))->order('date_order DESC');
                foreach ($user_orders as $row)
                {
                    $kredity = $row->ref('credits');
                    $body .= 'Dátum: '.$row->date_order->format('d.m.Y H:i:s').'<br/>';
                    $body .= 'Produkt: '.$kredity->nazov.'<br/> ';
                    $body .= 'VS: '.$row->variabilny_symbol.'<br/> ';
                    $body .= 'Číslo účtu: 2948026992 / 1100 <br/> ';
					$body .= 'IBAN: SK45 1100 0000 0029 4802 6992<br/>';
                    $body .= 'Suma: ' . $row->price.' EUR<br /><br />';
                }
                $body .= 'Skilliere a permanentky budú aktivované až po úspešnom prevedení peňazí na účet.<br /><br /> S pozdravom, No Will No Skill Academy' ;

				$this->sendgrid_email->sendEmail($user->login, ['name' => 'NWNS Academy', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
				
                $this->orders->db()->commit();

            } elseif ($values->uhrada == 'paypal') {
                // query ktora sa odosle na paypal
                $produkt = $this->orders->getKredity()->find()->where('id', $values->produkt)->fetch();
                // URL cesty
                $this->paypal->return_url = $this->link('//Coins:PaypalProcess');
                $this->paypal->cancel_url = $this->link('//Coins:PaypalError');

                $paypal_query = $this->paypal->getDataQuery($produkt->id, $produkt->nazov, $order->price, $order->price, 1);

                // odpoved paypalu
                $paypal_response = $this->paypal->expressCheckout(Models\PayPal::METHOD_SET, $paypal_query);
                // v pripade uspechu redirectnem na stránky paypalu
                if("SUCCESS" == strtoupper($paypal_response["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($paypal_response["ACK"])) {
                    // ak sa uspesne spojim, potrebujem si uchovat hodnoty v SESSION
                    $_SESSION['order_id'] = $order['id'];
                    $_SESSION['itemprice']   = $order->price;
                    $_SESSION['totalamount'] = $order->price;
                    $_SESSION['itemName']    = $produkt->nazov;
                    $_SESSION['itemNo']      = $produkt->id;
                    $_SESSION['itemQTY']     = 1;

                    // testovaci rezim alebo ostry
                    if($this->paypal->mode =='sandbox') { $paypalmode = '.sandbox'; } else { $paypalmode = ''; }

                    // presmerujem uzivatela na paypal stranku
                    $paypalurl = 'https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$paypal_response["TOKEN"].'';
                    header('Location: '.$paypalurl);
                    $this->orders->db()->commit();
                    return;
                } else {
                    // v pripade neuspechu zobrazim chybu
                    throw new \Exception("Zlyhala komunikácia s PayPal serverom. Opakujte neskôr prosím!");
                }
            }


        } catch (\PDOException $e) {
            $this->orders->db()->rollback();
            $this->flashMessage("Nastali problémy s databázovým pripojením! Opakujte neskôr prosím!", "danger");
        } catch (\Exception $e) {
            $this->orders->db()->rollback();
            $this->flashMessage($e->getMessage(), "danger");
        }
        $this->redirect('default');
    }


}
