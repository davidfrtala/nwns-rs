<?php

namespace App\UserModule\Presenters;

use \Nette,
    \Nette\Application\UI,
    \App\Models;


/**
 * Default presenter.
 */
class TransactionsPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Transakcie
     */
    public $transakcie;
    
    public function renderDefault()
    {
        $data = $this->transakcie->getDataByUser($this->user->getId());
        $this->template->data = $data;

        $this->template->chart_data = $this->transakcie->getChartData($this->profile->getId());
        $this->template->title = "Transakcie";
    }
}
