<?php

namespace App\AdminModule\Presenters;

use Components\Forms,
    Components\Grids,
    App\Models,
    Nette,
    Nette\Application\UI,
    Tracy\Debugger;


class DashboardPresenter extends BasePresenter
{
	
	/**
     * @inject
     * @var Models\Profile
     */
    public $profile;

    public function createComponentGrid($name)
    {
        return new Grids\Orders($this->model, $this, $name);
    }

    public function renderDefault()
    {	
		$this->template->sumOfUnconfirmedOrders = $this->profile->db()->fetchAll("
			SELECT SUM(o.price) as cena FROM orders o WHERE o.date_confirmed IS NULL AND o.platba = 'prevod'
		");
		
		$this->template->lastMonthSc = $this->profile->db()->fetchAll("
			SELECT SUM(o.price) AS suma
			FROM orders o
			WHERE o.date_confirmed IS NOT NULL AND YEAR(o.date_confirmed) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
			AND MONTH(o.date_confirmed) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)
		");
		
		$this->template->thisMonthSc = $this->profile->db()->fetchAll("
			SELECT SUM(o.price) AS suma
			FROM orders o
			WHERE o.date_confirmed IS NOT NULL AND YEAR(o.date_confirmed) = YEAR(CURRENT_DATE)
			AND MONTH(o.date_confirmed) = MONTH(CURRENT_DATE)
		");
		
		$this->template->vipMemberCount = $this->profile->db()->fetchAll("
			SELECT COUNT(*) AS count
			FROM user_season_tickets st
			WHERE st.valid_until >= NOW()
		");
    }
	
	public function createComponentNewUsersGrid($name)
    {
        $model = $this->profile->db()->fetchAll("
            select
                concat(u.priezvisko,' ',u.meno) full_name,
                u.*
            from view_user u
			WHERE date_created BETWEEN (CURRENT_DATE() - INTERVAL 1 MONTH) AND (NOW())
            order by date_created desc
            ");

        return new Grids\NewUsers($model, $this, $name);
    }
}
