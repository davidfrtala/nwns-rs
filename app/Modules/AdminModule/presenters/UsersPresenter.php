<?php

namespace App\AdminModule\Presenters;

use Components\UserForm;
use Components\Forms\EditProfile;
use Components\Grids;
use Nette,
 Nette\Application\UI,
        App\Models;
use Tracy\Debugger;


/**
 * Default presenter.
 */
class UsersPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Profile
     */
    public $profile;

    /**
     * @inject
     * @var Models\Transakcie
     */
    public $transakcie;

    /**
     * @inject
     * @var Models\Orders
     */
    public $orders;
	
	/**
     * @inject
     * @var Models\TrainingProfile
     */
    public $training_profile;

    public function beforeRender()
    {
        parent::beforeRender();

        $id = $this->getParameter('id');
        if (!empty($id))
        {
            // donut data
            $donutData = $this->profile->getDonutData($id);
            $this->template->training_colors = $donutData['colors'];
            $this->template->donut_data = $donutData['chart'];
            $this->template->training_stats = $donutData['stats'];

            // transakcia chart data
            $this->template->chart_data = $this->transakcie->getChartData($id);


            $data = $this->profile->db()->table('view_user')->where('id = ?', $id)->fetch();
            $this->template->data = $data;
        }
    }

    public function actionShow($id)
    {

    }

    public function actionEdit($id)
    {
        $defaults = $this->profile->db()->fetch('select * from view_user where id = ?', $id);
        $form = $this->getComponent('editProfileForm');
        $form->setDefaults($defaults);

        $this->setView('form');
    }



    public function createComponentEditProfileForm()
    {
        $form = new EditProfile($this, 'editProfileForm', $this->profile);
        $form->onSuccess[] = [$this, 'formEditProfileSubmitted'];
        return $form;
    }



    public function createComponentChangePasswordForm()
    {
        $form = new \Components\Forms\ChangePassword($this, 'changePasswordForm');
        $form->onSuccess[] = [$this, 'formChangePasswordSubmitted'];
        return $form;
    }



    public function createComponentGrid($name)
    {
        $model = $this->profile->db()->fetchAll("
            select
                concat(`u`.`meno`,' ',`u`.`priezvisko`) full_name,
                u.*
            from view_user u
            order by date_created desc
            ");

        return new Grids\Users($model, $this, $name);
    }




    public function createComponentTransakcieGrid($name)
    {
        $id = $this->getParameter('id');
        $grid = new Grids\Transakcie($this->transakcie, $this, $name, $id);
        $grid->setDefaultPerPage('10');
        return $grid;
    }



    public function createComponentOrdersGrid($name)
    {
        $id = $this->getParameter('id');
        $grid = new Grids\Orders($this->orders, $this, $name, $id);
        $grid->setDefaultPerPage('10');
        return $grid;
    }

    /**
     * @param $form
     * @throws \Exception
     */
    public function formEditProfileSubmitted($form)
    {
        $values = $form->getValues();
        try {
            $id = $this->getParameter('id');
            $this->profile->update($values, $id);

            $this->flashMessage('Údaje boli úspešne upravené!', "success");

            $this->redirect('show', $id);
        } catch (\Exception $e) {
            $code = $e->getCode();
            if (!empty($code)) {
                $this->flashMessage("Nepodarilo sa vykonať zmenu. Opakujte neskôr prosím! ".$e->getMessage(), "danger");
            } else {
                throw $e;
            }
        }
    }



    /**
     * @param $form
     */
    public function formChangePasswordSubmitted($form)
    {
        $values = $form->getValues();
        try {
            $id = $this->getParameter('id');
            $this->profile->changePassword($values, $id);

            $this->flashMessage('Údaje boli úspešne upravené!', "success");

        } catch (\Exception $e) {
            $code = $e->getCode();
            if (!empty($code)) {
                $this->flashMessage("Nepodarilo sa vykonať zmenu. Opakujte neskôr prosím! ".$e->getMessage(), "danger");
            }
        }
    }


    /**
     * @param integer $userId
     */
    public function handleActivate($id)
    {
        try {
            $this->profile->db()->beginTransaction();

            // vytiahneme si uzivatela z db (akceptujeme len tych s neaktivnym kontom)
            $user = $this->profile->find()->where(array(
                'id' => $id,
                'aktivny' => 0,
            ))->fetch();

            if ($user == false) {
                throw new \Exception("Aktivácia nieje možná! Užívateľ nebol nájdený alebo je konto už aktívne!");
            }

            // ak sme dosli az sem, mozme spokojne aktivovat konto
            $user->update(array(
                'aktivny' => 1
            ));

            $this->profile->db()->commit();

            $this->flashMessage("Užívateľské konto '".$user->login."' bolo aktivované.", "success");
        } catch (\Exception $e) {
            dlog($e);
            $this->profile->db()->rollback();
            $this->flashMessage($e->getMessage(), 'danger');
        }

        $this->redirect('show', $id);
    }

    /**
     * @param integer $userId
     */
    public function handleDeactivate($id)
    {
        try {
            $this->profile->db()->beginTransaction();

            // vytiahneme si uzivatela z db (akceptujeme len tych s neaktivnym kontom)
            $user = $this->profile->find()->where(array(
                'id' => $id,
                'aktivny' => 1,
            ))->fetch();

            if ($user == false) {
                throw new \Exception("Deaktivácia nieje možná! Užívateľ nebol nájdený alebo je konto už neaktívne!");
            }

            // ak sme dosli az sem, mozme spokojne aktivovat konto
            $user->update(array(
                'aktivny' => 0
            ));

            $this->profile->db()->commit();
            $this->flashMessage("Užívateľské konto '".$user->login."' bolo deaktivované.", "success");

        } catch (\Exception $e) {
            dlog($e);
            $this->profile->db()->rollback();
            $this->flashMessage($e->getMessage(), 'danger');
        }

        $this->redirect('show', $id);
    }
	
	/**
     * @param integer $userId
     */
    public function handleToggleDiscount($id)
    {
        try {
            $this->training_profile->db()->beginTransaction();

            // vytiahneme si uzivatela z db
            $user = $this->training_profile->find()->where(array(
                'user_id' => $id
            ))->fetch();

            if ($user == false) {
                throw new \Exception("Užívateľ nebol nájdený!");
            }
			
            // ak sme dosli az sem, zmenime stav
            $user->update(array(
                'discount' => $user->discount == 0 ? 1 : 0
            ));

            $this->training_profile->db()->commit();

            $this->flashMessage("Užívateľské konto bolo upravené.", "success");
        } catch (\Exception $e) {
            dlog($e);
            $this->training_profile->db()->rollback();
            $this->flashMessage($e->getMessage(), 'danger');
        }

        $this->redirect('show', $id);
    }
}
