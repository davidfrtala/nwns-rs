<?php

namespace App\AdminModule\Presenters;

use Nette,
 	Nette\Application\UI,
	Tracy\Debugger,
	App\Models;


/**
 * EmailParser presenter.
 * Sluzi na parsovanie b-mail vypisov z tatrabanky a automatizovanie schvalovania objednavok
 */
class EmailParserPresenter extends OrdersPresenter
{
	/**
	 *
	 * @var string
	 */
	private $simple_access_token = "kj7jsLy12";


	public function startup()
	{
		// zavolam dedkov startup (nie rodicovsky!)
		\App\Presenters\BasePresenter::startup();
	}



	public function actionProcess($access_token)
	{
		try {
			if ($access_token != $this->simple_access_token)
			{
				throw new \Exception("Neplatny access token! Pokus o pristup k tejto adrese mimo cron službu??", 1);
			}

			$host = "{mail.inetadmin.eu/ssl}INBOX";
			$processed_box = "INBOX/processed";


			$mbox = imap_open($host, "vypisy@nowillnoskillacademy.sk", "rsnwnsavypis");


			/* grab emails */
			$emails = imap_search($mbox,'SUBJECT "Kredit na ucte" ');

			/* if emails are returned, cycle through each... */
			if($emails) {

				/* put the newest emails on top */
				rsort($emails);

				/* for every email... */
				foreach($emails as $email_number) {
					/* get information specific to this email */
					$overview = imap_fetch_overview($mbox,$email_number,0);
					$message = imap_body($mbox,$email_number);

					$exp = explode("\n",$message);
					if (strpos($exp[8], 'Referencia platitela') === false)
					{
						continue;
					}


					$res = $this->model->db()->fetch("
						SELECT o.*
						FROM orders o
						where
							date_confirmed is null and platba = 'prevod'
							and variabilny_symbol = ?
							and date_order < ?
						limit 1
						", $this->parseVS($exp[8]), $this->parseTime($exp[2])->format('Y-m-d H:i:s'));

					if ($res !== false)
					{
						try {
							$this->handleConfirm($res['id']);
						} catch (Nette\Application\AbortException $e) {
							// handleConfirm hádže redirect, takze tu musime ten abort iba odchytit a odignorovat
						}
					}


					imap_mail_move($mbox,$email_number,$processed_box);

				}

			}

		} catch (\Exception $e) {
			Debugger::log($e);
		}

		imap_expunge($mbox);
		imap_close($mbox);
		exit();
	}



	private function parseTime($string)
	{
		$cas = substr($string, 0, strpos($string, " bol zostatok"));
		$date = \DateTime::createFromFormat("d.n.Y H:i", $cas);
		return $date;
	}

	private function parseMoney($string)
	{
		$e = explode(" zvyseny o ", $string);
		return (integer) substr($e[1], 0, -5);
	}

	private function parseVS($string)
	{
		$e = explode('/', $string);
		return (integer) substr($e[1], 2);
	}
}
