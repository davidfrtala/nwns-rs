<?php

namespace App\AdminModule\Presenters;

use Components\Forms,
    Components\Grids,
    App\Models,
    Nette,
    Nette\Application\UI,
    Tracy\Debugger,
    DateTime;


class TicketsPresenter extends BasePresenter
{

    /**
     * @inject
     * @var Models\SeasonTicket
     */
    public $model;


    /**
     * Screen admin/config/add
     */
    public function actionAdd()
    {
        $form = $this->getComponent('form');
        $form->onSuccess[] = [$this, 'addHandler'];

        $this->setView('form');
    }



    /**
     * Screen admin/config/edit/[id]
     *
     * @param $id integer
     */
    public function actionEdit($id)
    {
        $default = $this->model->find()->get($id);
        $form = $this->getComponent('form');
        $form->setDefaults($default);
        $form->onSuccess[] = [$this, 'editHandler'];

        $this->setView('form');
    }




    public function createComponentGrid($name)
    {
        $model = $this->model->find()->order('id DESC');

        return new Grids\SeasonTickets($model, $this, $name);
    }



    public function createComponentForm()
    {
        $control = new Forms\SeasonTickets($this, 'form', $this->model);
        return $control;
    }

    /*
     * Form handlers
     */


    public function editHandler($form)
    {

        $values = $form->getValues();
        $values = $this->fixEndDateTime($values);

        try {
            $this->model->db()->beginTransaction();

            $id = $this->getParameter('id');
            $this->model->find()->get($id)->update($values);

            $this->model->db()->commit();
            $this->flashMessage('Záznam bol úspešne zmenený', 'success');

            $this->redirect('default');

        } catch (\PDOException $e) {
            $this->model->db()->rollBack();
            Debugger::log($e);
            $this->flashMessage('Nepodarilo sa upraviť záznam! Opakujte neskôr prosím', 'danger');
        }
    }

    public function addHandler($form)
    {
        $values = $form->getValues();
        $values = $this->fixEndDateTime($values);

        try {
            $this->model->db()->beginTransaction();

            $this->model->table()->insert($values);

            $this->model->db()->commit();
            $this->flashMessage('Záznam bol pridaný', 'success');

            $this->redirect('default');
        } catch (\PDOException $e) {
            $this->model->db()->rollBack();
            Debugger::log($e);
            $this->flashMessage('Nepodarilo sa pridať záznam! Opakujte neskôr prosím', 'danger');
        }
    }

    private function fixEndDateTime($values) {
        if ($values['date_to'] == NULL || $values['date_to'] == '') {
            $values['date_to'] = NULL;
            return $values;
        }
        $values['date_to'] .= ' 23:59:59';
        $values['date_to'] = DateTime::createFromFormat('d.m.Y H:i:s', $values['date_to']);     

        return $values;
    }

}
