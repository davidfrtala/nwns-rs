<?php

namespace App\AdminModule\Presenters;

use Components\Forms,
    Components\Grids,
    App\Models,
    Nette,
    Nette\Application\UI,
    Tracy\Debugger;


class PacksPresenter extends BasePresenter
{

    /**
     * @inject
     * @var Models\Pack
     */
    public $model;
	
	/**
	 * @inject
	 * @var Models\Treningy
	 */
	public $treningy;

	/**
	 * @inject
	 * @var Models\TrainingProfile
	 */
	public $training_profile;


    /**
     * Screen admin/config/add
     */
    public function actionAdd()
    {
        $form = $this->getComponent('form');
        $form->onSuccess[] = [$this, 'addHandler'];

        $this->setView('form');
    }



    /**
     * Screen admin/config/edit/[id]
     *
     * @param $id integer
     */
    public function actionEdit($id)
    {
        $default = $this->model->find()->get($id);
        $form = $this->getComponent('form');
        $form->setDefaults($default);
        $form->onSuccess[] = [$this, 'editHandler'];

        $this->setView('form');
    }




    public function createComponentGrid($name)
    {
        $model = $this->model->find()->order('id DESC');

        return new Grids\Packs($model, $this, $name);
    }



    public function createComponentForm()
    {
        $control = new Forms\Packs($this, 'form', $this->model);
        return $control;
    }

    /*
     * Form handlers
     */


    public function editHandler($form)
    {

        $values = $form->getValues();
		if ($values->start_date == "") $values->start_date = NULL;

        try {
            $this->model->db()->beginTransaction();

            $id = $this->getParameter('id');
            $this->model->find()->get($id)->update($values);

            $this->model->db()->commit();
            $this->flashMessage('Záznam bol úspešne zmenený', 'success');

            $this->redirect('default');

        } catch (\PDOException $e) {
            $this->model->db()->rollBack();
            Debugger::log($e);
            $this->flashMessage('Nepodarilo sa upraviť záznam! Opakujte neskôr prosím', 'danger');
        }
    }

    public function addHandler($form)
    {
        $values = $form->getValues();
		if ($values->start_date == "") $values->start_date = NULL;

        try {
            $this->model->db()->beginTransaction();

            $this->model->table()->insert($values);

            $this->model->db()->commit();
            $this->flashMessage('Záznam bol pridaný', 'success');

            $this->redirect('default');
        } catch (\PDOException $e) {
            $this->model->db()->rollBack();
            Debugger::log($e);
            $this->flashMessage('Nepodarilo sa pridať záznam! Opakujte neskôr prosím', 'danger');
        }
    }
	
	public function createComponentKalendar()
	{
		$control = new \Components\PackKalendarControl($this->treningy, $this->training_profile);

		return $control;
	}

}
