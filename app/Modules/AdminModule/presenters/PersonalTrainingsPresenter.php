<?php

namespace App\AdminModule\Presenters;

use Components\Forms\OsobnyTrening;
use Components\Grids;
use Nette,
 Nette\Application\UI,
        App\Models;
use Tracy\Debugger;


/**
 * Default presenter.
 */
class PersonalTrainingsPresenter extends BasePresenter
{

    /**
     * @inject
     * @var Models\OsobneTreningy
     */
    public $model;

    public function actionAdd($id)
    {
        $form = $this->getComponent('form');
        $form->onSuccess[] = [$this, 'addTrainingHandler'];

        $this->setView('form');
    }

    public function actionEdit($id)
    {
        $default = $this->model->find()->get($id);
        $form = $this->getComponent('form');
        $form->setDefaults($default);
        $form['datum']->setDefaultValue($default['datum']->format('d.m.Y'));
        $form->onSuccess[] = [$this, 'editTrainingHandler'];

        $this->setView('form');
    }


    public function actionShow($id)
    {
        $data = $this->model->db()->table('view_user')->where('id = ?', $id)->fetch();

        $this->template->data = $data;
    }

    public function createComponentForm()
    {
        $control = new OsobnyTrening($this, 'form', $this->model);
        return $control;
    }

    public function createComponentGrid($name)
    {
        $model = $this->model->db()->fetchAll('select * from view_personal_training');

        return new Grids\OsobneTreningy($model, $this, $name);
    }

    public function addTrainingHandler($form)
    {
        $values = $form->getValues();

        try {
            $this->model->db()->beginTransaction();

            if (!$this->user->isInRole('Administrátor'))
            {
                $values['trener_profile_id'] = $this->user->getId();
            }
            $values['datum'] = date("Y-m-d", strtotime($values['datum']));
            $this->model->table()->insert($values);

            $this->model->db()->commit();
            $this->flashMessage('Záznam bol pridaný', 'success');
            $this->redirect('default');
        } catch (\PDOException $e) {
            Debugger::log($e);
            $this->model->db()->commit();
            $this->flashMessage('Nepodarilo sa pridať záznam! Opakujte neskôr prosím', 'danger');
        }
    }

    public function editTrainingHandler($form)
    {
        $values = $form->getValues();

        try {
            $this->model->db()->beginTransaction();
            $id = $this->getParameter('id');
            $training = $this->model->find()->get($id);

            if (!$this->user->isInRole('Administrátor') && $training['trener_profile_id'] != $this->user->getId())
            {
                throw new \PDOException("Užívatel s ID {$this->user->getID()} chce upravovat zaznam s id {$id}, na ktory nema prava!");
            }

            $values['datum'] = date("Y-m-d", strtotime($values['datum']));
            $training->update($values);

            $this->model->db()->commit();
            $this->flashMessage('Záznam bol úspešne zmenený', 'success');
            $this->redirect('default');
        } catch (\PDOException $e) {
            Debugger::log($e);
            $this->model->db()->commit();
            $this->flashMessage('Nepodarilo sa upraviť záznam! Opakujte neskôr prosím', 'danger');
        }
    }
}
