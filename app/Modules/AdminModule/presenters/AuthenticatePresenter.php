<?php

namespace App\AdminModule\Presenters;

use Components\LoginForm;
use Nette,
 Nette\Application\UI,
        App\Models;


class AuthenticatePresenter extends BasePresenter
{

    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('AdminModule');

    }

    public function beforeRender()
    {
        $this->template->version = $this->context->parameters['site']['version'];
        
        if($this->user->isLoggedIn() ) {
            $this->redirect("Dashboard:default");
        }
        $this->setLayout('auth_layout');
    }


    public function actionLogout()
    {
        $this->user->logout(true);
        $this->redirect('Authenticate:default');
    }

    public function createComponentLogin()
    {
        return new LoginForm($this, 'login');
    }
}