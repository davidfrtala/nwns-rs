<?php

namespace App\AdminModule\Presenters;

use Nette,
	Nette\Application\UI,
	App\Models;


/**
 * Treningy presenter.
 */
class TrainingsPresenter extends BasePresenter
{

	/**
	 * @inject
	 * @var Models\Treningy
	 */
	public $treningy;

	/**
	 * @inject
	 * @var Models\TrainingProfile
	 */
	public $training_profile;
	
	/**
	 * @inject
	 * @var Models\SeasonTicket
	 */
	public $user_season_ticket;
	
	/**
     * @inject
     * @var Models\SendgridEmail
     */
    public $sendgrid_email;

	/** @persistent */
	public $backlink;

	public function beforeRender() {
		parent::beforeRender();

		$this->backlink = $this->storeRequest();
	}

	public function actionDefault()
	{
		$actualWeek = new \DateTime($this['kalendar']->getActualWeek());
		$this->template->lastWeek = $actualWeek->sub(new \DateInterval('P1D'));
	}

	public function createComponentKalendar()
	{
		$control = new \Components\AdminKalendarControl($this->treningy, $this->training_profile);

		return $control;
	}


	public function actionRegisterTrainee($training_id)
	{
		$trening = $this->treningy->db()->fetch('select *from view_training where id = ?', $training_id);
		$this->template->training = $trening;
		$this->template->cvicenci = $this->treningy->getTrainees($training_id);

		$this->getComponent('traineeReservation')->setDefaults(array('id' => $training_id));
	}

	public function actionUnregisterTrainee($training_id, $user_id)
	{
		$trening = $this->treningy->db()->fetch('select id, nazov, termin_od from view_training where id = ?', $training_id);
		$trainee = $this->treningy->db()->fetch('select profile_id, meno, priezvisko from view_user where id = ?', $user_id);

		$this->template->trening = $trening;
		$this->template->trainee = $trainee;
	}


	public function handleConfirmUnregisterTrainee($training_id, $profile_id)
	{
		try {
			$this->training_profile->db()->beginTransaction();

			// selekty
			$tp = $this->training_profile->find()->wherePrimary($profile_id);

			$user_id = $tp->fetch()->user_id;
			$kredity = $this->treningy->find()->wherePrimary($training_id)->fetch()->kredity;

			$this->training_profile->db()->table('trainees')->where(array(
				'training_id' => $training_id,
				'training_profile_id' => $profile_id
			))->delete();

			$this->training_profile->addKredity($user_id, $kredity);

			$this->training_profile->getTransakcie()->addTransaction($user_id, $training_id, Models\Transakcie::TRENING_ODHLASENIE_TID);

			$this->training_profile->db()->commit();
			$this->flashMessage('Cvičenec bol úspešne odhlásený z tréningu!', 'success');
			$this->redirect('default');
		} catch (\PDOException $e) {
			$this->training_profile->db()->rollBack();
			Debugger::log($e);
			$this->flashMessage('Nepodarilo sa vykonať akciu! Opakujte neskôr prosím', 'danger');
		}

	}
	
	public function actionAddPenalty($training_id, $user_id) {
		$trening = $this->treningy->db()->fetch('select id, nazov, termin_od from view_training where id = ?', $training_id);
		$trainee = $this->treningy->db()->fetch('select id, profile_id, meno, priezvisko from view_user where id = ?', $user_id);
		$season_ticket = $this->user_season_ticket->db()->fetch('select penalties, valid_until from user_season_tickets where user_id = ?', $user_id);

		$this->template->trening = $trening;
		$this->template->trainee = $trainee;
		$this->template->season_ticket = $season_ticket;
	}

	public function handleConfirmAddPenalty($training_id, $user_id)
	{
		try {
			$this->user_season_ticket->db()->beginTransaction();
			
			$ticket = $this->user_season_ticket->addPenalty($user_id, $training_id);
			$this->training_profile->getTransakcie()->addTransaction($user_id, $training_id, Models\Transakcie::PRIDANIE_CERVENEHO_BODU);
			
			$this->user_season_ticket->db()->commit();
			
			$trainee = $this->treningy->db()->fetch('select login, meno, priezvisko, season_ticket_penalties from view_user where id = ?', $user_id);
			
			$subject = '[NWNS Academy] Dostal si červený bod';
			$penaltyText = "prvý";
			if ($ticket->penalties == 2) $penaltyText = "druhý";
			if ($ticket->penalties == 3) $penaltyText = "tretí";
			
			$body = '
				<p>Ahoj '.$trainee->meno . " " . $trainee->priezvisko . '</p>
				<p>Práve ti bol udelený ' . $penaltyText . ' červený bod za to, že si bol prihlásený na tréning a neprišiel si naň.</p>
			';
			
			if ($ticket->penalties < 3) {
				$body .= '
					<p>V prípade že dosiahneš za aktuálne obdobie trvania permanentky (do ' . $ticket->valid_until->format('d.m.Y') . ') 3 červené body, tvoja permanentka bude zrušená.</p>
					<p>Prosím, na tréning sa prihlasuj iba ak naň skutočne prídeš, alebo sa z neho nezabudni včas (do 15 min. pred jeho konaním) odhlásiť.</p>
				';
			} else {
				$body .= '
					<p>Keďže si dosiahol tretí červený bod za obdobie trvania tvojej aktuálnej permanentky, bola ti zrušená. Ak sa chceš znova neobmedzene prihlasovať na tréningy, musíš si zakúpiť novú.</p>
				';
			}
			$body .= '
				<p>Ďakujeme za pochopenie<br />Team NWNS Academy</p>
			';
			
			$this->sendgrid_email->sendEmail($trainee->login, ['name' => 'NWNS RS', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
			
			$this->flashMessage('Červený bod bol úspešne pridaný!', 'success');
			$this->redirect('default');
		} catch (\PDOException $e) {
			$this->user_season_ticket->db()->rollBack();
			Debugger::log($e);
			$this->flashMessage('Nepodarilo sa vykonať akciu! Opakujte neskôr prosím', 'danger');
		}

	}

	public function handleRenewSchedule($week)
	{

		try {
			// zacneme bezpecnu transakciu
			$this->treningy->db()->beginTransaction();

			// prekopirujeme rozvrh
			$this->treningy->renewSchedule(new \DateTime($week));

			// flash message
			$this->flashMessage('Tréningový rozvrh bol úspešne prekopírovaný!', 'success');

			// commitneme transakciu
			$this->treningy->db()->commit();
		} catch (\Exception $e) {
			// zastavime transakciu
			$this->treningy->db()->rollBack();
			$this->flashMessage('Nepodarilo sa vykonať akciu! Opakujte neskôr prosím', 'danger');
			Debugger::log($e);
		}
	}
}
