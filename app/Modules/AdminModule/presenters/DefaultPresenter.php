<?php

namespace App\AdminModule\Presenters;

use Nette,
 Nette\Application\UI,
        App\Models;


/**
 * Default presenter.
 */
class DefaultPresenter extends BasePresenter
{

	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}




}
