<?php

namespace App\AdminModule\Presenters;

use Nette,
	App\Models;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \App\Presenters\BasePresenter
{
	public function startup() {

		// zavolam dedkov startup (nie rodicovsky!)
		\Nette\Application\UI\Presenter::startup();

        $this->user->getStorage()->setNamespace('AdminModule');

        if(!$this->user->isInRole('Administrátor') && !$this->user->isInRole('Tréner') ) {
			$this->user->logout(true);
            $this->redirect("Authenticate:default");
        }
	}

	public function beforeRender()
	{
		$this->template->version = $this->context->parameters['site']['version'];
		
		$this->template->robots = "noindex,nofollow";
        $this->template->meno = ucfirst($this->user->getIdentity()->login);
	}
}
