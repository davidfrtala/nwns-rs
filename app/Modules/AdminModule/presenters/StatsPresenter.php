<?php

namespace App\AdminModule\Presenters;

use Components\Chart\Base;
use Nette,
    Nette\Application\UI,
    App\Models,
    DateTime;

/**
 * Stats presenter
 */
class StatsPresenter extends BasePresenter {
	
    /**
     * @inject
     * @var Models\Statistics
     */
    public $statistics_model;

    /**
     * @var string
     */
    public $date_from;

    /**
     * @var string
     */
    public $date_to;
    
    /**
     * @var string
     */
    public $date_from_orders;
    
    /**
     * @var string
     */
    public $date_to_orders;

    public function startup() {
        parent::startup();
    }
    
    public function renderDefault($date_from = NULL, $date_to = NULL, $date_from_orders = NULL, $date_to_orders = NULL) {
        //default is current month
        $this->date_from = $date_from ? $date_from : date('01.m.Y');
        $this->date_to = $date_to ? $date_to : date('d.m.Y');
        
        $this->date_from_orders = $date_from_orders ? $date_from_orders : date('01.m.Y');
        $this->date_to_orders = $date_to_orders ? $date_to_orders : date('d.m.Y');

        // get last week start day and last week end day
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last monday midnight", $previous_week);
        $end_week = strtotime("next sunday", $start_week);

        //template data
        $this->template->lastWeekStart = date("d.m.Y", $start_week);
        $this->template->lastWeekEnd = date("d.m.Y", $end_week);
        $this->template->currentDate = date("d.m.Y");
        $this->template->currentWeekStart = date("d.m.Y", strtotime('this week', time()));
        $this->template->currentMonthStart = date('01.m.Y');
        $date = new DateTime("first day of last month");
        $this->template->lastMonthStart = $date->format("d.m.Y");
        $date = new DateTime("last day of last month");
        $this->template->lastMonthEnd = $date->format("d.m.Y");

        $this->template->ordersCounts = $this->statistics_model->getOrdersCounts(new \DateTime($this->date_from), new \DateTime($this->date_to));
        $this->template->trainings = $this->statistics_model->getTrainings(new \DateTime($this->date_from), new \DateTime($this->date_to));
        $this->template->trainers = $this->statistics_model->getTrainers(new \DateTime($this->date_from), new \DateTime($this->date_to));
        $this->template->registeredUsersCount = $this->statistics_model->getRegisteredUsersCount(new \DateTime($this->date_from), new \DateTime($this->date_to));
        $this->template->confirmedUsersCount = $this->statistics_model->getConfirmedUsersCount(new \DateTime($this->date_from), new \DateTime($this->date_to));
        $this->template->convertedUsersCount = $this->statistics_model->getConvertedUsersCount(new \DateTime($this->date_from), new \DateTime($this->date_to));
    }

    public function createComponentDaterangeForm() {
        $form = new UI\Form();

        $form->addText('date_from', 'Dátum od')
                        ->setDefaultValue($this->date_from)
                        ->getControlPrototype()->class[] = 'datepicker';

        $form->addText('date_to', 'Dátum do')
                        ->setDefaultValue($this->date_to)
                        ->getControlPrototype()->class[] = 'datepicker';

        $form->addSubmit('submit', 'Použi filter');

        $form->onSuccess[] = function($form) {
            $values = $form->getValues();
            if (strtotime($values['date_from']) > strtotime($values['date_to'])) {
                $this->flashMessage('Dátum do nemôže byť menší ako dátum od.', 'warning');
                $this->redirect('Stats:', array('date_from' => $this->date_from, 'date_to' => $this->date_to));
            } else {
                $this->redirect('Stats:', array('date_from' => $values['date_from'], 'date_to' => $values['date_to']));
            }
        };
        return $form;
    }
    
    public function createComponentSelectMonthForm() {
    	$formMonths = new UI\Form();
    	$formMonths->setMethod('GET');
    	$formMonths->getElementPrototype()->class('form-inline');
    	
    	$months = [];
    	for ($i = 12; $i >= 1; $i--) {
    		$months[$i] = date("M - Y", strtotime( date( 'Y-m-01' )." -$i months"));
    	}
    	
    	$formMonths->addSelect('month', 'Mesiac:', $months)
    		->setPrompt('Zvolte mesiac');
    	
    	$formMonths->addSubmit('submit', 'Zvolený mesiac');
    	
    	$formMonths->onSuccess[] = function($formMonths) {
    		if($this->getUser()->isInRole('Administrátor')){
    			$values = $formMonths->getValues();
    			$month = $values['month'];
    			if (!isset($month) || $month < 1 || $month > 13) {
    				$this->flashMessage('Zvolený mesiac je neplatný', 'warning');
    			} else {
    				$nextMonth = ($month-1);
    				$this->downloadSCVFile(
    						new \DateTime(date("01.m.Y", strtotime( date( 'Y-m-01' )." -$month months"))),
    						new \DateTime(date("01.m.Y", strtotime( date( 'Y-m-01' )." -$nextMonth months"))));
    			}
    		}
    	};
    	return $formMonths;
    	
    }
    
    public function createComponentDaterangeFormForOrders() {
    	$formOrder = new UI\Form();
    	$formOrder->setMethod('GET');
    	$formOrder->getElementPrototype()->class('form-inline');
    	
    	$formOrder->addText('date_from_orders', 'Objednávky od')
    					->setDefaultValue($this->date_from_orders)
    					->getControlPrototype()->class[] = 'datepicker';
    
    	$formOrder->addText('date_to_orders', 'Objednávky do')
    					->setDefaultValue($this->date_to_orders)
    					->getControlPrototype()->class[] = 'datepicker';
    
    	$formOrder->addSubmit('submit', 'Zvolené obdobie');
    
    	$formOrder->onSuccess[] = function($formOrder) {
    		if($this->getUser()->isInRole('Administrátor')){
	    		$values = $formOrder->getValues();
	    		if (strtotime($values['date_from_orders']) > strtotime($values['date_to_orders'])) {
	    			$this->flashMessage('Dátum do nemôže byť menší ako dátum od.', 'warning');
	    		} else {
	    			$this->downloadSCVFile(new \DateTime($values['date_from_orders']), new \DateTime(date("d.m.Y", strtotime($values['date_to_orders'] . ' + 1 days'))));
	    		}
    		}
    	};
    	return $formOrder;
    }
    
    public function actionDownload($from = NULL, $to = NULL){
    	if($this->getUser()->isInRole('Administrátor')){
    		$this->downloadSCVFile(new \DateTime($from), new \DateTime($to));
    	}
    }

    private function downloadSCVFile($from, $to){
    	$path = "./../temp/";
    	$file = "export.csv";
    	$this->sendResponse(new  Nette\Application\Responses\FileResponse($this->createCSVFile((array) $this->statistics_model->dataToCSV(
    			$from, $to), $path . $file,
    			["produkt", "VS", "dátum", "suma", "spôsob platby", "meno", "adresa"]))
    			);
    	unlink($path . $file);
    }
    
    private function createCSVFile(array $rows, $filename, array $headers = NULL) {
    	$fh = fopen($filename, 'w');
    	if($headers) {
			$rowResult = [];
    		foreach($headers as $header) {
    			$rowResult[] = iconv('UTF-8', 'UTF-8', $header);
    		}
    		fputcsv($fh, $rowResult, ';');
    	}
    	foreach ($rows as $row) {
    		$rowResult = [];
    		foreach($row as $value) {
    			$rowResult[] = iconv('UTF-8', 'UTF-8', $value);
    		}
    		fputcsv($fh, $rowResult, ';');
    	}
    	fclose($fh);
    	return $filename;
    }
}
