<?php

namespace App\AdminModule\Presenters;

use Components\Forms,
    Components\Grids,
    App\Models,
    Nette,
    Nette\Application\UI,
    Tracy\Debugger,
	Nette\Application\Responses;


class OrdersPresenter extends BasePresenter
{

    /**
     * @inject
     * @var Models\Orders
     */
    public $model;

    /**
     * @inject
     * @var Models\TrainingProfile
     */
    public $training_profile;

    /**
     * @inject
     * @var Models\Kredity
     */
    public $kredity;

    /**
     * @inject
     * @var Models\Transakcie
     */
    public $transakcie;
	
	/**
     * @inject
     * @var Models\UserSeasonTicket
     */
    public $user_season_ticket;


    public function createComponentGrid($name)
    {
        return new Grids\Orders($this->model, $this, $name);
    }

    public function renderAddCash($id)
    {
        if ($id != NULL)
        {
            $form = $this->getComponent('cashOrderForm');

            $control = $form['user_id'];
            if (array_key_exists($id, $control->getItems()))
            {
                $control->setDefaultValue($id);
            } else {
                $this->flashMessage('Nepodarilo sa nájsť užívatela! Volaj Dejvovi!', 'warning');
                $this->redirect('addCash');
            }
        }
    }


    public function createComponentCashOrderForm($name)
    {
        return new \Components\Forms\AddCashOrder($this, $name, $this->model, $this->training_profile, $this->kredity);
    }


    public function handleConfirm($id)
    {
        try {
            $this->model->db()->beginTransaction();

            $order = $this->model->db()->table('view_orders')->where(array(
                'id = ?' => $id,
                'date_confirmed' => null
            ))->fetch();

            if (!$order) {
                throw new \PDOException("Objednávka s ID:$id sa nenašla (možno už je potvrdená)");
            }

            $this->model->confirmOrder($id);	
			
            $this->model->db()->commit();
            $this->flashMessage('Užívateľovi "'.$order->uzivatel.'" bola potvrdená objednávka', 'success');
        } catch (\PDOException $e) {
            $this->model->db()->rollBack();
            Debugger::log($e);
            $this->flashMessage('Nepodarilo sa upraviť záznam! Opakujte neskôr prosím', 'danger');
        }
        $this->redirect('default');
    }
    
    public function handleCancel($id)
    {
        try {
            $this->model->db()->beginTransaction();

            $order = $this->model->find()->where(array(
                'id = ?' => $id,
                'date_confirmed' => null
            ))->fetch();

            if (!$order) {
                throw new \PDOException();
            }

            $order->delete();

            $this->model->db()->commit();
            $this->flashMessage('Objednávka bola zmazaná zo systému', 'success');
        } catch (\PDOException $e) {
            $this->model->db()->rollBack();
            Debugger::log($e);
            $this->flashMessage('Nepodarilo sa upraviť záznam! Opakujte neskôr prosím', 'danger');
        }
        $this->redirect('default');
    }
    
	public function handleDisenroll($id)
    {
    	try {
    		//$this->model->db()->beginTransaction();
    			
    		$order = $this->model->find()->where(array(
    				'id = ?' => $id
    		))->fetch();
    
    		if (!$order) {
    			throw new \PDOException();
    		}
    
    		$this->model->disenrollPackOrder($id);
    		$order->update(['cancelled' => true]);
    		$this->flashMessage('Používateľ bol z kurzu odhlásený', 'success');
    		//$this->model->db()->commit();
    	} catch (\PDOException $e) {
    		//$this->model->db()->rollBack();
    		Debugger::log($e);
    		$this->flashMessage('Nepodarilo sa upraviť záznam! Opakujte neskôr prosím', 'danger');
    	}
    	$this->redirect('default');
    }
}
