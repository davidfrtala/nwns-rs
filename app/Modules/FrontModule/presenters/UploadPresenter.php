<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Models;

/**
 * Homepage presenter.
 */
class UploadPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Image
     */
    public $image;
    /**
     * @inject
     * @var Models\Galeries
     */
    public $galeries;

    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('FrontModule');

    }

    public function beforeRender()
    {
        if($this->user->isLoggedIn()) {
            $this->redirect("Homepage:default");
        }

        //$this->setLayout('auth_layout');
    }

    public function createComponentImageForm($name)
    {
        return new \Components\ImageForm($this, $name);
    }
    
    

}
