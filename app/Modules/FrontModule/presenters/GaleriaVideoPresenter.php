<?php

namespace App\FrontModule\Presenters;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form,
    App\Models;


class GaleriaVideoPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Clanky
     */

    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('FrontModule');

    }

    public function beforeRender()
    {
        if($this->user->isLoggedIn()) {
            $this->redirect("Homepage:default");
        }

        //$this->setLayout('auth_layout');
    }

    public function renderDefault()
    {
        return;
    }
    
    
}
