<?php

namespace App\FrontModule\Presenters;

use Components\Forms\NovyClovek;
use Nette,
    App\Models;

/**
 * Default presenter.
 */
class HomepagePresenter extends BasePresenter
{

	/**
     * @inject
     * @var Models\SendgridEmail
     */
    public $sendgrid_email;

    public function createComponentSignInForm($name)
    {
        return new \Components\Forms\NovyClovek($this, $name);
    }
    
    public function renderOffer()
    {
        $this->template->title = "Ponúkame";
    }
    
    public function renderNovyclovek()
    {
        $this->template->title = "Kurz Nový človek";
    }
    
    public function renderAboutus()
    {
        $this->template->title = "O nás";
    }
    
    public function renderContact()
    {
        $this->template->title = "Kontakt";
    }   
    
    public function renderVyucba()
    {
        $this->template->title = "Výučba";
    }
	
	public function renderKoncept()
    {
        $this->template->title = "Koncept";
    }
}
