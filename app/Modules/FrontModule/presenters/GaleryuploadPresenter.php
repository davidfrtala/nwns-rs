<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Models;

/**
 * Homepage presenter.
 */
class GaleryuploadPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Galeries
     */
    public $galeries;

    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('FrontModule');

    }

    public function beforeRender()
    {
        if($this->user->isLoggedIn()) {
            $this->redirect("Homepage:default");
        }

        //$this->setLayout('auth_layout');
    }

    public function createComponentGaleryForm($name)
    {
        return new \Components\GaleryForm($this, $name);
    }
    
    

}
