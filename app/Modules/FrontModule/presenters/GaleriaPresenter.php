<?php

namespace App\FrontModule\Presenters;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form,
    App\Models;


class GaleriaPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Galeries
     */
    public $galeries;
    /**
     * @inject
     * @var Models\Image
     */
    public $image;
    
    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('FrontModule');

    }
    
    public function renderDefault()
    {
        $this->template->image = $this->image->fetchAll();
        $this->template->title = "Galéria";
    }
    
}
