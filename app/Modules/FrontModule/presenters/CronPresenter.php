<?php

namespace App\FrontModule\Presenters;

use App\Models,
	Components\AdminKalendarControl,
	Tracy\Debugger;
use Components\KalendarControl;
use Nette\Utils\DateTime;


class CronPresenter extends BasePresenter
{
	/**
	 * @inject
	 * @var Models\Treningy
	 */
	public $treningy;

	/**
	 * @inject
	 * @var Models\Orders
	 */
	public $orders;

	/**
	 * @inject
	 * @var Models\TrainingProfile
	 */
	public $training_profile;

	/**
	 * @inject
	 * @var Models\Transakcie
	 */
	public $transakcie;
	
	/**
     * @inject
     * @var Models\ConvertKit
     */
    public $convertKit;
	
	/**
     * @inject
     * @var Models\SendgridEmail
     */
    public $sendgrid_email;


	/**
	 *
	 * @var string
	 */
	private $simple_access_token = "kj7jsLy12";


	/**
	 * @param string $access_token
	 * @throws \Exception
	 */
	public function actionDailyProcedure($access_token)
	{

		if ($access_token != $this->simple_access_token)
		{
			Debugger::log("Neplatny access token! Pokus o pristup k tejto adrese mimo cron službu??");
		} else {

			$this->deleteCancelledPaypalOrders();
		}

		exit();
	}


	/**
	 * Zmazeme vsetky paypal stare objednavky ktore visia v systeme ako nezaplatene
	 *
	 */
	private function deleteCancelledPaypalOrders()
	{
		try {
			$this->orders->db()->beginTransaction();

			$orders = $this->orders->find()->where(array(
				'platba = ?' => 'paypal',
				'date_confirmed' => null,
				'date_order < ?' => date('Y-m-d')
			));


			foreach ($orders as $order)
			{
				// zmažeme objednavku
				$order->delete();
			}

			// commit db prikaz
			$this->orders->db()->commit();
		} catch (\PDOException $e) {
			Debugger::log($e);
			$this->orders->db()->rollBack();
		}
	}




	public function actionTrener($access_token)
	{
		try {
			if ($access_token != $this->simple_access_token)
			{
				throw new \Exception("Neplatny access token! Pokus o pristup k tejto adrese mimo cron službu??", 1);
			}

			$treningy = $this->treningy->db()->fetchAll('
				SELECT t.*
				FROM view_training t
				where date(t.datum_od) = date(now())
				and zruseny = 0
				and now() >= t.termin_od - interval ? minute
				and now() < t.termin_od
				order by datum_od desc
			', KalendarControl::DEADLINE_PRIHLASENIE);

			foreach ($treningy as $trening)
			{
				$subject = '[NWNS Academy] Informácia o počte členov na tréning '.$trening->nazov.' ('.$trening->termin_od->format("h:i").' - '.date("F j, Y", strtotime($trening->termin_od)).')';

				$body = '
					<h3>'.$trening->nazov.'</h3>
					<b>Dátum: </b>'.date("F j, Y", strtotime($trening->termin_od)).'<br />
					<b>Čas: </b>'.$trening->termin_od->format("H:i").' - '.$trening->termin_do->format("H:i").'<br />
					<b>Tréner: </b>'.$trening->trener.'<br />
					<b>Max. počet prihlásených: </b>'.$trening->max_pocet.'<br />
					<b>Počet prihlásených: </b>'.$trening->cvicencov.'<br />
					<b>Prihlásení cvičenci: </b><p>'.$trening->cvicenci.'</p><br />
					<br />
					<p><i>(Táto správa bola automaticky vygenerovaná CRON serverovou službou)</i></p>
				';

				$recipients = ['treningy@nowillnoskill.academy'];

				if ($trening['trener'] != NULL)
					$recipients[] = $trening['trener_email'];

				if ($trening['asistent'] != NULL)
					$recipients[] = $trening['asistent_email'];
				
				$this->sendgrid_email->sendEmail($recipients, ['name' => 'NWNS RS Cron', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
			}
		} catch (\Exception $e) {
			Debugger::log($e);
		}

	    exit();
	}


	public function actionCheckPremium($access_token)
	{
		try {
			if ($access_token != $this->simple_access_token)
			{
				throw new \Exception("Neplatny access token! Pokus o pristup k tejto adrese mimo cron službu??", 1);
			}
			
			$trainees = $this->treningy->db()->fetchAll('
				SELECT tr.training_profile_id, COUNT(tr.training_profile_id) AS training_count, concat(i.meno, " ", i.priezvisko) AS cele_meno, i.meno AS firstname, u.login AS email, u.date_created
				FROM trainees tr
				LEFT JOIN training_profile tp ON tr.training_profile_id = tp.id
				LEFT JOIN invoice_detail i ON i.id = tp.id
				LEFT JOIN user u ON tp.user_id = u.id
				WHERE tp.premium != 1
				GROUP BY tr.training_profile_id
				HAVING COUNT(tr.training_profile_id) > 99
			');

			foreach ($trainees as $trainee)
			{
				// zacneme bezpecnu transakciu
				$this->training_profile->db()->beginTransaction();
				
				$profile = $this->training_profile->find()->wherePrimary($trainee->training_profile_id)->fetch();
				
				$profile->update(array(
					'premium' => true,
					'discount' => true
				));
				
				//Email cvicencovi
				$subject = '[NWNS Academy] Stal si sa Skalným Skillerom!';

				$body = '
					<p>Ahoj '.$trainee->firstname.'</p>
					<p>Gratulujeme!</p>
					<p>Práve si prekročil/a hranicu 100 tréningov. Znamená to, že získavaš titul "Skalný Skiller". Odteraz máš u nás nasledovné výhody:</p>
					<p>
					- možnosť kúpiť si mesačné členstvo za 50€ (miesto 60€)<br />
					- 20% zľava na workshopy v našej réžii 
					</p>
					<p>Okrem toho, ako náš znak vďaky za tvoju vernosť, získavaš tričko / tielko podľa svojho výberu z aktuálnej ponuky. Prosím napíš nám ako odpoveď na tento email tvoju preferovanú veľkosť, farbu a štýl trička a na kedy ti ho máme pripraviť.</p>
					<p>Ďakujeme ti a veríme, že s nami ostaneš aj naďalej.</p>
					<p>
						Pekný zvyšok dňa<br />Team NWNS Academy
					</p>
				';
				
				$this->sendgrid_email->sendEmail($trainee->email, ['name' => 'NWNS Academy', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
				
				//Email systemu
				$subject = '[NWNS Academy] Nový skalný člen';

				$body = '
					<p>Pribudol nám nový skalný člen: ' . $trainee->meno . ', členom od ' . $trainee->date_created . '<br />
						Email: ' . $trainee->email . '
					</p>
				';
				
				$this->sendgrid_email->sendEmail('kontakt@nowillnoskill.academy', ['name' => 'NWNS Academy', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
				
				// pridame do convertkitu
				$this->convertKit->setFormId("51479");
				$this->convertKit->setTags("59958");
				$this->convertKit->subscribe($trainee->email,  "{$trainee->firstname}");
				
				// commitneme transakciu
				$this->training_profile->db()->commit();
			}
		} catch (\Exception $e) {
			Debugger::log($e);
		}

	    exit();
	}


	public function actionTrening($access_token)
	{		
		try {
			if ($access_token != $this->simple_access_token)
			{
				throw new \Exception("Neplatny access token! Pokus o pristup k tejto adrese mimo cron službu??", 1);
			}


			$treningy = $this->treningy->db()->fetchAll('
				SELECT t.*
				FROM view_training t
				where
					date(t.datum_od) = date(now())
					and zruseny = 0
					and now() between (t.termin_od - interval ? hour) and t.termin_do
				order by datum_od desc
			', KalendarControl::DEADLINE_ODHLASENIE);

			// zacneme bezpecnu transakciu
			$this->treningy->db()->beginTransaction();

			foreach ($treningy as $trening)
			{

				if ($trening['cvicencov'] < 1)
				{
					$cvicencov = $this->treningy->db()->fetchAll('SELECT * FROM trainees where training_id = ?', $trening->id);

					// zmenime status na zruseny
					$this->treningy->find()->where('id', $trening->id)->update(array('zrusena_instancia' => 1));

					// odhlasime cvicencov
					$this->treningy->db()->table('trainees')->where('training_id = ?', $trening->id)->delete();

					foreach ($cvicencov as $cvicenec)
					{
						// vyrobime si model cvicenca
						$profile = $this->training_profile->find()->wherePrimary($cvicenec->training_profile_id)->fetch();
						$user = $this->profile->db()->fetch('SELECT * FROM view_user WHERE id = ?', $profile->user_id);


						// uzivatelovi pripiseme naspet kredity za zruseny trening
						$profile->update(array(
							'pocet_kreditov' => ($user->pocet_kreditov + $trening->kredity)
						));

						// pridam zaznam o transakcii
						$this->transakcie->table()->insert(array(
							'date_created' => new \DateTime(),
							'transaction_type_id' => 2,
							'target_id' => $trening->id,
							'user_id' => $profile->user_id
						));



						$subject = "[NWNS Academy] Dnešný tréning $trening->nazov sa ruší";

						$body = '
							Ahoj '.$user->meno.'<br/><br/>
							S ľútosťou ti oznamujeme, že dnešný tréning <b>'.$trening->nazov.'</b> sa z dôvodu nízkej účasti ruší. Ospravedlňujeme sa za nepríjemnosti a prajeme ti príjemný zvyšok dňa.<br/><br/>
							S pozdravom<br/>
							Team No Will No Skill Academy<br />';
						
						$this->sendgrid_email->sendEmail($user->login, ['name' => 'NWNS RS', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
					}
				}
			}
			// commitneme transakciu
			$this->treningy->db()->commit();
		} catch (\Exception $e) {
			// zastavime transakciu
			Debugger::log($e);
			$this->treningy->db()->rollBack();
		}

	    exit();
	}


	/**
	 * CRON funkcia, ktora skopiruje treningy z aktualneho tyzdna
	 *
	 * @param $access_token
	 */
	public function actionRenewSchedule($access_token)
	{
		try {
			if ($access_token != $this->simple_access_token)
			{
				//throw new \Exception("Neplatny access token! Pokus o pristup k tejto adrese mimo cron službu??", 1);
			}
			// zacneme bezpecnu transakciu
			$this->treningy->db()->beginTransaction();

			// prekopirujeme rozvrh
			$this->treningy->renewSchedule(new \DateTime());

			// commitneme transakciu
			$this->treningy->db()->commit();
		} catch (\Exception $e) {
			// zastavime transakciu
			$this->treningy->db()->rollBack();
			Debugger::log($e);
		}

		exit();
	}
}