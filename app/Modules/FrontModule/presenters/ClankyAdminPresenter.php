<?php

namespace App\FrontModule\Presenters;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form,
    App\Models;


class ClankyAdminPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Clanky
     */
    public $clanky;
    /**
     * @inject
     * @var Models\Categories
     */
    public $categories;

    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('FrontModule');

    }

    public function beforeRender()
    {
        if($this->user->isLoggedIn()) {
            $this->redirect("Homepage:default");
        }

        //$this->setLayout('auth_layout');
    }

    public function createComponentClankyForm($name)
    {
        return new \Components\ClankyForm($this, $name);
    }

    public function createComponentLoginForm($name)
    {
        return new \Components\LoginForm($this, $name);
    }

    public function createComponentRegisterForm($name)
    {
        return new \Components\RegisterForm($this, $name);
    }
}
