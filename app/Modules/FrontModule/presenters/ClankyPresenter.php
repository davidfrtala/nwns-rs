<?php

namespace App\FrontModule\Presenters;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form,
    App\Models;


class ClankyPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Clanky
     */
    public $clanky;
    /**
     * @inject
     * @var Models\Image
     */
    public $image;

    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('FrontModule');

    }

    public function renderDefault()
    {
        $this->template->clanky = $this->clanky->fetchClanky();
    }
    
    public function renderClanok($id = 0)
    {
        $clanok = $this->clanky->fetchClanok($id);
        if(count($clanok) > 0){
            $this->template->clanok = $clanok[$id];
        } else {
            $this->flashMessage('�l�nok s dan�m ID neexistuje');
        }
    }
    
}
