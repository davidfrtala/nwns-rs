<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Models;

/**
 * Default presenter.
 */
class RozvrhPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Treningy
     */
    public $treningy;
    
    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->title = "Rezervácie";
    }

    public function createComponentKalendar()
    {
        $control = new \Components\KalendarControl($this->profile, $this->treningy);

        return $control;
    }

}
