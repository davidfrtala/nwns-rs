<?php

namespace App\FrontModule\Presenters;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form,
    App\Models;


class AuthenticatePresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Treningy
     */
    public $treningy;

    /**
     * @inject
     * @var Models\PasswordHelper
     */
    public $password_helper;

    /**
     * @inject
     * @var Models\ConvertKit
     */
    public $convertKit;
	
	/**
     * @inject
     * @var Models\SendgridEmail
     */
    public $sendgrid_email;

    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('FrontModule');
    }

    public function beforeRender()
    {
		parent::beforeRender();
		
        if($this->user->isLoggedIn()) {
            if ($this->user->isInRole('Administrátor')) {
                $this->flashMessage("Administrátor sa nemôže prihlásiť na frontend!", "danger");
                $this->rememberMe->remove($this->user->identity->login);
                $this->user->logout(true);
                $this->redirect('Authenticate:default');
            }
            else if ($this->user->isInRole('Tréner'))
            {
                $this->redirect(":Front:Rozvrh:default");
            } else {
                $this->redirect(":User:Default:default");
            }
        }
    }

    public function renderDefault()
    {
        $this->template->title = "Prihlásenie";
    }

    public function renderRegister()
    {
        $this->template->title = "Registrácia";
    }

    public function renderLost()
    {
        $this->template->title = "Obnova hesla";
    }

    public function actionLogout()
    {
        $this->rememberMe->remove($this->user->identity->login);
        $this->user->logout(true);
        $this->redirect('Authenticate:default');
    }



    public function actionActivate($id, $fingerprint)
    {
        try {
            $this->profile->db()->beginTransaction();

            // vytiahneme si uzivatela z db (akceptujeme len tych s neaktivnym kontom)
            $user = $this->profile->getByUserId($id);
				
			if ($user == false || $user->aktivny) {
                throw new \Exception("Aktivácia nieje možná! Užívateľ nebol nájdený alebo je konto už aktívne!");
            }

            // fakturacne udaje
            $invoice = $user->related('invoice_detail')->fetch();

            // skutocny aktivacny kod
            $true_fingerprint = md5($user->login . $user->id);

            // skutocny aktivacny kod porovname s tym co sa nachadza v url
            if ($fingerprint != $true_fingerprint) {
                throw new \Exception("Aktivácia nieje možná! Neplatný aktivačný kód!");
            }

            // ak sme dosli az sem, mozme spokojne aktivovat konto
            $user->update(array(
                'aktivny' => 1
            ));

            // odoslat emailu po aktivácií
            $subject = "[NWNS Academy] Vitaj v No Will No Skill Academy";

            $body = $invoice->meno.'<br /><br />
                    Vítame ťa v No Will No Skill Academy. Sme radi že si sa s nami vydal na cestu k lepšej verzii samého seba. <br/><br/>
                    Tvoje prihlasovacie meno do systému: <strong>'.$user->login.'</strong><br /><br />Navyše, ako darček za tvoju registráciu si od nás dostal 10 skillierov úplne zadarmo.
                    Môžeš ich využiť na svoj prvý tréning podľa svojho výberu.<br /><br />
                    S pozdravom, No Will No Skill Academy';
			
			$this->sendgrid_email->sendEmail($user->login, ['name' => 'NWNS RS', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
			
            $this->profile->db()->commit();

            // pridame do convertkitu
			$this->convertKit->setFormId("30943");
			$this->convertKit->setTags("21689");
            $this->convertKit->subscribe($user->login,  "{$invoice->meno}");

            $this->flashMessage("Užívateľské konto '".$user->login."' bolo aktivované. Môžeš sa prihlásiť do systému.", "success");
        } catch (\Exception $e) {
            $this->profile->db()->rollback();
            $this->flashMessage($e->getMessage(), 'danger');
        }

        $this->redirect('Authenticate:default');
    }



    public function createComponentLoginForm($name)
    {
        return new \Components\LoginForm($this, $name);
    }






    public function createComponentRegisterForm($name)
    {
        return new \Components\RegisterForm($this, $name);
    }



    public function createComponentLostPwForm()
    {
        $form = new Form();

        $form->addText('email', 'E-mail')
            ->addRule(Form::EMAIL, 'Neplatný tvar e-mailovej adresy')
            ->setRequired("E-mail nesmie byť prázdny!");

        $form->addSubmit('submit', 'Potvrď');

        $form->onSuccess[] = [$this, 'lostPwFormSubmitted'];

        return $form;
    }
    public function lostPwFormSubmitted(Form $form)
    {
        $values = $form->getValues();
        try {
            $user = $this->profile->table()->where('login', $values->email)->fetch();

            $new_pwd = $this->password_helper->regeneratePassword($values->email);

            // updatneme heslo uzivatelovi
            $user->update(array(
                'password' => $this->password_helper->hash($new_pwd)
            ));

            // odoslat email
            $subject = "[NWNS Academy] Obnova strateného hesla";

            $body =  'Ahoj '.$user->related('invoice_detail')->fetch()->meno.'<br />
                        na základe tvojej žiadosti o obnovu hesla k tvojmu kontu na <a href="http://reservations.nowillnoskillacademy.com">reservations.nowillnoskillacademy.com</a>
                        ti zasielame tvoje nové heslo:<br /><br />
                        <strong>'.$new_pwd.'</strong><br /><br />
                        Toto heslo si prosím po prihlásení zmeň, hlavne kvôli bezpečnosti.<br /><br />';

			$this->sendgrid_email->sendEmail($user->login, ['name' => 'NWNS RS', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
			
            $this->flashMessage("Tvoje heslo bolo obnovené a zaslané na e-mail!", "success");
        } catch (\Exception $e) {
            $this->flashMessage($e->getMessage(), 'danger');
        }
    }
}
