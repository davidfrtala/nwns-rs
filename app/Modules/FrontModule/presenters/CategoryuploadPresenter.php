<?php

namespace App\FrontModule\Presenters;

use Nette,
    App\Models;

/**
 * Homepage presenter.
 */
class CategoryuploadPresenter extends BasePresenter
{
    /**
     * @inject
     * @var Models\Categories
     */
    public $categories;

    public function startup()
    {
        // zavolam dedkov startup (nie rodicovsky!)
        \App\Presenters\BasePresenter::startup();

        $this->user->getStorage()->setNamespace('FrontModule');

    }

    public function beforeRender()
    {
        if($this->user->isLoggedIn()) {
            $this->redirect("Homepage:default");
        }

        //$this->setLayout('auth_layout');
    }

    public function createComponentCategoryForm($name)
    {
        return new \Components\CategoryForm($this, $name);
    }
    
    

}
