<?php

namespace App\FrontModule\Presenters;

use \Nette,
	\App\Models;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends \App\Presenters\BasePresenter
{
    /**
     * @inject
     * @var \App\Models\Profile
     */
    public $profile;

    public function startup()
    {
        parent::startup();

        $this->user->getStorage()->setNamespace('FrontModule');
    }
	
	public function beforeRender()
    {
        parent::beforeRender();
		
		$this->template->version = $this->context->parameters['site']['version'];
    }

}
