<?php
namespace App\Models;

class Image extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'images';

	public function add($values)
	{
            $this->db()->beginTransaction();
            if(isset($_POST['image_url']) && !empty($_POST['image_url'])){
                foreach($_POST['image_url'] as $key=>$url){
                    $insert = array(
                        'nazov' => $_POST['nazov'][$key],
                        'image_url' => $url,
                        'galery_id' => $values['galery_id']
                    );
                    $result = $this->table()->insert($insert);
                }
            }
            $this->db()->commit();
            return $result;
	}
        
        public function fetchAll(){
            return $this->table()->fetchAll();
        }
        
}