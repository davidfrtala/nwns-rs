<?php
namespace App\Models;

class Galeries extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'galeries';

	public function add($values)
	{
            $insert = array(
                'nazov' => $values['nazov']
            );
            $result = $this->table()->insert($insert);
            return $result;
	}
        
        public function fetchAll(){
            $result = $this->table()->fetchAll();
            $pole = array();
            foreach($result as $res){
                $pole[$res['id']] = $res['nazov'];
            }
            return $pole;
        }
        
}