<?php
namespace App\Models;

use \Nette\Database\Table\Selection,
	\Exception;

class UserSeasonTicket extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'user_season_tickets';


	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(\Nette\Database\Context $db)
	{
		parent::__construct($db);
	}
	
	public function addPenalty($user_id, $training_id) {
		$ticket = $this->find()->where('user_id = ?', $user_id)->fetch();
		
		$expiration = $ticket->valid_until;
		if ($ticket->penalties >= 2) {
			$expiration = new \DateTime();
		}
		
		$ticket->update(array(
			'penalties+=' => 1,
			'valid_until' => $expiration
		));
		
		return $ticket;
	}
	
	public function prolongDuration($user_id, $season_ticket) {
		$ticket = $this->find()->where('user_id', $user_id)->fetch();

		$expiration = NULL;
		if ($season_ticket->date_to != NULL) {
			$expiration = $season_ticket->date_to;
		}
		if (!$ticket) {
			if ($expiration == NULL) {
				$expiration = new \DateTime("tomorrow +$season_ticket->duration months");
				$expiration->sub(new \DateInterval("PT1M"));
			}
			//vytvorim novu permanentku
			$insert = array(
				'valid_until' => $expiration,
				'penalties' => 0,
				'user_id' => $user_id
			);
			$this->table()->insert($insert);
		} else {
			if ($expiration == NULL) {
				$expiration = new \DateTime();
				if ($ticket->valid_until > new \DateTime()) {
					$expiration = $ticket->valid_until;
				}
				$expiration = $expiration->modify("tomorrow");
				$expiration->sub(new \DateInterval("PT1M"));
				$expiration->add(new \DateInterval("P" . $season_ticket->duration . "M"));
			}
			
			//update existujucej permanentky
			$ticket->update(array(
				'penalties' => 0,
				'valid_until' => $expiration
			));
		}
	}
}