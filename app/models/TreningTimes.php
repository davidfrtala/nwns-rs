<?php
namespace App\Models;


class TreningTimes extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'training_times';

	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(\Nette\Database\Context $db)
	{
		parent::__construct($db);
	}

}