<?php

/**
 * Created by PhpStorm.
 * User: menthor
 * Date: 4.10.15
 * Time: 14:38
 */

namespace App\Models;

class Statistics extends BaseModel {

    /**
     * Constructor
     *
     * @param	\Nette\Database\Connection	$db
     */
    public function __construct(\Nette\Database\Context $db) {
        parent::__construct($db);
    }

    public function getTrainings(\DateTime $from, \DateTime $to) {
        $sql = "
		select
			nazov,
			sum(case zruseny when 0 then 1 else 0 end) odtrenovanych,
			sum(zruseny) zrusenych,
                        sum(case zruseny when 0 then cvicencov else 0 end) celkom_ludi 
		from view_training
		where
			datum_od between ? and ?
		group by nazov
		";

        $result = $this->db()->fetchAll($sql, $from, $to);
        return $result;
    }

    public function getOrdersCounts(\DateTime $from, \DateTime $to) {
        $sql = "
		select
			o.nazov,
			count(o.nazov) pocet,
			sum(case o.platba when 'paypal' then 1 else 0 end) paypal,
			sum(case o.platba when 'prevod' then 1 else 0 end) prevod,
			sum(case o.platba when 'hotovost' then 1 else 0 end) hotovost,
			sum(if(o.date_confirmed, 0, 1)) neuhradene,
                        sum(cr.cena) eur
		from view_orders o
		left join credits cr on o.credits_id = cr.id
		where
			o.date_order between ? and ?
		group by nazov
		order by eur desc
		";

        $result = $this->db()->fetchAll($sql, $from, $to);
        return $result;
    }

    public function getTrainers(\DateTime $from, \DateTime $to) {
        $sql = "
		select
			trener,
			sum(case zruseny when 0 then 1 else 0 end) odtrenovanych,
			sum(zruseny) zrusenych,
                        sum(case zruseny when 0 then cvicencov else 0 end) celkom_ludi 
		from view_training v
		where
			datum_od between ? and ?
		group by trener
		order by kredity desc
		";

        $result = $this->db()->fetchAll($sql, $from, $to);
        return $result;
    }

    public function getRegisteredUsersCount(\DateTime $from, \DateTime $to) {
        $sql = "select count(*) from user where date_created between ? and ?";
        $result = $this->db()->fetch($sql, $from, $to);

        return $result[0];
    }

    /**
     * Get registered users with training order
     * @param \DateTime $from
     * @param \DateTime $to
     * @return type
     */
    public function getConfirmedUsersCount(\DateTime $from, \DateTime $to) {
        $sql = "
                select
                        count(distinct trainees.training_profile_id)
                from
                    trainees
                left join training_profile on trainees.training_profile_id = training_profile.id
                left join user on training_profile.user_id = user.id
                where                   
                    user.date_created between ? and ?
            ";

        $result = $this->db()->fetch($sql, $from, $to);

        return $result[0];
    }

    /**
     * Get converted users with training order
     * @param \DateTime $from
     * @param \DateTime $to
     * @return type
     */
    public function getConvertedUsersCount(\DateTime $from, \DateTime $to) {
        $sql = "
                select
                        count(*)
                from
                    user
                left join orders on user.id = orders.user_id
                where
                    (orders.date_order between ? and ?)
                    and (user.date_created between ? and ?)
            ";

        $result = $this->db()->fetch($sql, $from, $to, $from, $to);

        return $result[0];
    }
    
    public function dataToCSV(\DateTime $from, \DateTime $to){
    	$sql = "
		select
			o.nazov, o.variabilny_symbol, o.date_confirmed, c.cena, o.platba, CONCAT(u.meno, ' ', u.priezvisko), CONCAT(u.ulica, ', ', u.psc, ' ', u.mesto)
		from
    			view_orders o
    	left join
    			view_user u on o.user_id = u.id
    	left join
    			credits c on c.id = o.credits_id
    	where                   
                o.date_confirmed BETWEEN ? AND ?
		";
    	return $this->db()->fetchAll($sql, $from, $to);
    }

}
