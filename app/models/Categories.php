<?php
namespace App\Models;

class Categories extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'categories';

	public function add($values)
	{
            $parent_category_id = $values['parent_category_id'];
            if($values['parent_category_id'] == 0){
                $parent_category_id = null;
            }
            $insert = array(
                'parent_category_id' => $parent_category_id,
                'nazov' => $values['nazov']
            );
            $result = $this->table()->insert($insert);
            return $result;
	}
        
        public function fetchAll(){
            $result = $this->table()->fetchAll();
            $pole = array();
            foreach($result as $res){
                $pole[$res['id']] = $res['nazov'];
            }
            return $pole;
        }
        
}