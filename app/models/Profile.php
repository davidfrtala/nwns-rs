<?php
namespace App\Models;

use	\Nette\Security\User;

class Profile extends BaseModel
{
    /**
     * @var string
     */
    protected $_table = 'user';

    /**
     * @var User
     */
    private $user;

    /**
     * @var PasswordHelper
     */
    private $password;

    /**
     * @var InvoiceDetail
     */
    private $invoice_detail;

    /**
     * @var TrainingProfile
     */
    private $training_profile;
	
	/**
     * @var SeasonTicket
     */
    private $user_season_ticket;

    /**
     * Constructor.
     * @param    User $user
     * @param    \Nette\Database\Connection $db
     */
    public function __construct(User $user, TrainingProfile $tp, InvoiceDetail $invd, UserSeasonTicket $st, PasswordHelper $pwd, \Nette\Database\Context $db)
    {
        parent::__construct($db);

        $this->user = $user;
        $this->password = $pwd;
        $this->invoice_detail = $invd;
        $this->training_profile = $tp;
		$this->user_season_ticket = $st;
    }


    /**
     * Vrati User ID ak som prihlaseny
     *
     * @return integer
     */
    public function getId()
    {
        return $this->user->getId();
    }
		
	public function getSeasonTicket() 
	{
		return $this->user_season_ticket->find()->where("user_id", $this->getId());
	}


    public function getByUserId($userId = NULL)
    {
        return $this->find()->get(
            ($userId != NULL) ? $userId : $this->getId()
        );
    }


    /**
     * Updatne heslo aktualne prihlasenemu uzivatelovi
     *
     * @param array
     */
    public function changePassword($values, $userId = NULL)
    {
        if ($this->getId() == null) {
            throw new Exception('Užívateľ nieje prihlásený!');
        }

        if ($values['password'] != null && $values['password2'] != null && $values['password'] == $values['password2']) {
            $password = $this->password->hash($values['password']);
        } else {
            throw new Exception('Zle zadané heslá!');
        }

        $this->find()->where(array(
            'id' => ($userId != NULL) ? $userId : $this->getId()
        ))->update(array('password' => $password));
    }

    /**
     * Zaregistrujeme noveho uzivatela
     *
     * @param array
     */
    public function register($values)
    {
        $cvicenec = $this->db->table('role')->get(3);

        if ($cvicenec == false) {
            throw new \DibiException("Nepodarilo sa získať rolu z databázy. Je tabulka [role] naplnená?");
        }

        if ($values['password'] != null && $values['password2'] != null && $values['password'] == $values['password2']) {
            $heslo = $this->password->hash($values['password']);
        }


        $insert = array(
            'role_id' => $cvicenec,
            'login' => $values['login'],
            'password' => $heslo
        );
        $user_id = $this->table()->insert($insert);
        unset($insert);

        $insert = array(
            'meno' => $values['meno'],
            'priezvisko' => $values['priezvisko'],
            'ulica' => $values['ulica'],
            'mesto' => $values['mesto'],
            'psc' => $values['psc'],
            //'variabilny_symbol' => $this->invoice_detail->generateUniqeVS(8),
            'user_id' => $user_id
        );
        $this->invoice_detail->table()->insert($insert);
        unset($insert);

        $insert = array(
            'platnost_kreditov' => new \Nette\Database\SqlLiteral('NOW() + INTERVAL 3 MONTH'),
            'odporucil' => $values['odporucil'],
            'user_id' => $user_id
        );
        $this->training_profile->table()->insert($insert);
		
		$insert = array(
            'valid_until' => new \Nette\Database\SqlLiteral('NOW()'),
			'penalties' => 0,
            'user_id' => $user_id
        );
        $this->user_season_ticket->table()->insert($insert);
        unset($insert);

        return $user_id;
    }


    public function update($values, $userId = NULL)
    {
        try {
            $this->db()->beginTransaction();

            $this->getByUserId($userId)->update(array(
                'login' => $values['login']
            ));

            $this->getInvoiceDetail($userId)->update(array(
                'meno' => $values['meno'],
                'priezvisko' => $values['priezvisko'],
                'ulica' => $values['ulica'],
                'mesto' => $values['mesto'],
                'psc' => $values['psc']
            ));


            $this->getTrainingProfile($userId)->update(array(
                'o_mne' => $values['o_mne'],
                'citat' => $values['citat'],
                'citat_autor' => $values['citat_autor']
            ));

            $this->db()->commit();
        } catch (\Exception $e) {
            $this->db()->rollBack();
            throw $e;
        }
    }
    public function getTrainingProfile($userId = NULL)
    {
        return $this->training_profile->find()->where("user_id",
            ($userId != NULL) ? $userId : $this->getId()
        );
    }

    public function getInvoiceDetail($userId = NULL)
    {
        return $this->invoice_detail->find()->where("user_id",
            ($userId != NULL) ? $userId : $this->getId()
        );
    }

    public function getDonutData($userId)
    {
        $sql = "
            select vt.nazov, vt.color, count(vt.nazov) pocet from trainees ts
            join training_profile tp on tp.id = ts.training_profile_id and tp.user_id = ?
            join view_training vt on vt.id = ts.training_id
			join training_type tt on tt.id = vt.training_type_id
            where vt.zruseny = 0 and (vt.termin_do < NOW()) and (tt.aktivny = 1)
            group by vt.nazov";

        $dbdata = $this->db()->fetchAll($sql, $userId);
		
        $sum = 0;
        $chart = array();
        $stats = array();
        $colors = array();

        // pocet ide zvlast
        foreach($dbdata as $row)
        {
            $sum += $row['pocet'];
        }
        foreach($dbdata as $row)
        {

            $colors[] = '#'.$row['color'];

            $chart[] = array(
                'label' => $row['nazov'],
                'value' => $row['pocet']
            );

            $stats[$row['nazov']] = array(
                'share' => round(($row['pocet'] / $sum) * 100, 2),
                'color' => '#'.$row['color']
            );
        }
        return array(
            'colors' => $colors,
            'chart' => $chart,
            'stats' => $stats,
        );
    }

    public function getTodaysTrainingData($userId)
    {
        $sql = '
            select date_format(vt.termin_od, "%e %b %Y, %H:%i") termin_od, vt.nazov from trainees ts
            join training_profile tp on tp.id = ts.training_profile_id and tp.user_id = ?
            join view_training vt on vt.id = ts.training_id
            where date_format(vt.termin_od, "%Y-%m-%d") = date_format(NOW(), "%Y-%m-%d")
            order by vt.termin_od asc';

        $result = $this->db()->fetchAll($sql, $userId);

        return $result;
    }


    public function getNextTrainingsData($userId)
    {
        $sql = '
            select date_format(vt.termin_od, "%e %b %Y, %H:%i") termin_od, vt.nazov from trainees ts
            join training_profile tp on tp.id = ts.training_profile_id and tp.user_id = ?
            join view_training vt on vt.id = ts.training_id
            where date_format(vt.termin_od, "%Y-%m-%d") > date_format(NOW(), "%Y-%m-%d")
            order by vt.termin_od asc';

        $result = $this->db()->fetchAll($sql, $userId);

        return $result;
    }
}