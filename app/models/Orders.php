<?php
namespace App\Models;

use \Nette\Database\Table\Selection,
	\Nette,
	\Exception;

class Orders extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'orders';

	/**
	 * @var Kredity
	 */
	private $kredity;

	/**
	 * @var Transakcie
	 */
	private $transakcie;
	
	/**
	 * @var TrainingProfile
	 */
	private $training_profile;
	
	/**
	 * @var UserSeasonTicket
	 */
	private $user_season_ticket;
	
	/**
	 * @var Pack
	 */
	private $pack;

	/**
	 * Constructor
	 *
	 * @param	Produkty	$produkty
	 * @param	Transakcie	$transakcie
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(Kredity $kredity, Transakcie $transakcie, TrainingProfile $tp, UserSeasonTicket $ust, Pack $p, \Nette\Database\Context $db)
	{
		parent::__construct($db);
		$this->kredity = $kredity;
		$this->transakcie = $transakcie;
		$this->training_profile = $tp;
		$this->user_season_ticket = $ust;
		$this->pack = $p;
	}
	
	/**
	 * Vygeneruje unikatny variabilny symbol
	 * @param integer $length - pocet znakov
	 */
	public function generateUniqueVS($length)
	{
		$moznosti='0123456789';
		do {
			$result="";
			while(strlen($result) < $length) {
				$result .= substr($moznosti, mt_rand(0, strlen($moznosti) - 1), 1);
			}

			$exists = $this->find()->where('variabilny_symbol', $result)->fetch();
		} while ($exists != false); // ak sme nááááhodou vygenerovali variabilny symbol ktory uz v databaze raz existuje

		return (int) $result;
	}

	/**
	 * @param Profile
	 * @param integer
	 * @throws \Exception
	 */
	public function checkout($user_id, $platba, $produkt, $price = NULL)
	{
		$row = $this->kredity->find()->where('id', $produkt)->fetch();

		if ($row == false)
		{
			throw new \Exception('Neexistujúci produkt!');
		}
		
		$order = $this->table()->insert(array(
			'user_id' 	=> $user_id,
			'credits_id' 	=> $row->id,
			'platba'	=> $platba,
			'date_order'	=> new \DateTime(),
			'date_confirmed'	=> null,
			'variabilny_symbol' => $this->generateUniqueVS(8),
			'price' => ($price == NULL) ? $row->cena : $price
		));
		return $order;
	}


    public function getGridData(){
        return $this->db()->table('view_orders');
    }


	/**
	 * @return Kredity
	 */
	public function getKredity()
	{
		return $this->kredity;
	}
	
	public function getCreditsByCreditsID($credits_id)
	{
		return $this->kredity->find()->where('id', $credits_id)->fetch();
	}
	
	public function getPackByCreditsPackID($packID)
	{
		return $this->db()->table('packs')->where(array(
				'id = ?' => $packID
		))->fetch();
	}
	
	

	public function confirmOrder($order_id)
	{
		$order = $this->db()->table('orders')->where(array(
					'id = ?' => $order_id,
					'date_confirmed' => null
					))->fetch();
		
		$kredity = $this->getCreditsByCreditsID($order->credits_id);
		
		//pridame kredity a predlzime platnost
		$this->training_profile->addKredity($order->user_id, $kredity->kredity);
		$this->training_profile->prolongKredityExpiration($order->user_id);
		
		switch($kredity->credits_type_id) {
			case "1":
				//kredity sme uz pridali
				break;
			case "2":
				//predlzime permanentku
				$season_ticket = $this->db()->table('season_tickets')->where(array(
					'id = ?' => $kredity->season_ticket_id
				))->fetch();
				$this->user_season_ticket->prolongDuration($order->user_id, $season_ticket);
				break;
			case "3":
				//zarezervujeme treningy z balicka
				$pack = $this->getPackByCreditsPackID($kredity->pack_id);
				$this->pack->applyTrainingPack($order->user_id, $pack);
				break;
		}
		
		$this->transakcie->addKreditTransaction($order->user_id, $order->credits_id);
		$order->update(array(
			'date_confirmed' => new Nette\Utils\DateTime()
		));
	}
	
	public function disenrollPackOrder($order_id)
	{
		$order = $this->db()->table('orders')->where(array(
					'id = ?' => $order_id
					))->fetch();
		
		$kredity = $this->getCreditsByCreditsID($order->credits_id);
		if($kredity->credits_type_id == 3) {
			$pack = $this->getPackByCreditsPackID($kredity->pack_id);
			$this->pack->disenrollTrainingPack($order->user_id, $pack);
		}
	}
}