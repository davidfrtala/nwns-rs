<?php
namespace App\Models;

use \Nette\Database\Table\Selection,
	\Exception;

class Produkty extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'product';


	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(\Nette\Database\Context $db)
	{
		parent::__construct($db);
	}

	/**
	 *
	 *
	 * @return	array
	 */
	public function fetchProdukty()
	{
		$result = array();

		$rows = $this->find()->where('aktivny',1)->order('id ASC');

		foreach ($rows as $produkt)
		{
			$result[$produkt->id] = $produkt->nazov . ' (' . $produkt->kredity . ' Kreditov)';
		}

		return $result;
	}
}