<?php
namespace App\Models;

use \Components\KalendarControl,
	\Exception;

class Treningy extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'training';

	/**
	 * @var Profile
	 */
	private $profile;

	/**
	 * @var TrainingProfile
	 */
	private $training_profile;

	/**
	 * @var TreningType
	 */
	private $trening_type;

	/**
	 * @var TreningTimes
	 */
	private $trening_times;
	
	/**
     * @inject
     * @var Models\SendgridEmail
     */
    private $sendgrid_email;

	/**
	 * Constructor
	 *
	 * @param	Profile	$profile
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(Profile $profile, TrainingProfile $training_profile, TreningType $trening_type, TreningTimes $trening_times, SendgridEmail $email, \Nette\Database\Context $db)
	{
		parent::__construct($db);

		$this->profile = $profile;
		$this->trening_type = $trening_type;
		$this->training_profile = $training_profile;
		$this->trening_times = $trening_times;
		$this->sendgrid_email = $email;
	}



	/**
	 * Zarezervuje prihlasenemu uzivatelovi daný trening. Zisti či ho je možne zarezervovat, či ma dost kreditov,
	 * či nieje po deadlinoch a podobne. Vykonam aj zaznam o transakcii
	 *
	 * @param integer
	 * @throws \Exception
	 */
	public function reserveTrening($trening_id, $reservationType = 1, $userId = NULL)
	{
		$profileId = ($userId != NULL) ? $userId : $this->profile->getId();
		if ($profileId == null) {
			throw new Exception('Ak si chceš tréning rezervovať, musíš sa najskôr prihlásiť!');
		}

        $tp = $this->profile->getTrainingProfile($profileId)->select("id")->fetch();
        $training_profile_id = ($tp != false) ? $tp->id : false;
        $row = $this->db->fetch("
            select
                t.*,
                case when ts.training_id is null then false else true end prihlaseny_na_trening
            from view_training t
            left join trainees ts on ts.training_id = t.id and ts.training_profile_id = ?
			where t.id = ?
			", $training_profile_id, $trening_id);

		// zle id treningu? pokial databaza nevratila ziadny riadok
		if ($row == false) {
			throw new Exception('Na tento tréning sa nie je možné prihlásiť. Pravdepodobne zlé vstupné údaje!');
		}

		// ak su uz kapacity plne
		if ($row['cvicencov'] >= $row['max_pocet']) {
			throw new Exception('Na tento tréning sa už nedá prihlásiť, pretože už je plný!');
		}

		// ak je uz davno po deadline na prihlasenie
		$diff = $row['termin_od']->diff(new \DateTime());
		if ($diff->d == 0 && $diff->h == 0 && $diff->i < KalendarControl::DEADLINE_PRIHLASENIE) {
			throw new Exception('Na tento tréning sa už nedá prihlásiť, pretože vypršal jeho deadline na prihlásenie!');
		}

		// ak som uz prihlaseny
		if ($row['prihlaseny_na_trening'] == true) {
			throw new Exception('Na tomto tréningu si už prihlásený!');
		}

		// ak sa snazim prihlasit na zruseny trening
		if ($row['zruseny'] == true) {
			throw new Exception('Tento tréning bol zrušený!');
		}

		// alebo mam nizsiu uroven nez trening pozaduje
        /*
		$user_level =   $this->profile->table()
								->get($this->profile->getId())
									->ref('skill_level', 'skill_level_id')->level;
		if ($user_level < $row['level'] && $row['level'] != '0')
			throw new Exception('Nedostatočný level pre prihlásenie sa na tréning!');
        */
		
		$season_ticket_valid = false;
		if ($reservationType != 3) { //ak nejde o treningovy balik
			if ($season_ticket = $this->profile->getSeasonTicket()->fetch()) {
				$diff = $row['termin_od']->getTimeStamp() - $season_ticket->valid_until->getTimeStamp();
				if ($diff < 0) { //permanentka plati v case zaciatku treningu
					$training_count = $this->db->fetch("
						select
							count(t.termin_od) as pocet_treningov
						from view_training t
						left join trainees ts on ts.training_id = t.id and ts.training_profile_id = ?
						where ts.reservation_type = 2 and t.termin_do > NOW()
						", $training_profile_id);
					if ($training_count->pocet_treningov == 3) {
						throw new Exception('Nie je možné prihlásiť sa na viac ako 3 tréningy, ktoré ešte neskončili.');
					}
					
					$season_ticket_valid = true;
				}
			}
		}
		
		if ($reservationType != 3 && !$season_ticket_valid) {
			// ak som sa dostal az sem, idem ratat kredity
			$tp = $this->profile->getTrainingProfile()->fetch();
			$platnost_kreditov = $tp['platnost_kreditov'];
			$pocet_kreditov = $tp['pocet_kreditov'];

			// ak mi vyprsala platnost kreditov
			//if ((time() - strtotime($platnost_kreditov)) >= 0) {
			//    throw new Exception('Platnosť tvojich kreditov vypršala!');
			//}

			// ak nemam dost kreditov
			if ($pocet_kreditov < $row['kredity']) {
				throw new Exception('Nemáš dostatočný počet skillierov na prihlásenie sa na tréning!');
			}
		}
		
        try {
			if ($reservationType != 3) $this->db->beginTransaction();

            // prihlasime sa na trening
            $this->db->table('trainees')->insert(array(
                'training_id' => $trening_id,
                'training_profile_id' => $tp->id,
				'reservation_type' => $season_ticket_valid ? 2 : $reservationType
            ));

			if ($reservationType != 3 && !$season_ticket_valid) {
				// updatnem zostavajuci pocet kreditov uzivatela
				$this->profile->getTrainingProfile()->update(array(
					'pocet_kreditov' => ($pocet_kreditov - $row['kredity'])
				));
			}

            // pridam zaznam o transakcii
            $this->db->table('transactions')->insert(array(
                'transaction_type_id' => 1,
                'user_id' => $profileId,
                'target_id' => $trening_id,
                'date_created' => new \DateTime(),
            ));

            if ($reservationType != 3) $this->db->commit();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw $e;
        }

		return $row['cvicencov'] == ($row['max_pocet'] - 1);
	}

	
	public function unreserveCourseTrening($trening_id, $userId){
		$tp = $this->profile->getTrainingProfile($userId)->select("id")->fetch();
	
		$training_profile_id = ($tp != false) ? $tp->id : false;
		$row = $this->db->fetch("
            select
                t.*,
                case when ts.training_id is null then false else true end prihlaseny_na_trening
            from view_training t
            left join trainees ts on ts.training_id = t.id and ts.training_profile_id = ?
			where t.id = ?
			", $training_profile_id, $trening_id);
	
		try {
	
			// zle id treningu? pokial databaza nevratila ziadny riadok
			if ($row == false) {
				return;
			}
				
			// ak som na tomto treningu neni prihlaseny
			if ($row['prihlaseny_na_trening'] != true) {
				return;
			}
				
			// ak je uz davno po deadline na prihlasenie
			$diff = $row['termin_od']->diff(new \DateTime());
			if ($diff->d == 0 && $diff->h < $row['unreserve_deadline']) {
				return;
			}
				
			// ak som sa dostal az sem, idem ratat kredity
			$tp = $this->profile->getTrainingProfile()->fetch();
			$platnost_kreditov = $tp['platnost_kreditov'];
			$pocet_kreditov = $tp['pocet_kreditov'];
				
	
			// zacneme bezpecnu transakciu
			$this->db->beginTransaction();
	
			$trainee = $this->db->table('trainees')->where(array(
					'training_id' => $trening_id,
					'training_profile_id' => $training_profile_id
			))->fetch();
	
			// odhlasime sa z treningu
			$this->db->table('trainees')->where(array(
					'training_id' => $trening_id,
					'training_profile_id' => $training_profile_id
			))->delete();
	
			// pridam zaznam o transakcii
			$this->db->table('transactions')->insert(array(
					'transaction_type_id' => 2,
					'user_id' => $this->profile->getId(),
					'target_id' => $trening_id,
					'date_created' => new \DateTime(),
			));
	
			// commitneme transakciu
			$this->db->commit();
		} catch (Exception $e) {
			$this->db->rollBack();
			throw $e;
		}
	}

	/**
	 * Odhlasim prihlaseneho uzivatela z treningu. Zistim ci ho mozem odhlasit. Vykonam aj zaznam o transakcii
	 *
	 * @param integer
	 * @throws \Exception
	 */
	public function unreserveTrening($trening_id)
	{
		if ($this->profile->getId() == null) {
			throw new Exception('Ak si chceš tréning odhlásiť, musíš sa najskôr prihlásiť!');
		}

        $tp = $this->profile->getTrainingProfile()->select("id")->fetch();
        $training_profile_id = ($tp != false) ? $tp->id : false;
        $row = $this->db->fetch("
            select
                t.*,
                case when ts.training_id is null then false else true end prihlaseny_na_trening
            from view_training t
            left join trainees ts on ts.training_id = t.id and ts.training_profile_id = ?
			where t.id = ?
			", $training_profile_id, $trening_id);

		// zle id treningu? pokial databaza nevratila ziadny riadok
		if ($row == false) {
			throw new Exception('Z tohto tréningu sa nie je možné odhlásiť. Pravdepodobne zlé vstupné údaje!');
		}

		// ak som na tomto treningu neni prihlaseny
		if ($row['prihlaseny_na_trening'] != true) {
			throw new Exception('Na tomto tréningu niesi prihlásený!');
		}

		// ak je uz davno po deadline na prihlasenie
        $diff = $row['termin_od']->diff(new \DateTime());
        if ($diff->d == 0 && $diff->h < $row['unreserve_deadline']) {
			throw new Exception('Z tohto tréningu sa nedá odhlásiť, pretože vypršal jeho deadline na odhlásenie!');
		}


        // ak som sa dostal az sem, idem ratat kredity
        $tp = $this->profile->getTrainingProfile()->fetch();
        $platnost_kreditov = $tp['platnost_kreditov'];
        $pocet_kreditov = $tp['pocet_kreditov'];

        try {
            // zacneme bezpecnu transakciu
            $this->db->beginTransaction();
			
			$trainee = $this->db->table('trainees')->where(array(
                'training_id' => $trening_id,
                'training_profile_id' => $tp->id
            ))->fetch();

            // odhlasime sa z treningu
            $this->db->table('trainees')->where(array(
                'training_id' => $trening_id,
                'training_profile_id' => $tp->id
            ))->delete();
			
			if ($trainee->reservation_type == 1) {
				// updatnem zostavajuci pocet kreditov uzivatela
				$this->profile->getTrainingProfile()->update(array(
					'pocet_kreditov' => ($pocet_kreditov + $row['kredity'])
				));
			} else if ($trainee->reservation_type == 3) {
				// updatnem zostavajuci pocet kreditov uzivatela
				$this->profile->getTrainingProfile()->update(array(
						'pocet_kreditov' => ($pocet_kreditov + 5)
				));
			}
       
            // pridam zaznam o transakcii
            $this->db->table('transactions')->insert(array(
                'transaction_type_id' => 2,
                'user_id' => $this->profile->getId(),
                'target_id' => $trening_id,
                'date_created' => new \DateTime(),
            ));

            // commitneme transakciu
            $this->db->commit();
        } catch (Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
	}

    private function getDayString($day)
    {
        switch ($day) {
            case 0:
                return "Nedeľa";
            case 1:
                return "Pondelok";
            case 2:
                return "Utorok";
            case 3:
                return "Streda";
            case 4:
                return "Štvrtok";
            case 5:
                return "Piatok";
            case 6:
                return "Sobota";
        }       
    }

    public function getData(\DateTime $start, $firstday, $dayscount = 9)
    {
        $data = array();
        $startDate = $start->format("w");
        for ($i = 0; $i < $dayscount; $i++)
        {
            $day = $this->getDayString($startDate % 7);
            $data[] = array('day' => $day, 'treningy' => array());
            $startDate += 1;
        }

        $tp = $this->profile->getTrainingProfile()->select("id")->fetch();
        $training_profile_id = ($tp != false) ? $tp->id : false;
    	for ($i = 0; $i < $dayscount; $i++)
    	{
	        $result = $this->db->fetchAll("
				select
					t.*,
					case when ts.training_id is null then false else true end prihlaseny_na_trening
				from view_training t
				left join trainees ts on ts.training_id = t.id and ts.training_profile_id = ?
				where date_format(termin_od, '%Y-%m-%d') = ? AND (t.zruseny = 0 OR t.zrusena_instancia = 1)
	        	", $training_profile_id, $start->format('Y-m-d'));

			$data[$i]['treningy'] = array();
	        foreach ($result as $row) {
				$key = 'training_times_id-'.$row->training_times_id;
	        	$data[$i]['treningy'][$key] = $row;
                $data[$i]['datetime'] = $start->format('Y-m-d');
	        }
            $data[$i]['today'] = ($start->format('Y-m-d') == date('Y-m-d'));
            $data[$i]['firstday'] = ($start->format('Y-m-d') == $firstday);

	        $start->add(new \DateInterval('P1D'));
    	}

    	return $data;
    }

    public function getAllData($start, $end)
    {
        $tp = $this->profile->getTrainingProfile()->select("id")->fetch();
        $training_profile_id = ($tp != false) ? $tp->id : false;
        $result = $this->db->fetchAll("
				select
					t.*,
					case when ts.training_id is null then false else true end prihlaseny_na_trening
				from view_training t
				left join trainees ts on ts.training_id = t.id and ts.training_profile_id = ?
				where termin_od between ? and ?
        	", $training_profile_id, $start, $end);

    	return $result;
    }



	public function getTrainees($training_id)
	{
		$sql = '
			SELECT tr.training_id, tr.reservation_type, u.login, u.id user_id, u.meno, u.priezvisko, u.season_ticket_id, u.season_ticket_valid_until
			FROM trainees tr
			join view_user u on u.profile_id = tr.training_profile_id
			where training_id = ?';

		return $this->db()->fetchAll($sql, $training_id);
	}


    public function getCronTrenerData($datum, $cas_od, $cas_do)
    {

	    $result = $this->db->fetchAll("select
					t.trening_id,
					tt.name nazov,
					t.max_pocet_cvicencov,
					c.cvicencov,
					uc.cvicenci,
					t.datum,
					t.cas,
					t.cas_do,
					concat(u.meno, ' ', u.priezvisko) trener,
					u.email trener_email,
					concat(ua.meno, ' ', ua.priezvisko) asistent,
					ua.email asistent_email
					FROM trening t
					join trening_type tt on tt.id = t.trening_type
					left join (
						select trening_id, count(trening_id) cvicencov from trening_cvicenci
						group by trening_id
						) c on c.trening_id = t.trening_id
					left join (
				        select
						    tc.trening_id,
							tc.user_id,
							GROUP_CONCAT(CONCAT(u.meno, ' ', u.priezvisko) SEPARATOR ', ') cvicenci
						from trening_cvicenci tc
				        join oxs_users u ON u.user_id = tc.user_id
						group by tc.trening_id
						) uc on uc.trening_id = t.trening_id
					join oxs_users u on u.user_id = t.trener_id
					left join oxs_users ua on ua.user_id = t.asistent_id
					where datum = ?
					and zruseny = 0
					and cas between ? and ?
					order by cas asc
	    	", $datum, $cas_od, $cas_do);

		return $result;
    }


	public function saveNewTraining($startdate, $time_id, $trening_nazov)
	{
		// ziskame z nazvu treningu jeho trening_type_id
		$trening_type = $this->trening_type->find()->where('nazov = ?', $trening_nazov)->fetch();

		// ziskame casy podla time_id
		$trening_times = $this->trening_times->find()->wherePrimary($time_id)->fetch();




		// najprv si zistime, ci takyto trening uz existuje
		$sql = "
            SELECT id FROM training t
            where DATE_FORMAT(termin_od, '%Y-%m-%d') = ? and training_times_id = ? and zruseny = 0
            ";
		$exist = $this->db()->fetch($sql, $startdate, $time_id);

		// ak ano, tak mu iba zmenime typ treningu
		if ($exist != false)
		{
			$this->find()->wherePrimary($exist->id)->update(array(
				'training_type_id' => $trening_type->id)
			);

			return;
		}





		// teraz si zistime, ci sa takyto trening uz minuly tyzden o takomto case opakoval
		// ak ano, tak vytiahneme predchadzajuce meta data o treningu a pouzijeme ich aj pri tomto
		$sql = "
            SELECT max_pocet, kredity, trener_user_id, asistant_user_id
            FROM training t
            where DATE_FORMAT(termin_od, '%Y-%m-%d') = DATE_ADD(?, INTERVAL -7 DAY)
            and training_times_id = ?
            ";
		$last = $this->db()->fetch($sql, $startdate, $time_id);

		// ak nie, tak nastavime taketo defaultne
		if ($last == false)
		{
			$last = array(
				'max_pocet' => 10,
				'kredity' => 10,
				'trener_user_id' => 11,
				'asistant_user_id' => null,
			);
		}



		$data = array_merge(
			(array) $last,
			array(
			'termin_od' => $startdate.' '.$trening_times->cas_od.':00',
			'termin_do' => $startdate.' '.$trening_times->cas_do.':00',
			'training_type_id' => $trening_type->id,
			'training_times_id' => $time_id,
			'zruseny' => 0
		));

		$this->table()->insert($data);
	}


	public function cancelTraining($training_id, $cancelSingleTraining = false)
	{
		// cvicenci
		$trainees = $this->getTrainees($training_id);

		// vytiahneme trening
		$trening = $this->db()->fetch('select * from view_training where id = ?', $training_id);

		foreach ($trainees as $trainee)
		{
			// uzivatelom vratime kredity
			if ($trainee->reservation_type == 1) {
				$this->training_profile->addKredity($trainee->user_id, $trening->kredity);
			} else if ($trainee->reservation_type == 3) {
				$this->training_profile->addKredity($trainee->user_id, 5);
			}

			// rozosleme email
			$subject = "[NWNS Academy] Tréning $trening->nazov ({$trening->datum_od->format('d.m.Y')}) bol zrušený";

			$body = "
                        Ahoj {$trainee->meno},<br/><br/>
						S ľútosťou ti oznamujeme, že tréning <b>{$trening->nazov}</b>, ktorý sa mal konať dňa <i>{$trening->datum_od->format('d.m.Y')}</i> o <i>{$trening->termin_od->format('H:i')}</i>, bol zrušený!
						Ospravedlňujeme sa za nepríjemnosti a prajeme ti príjemný zvyšok dňa.<br/><br/>
						S pozdravom<br/>
						Team No Will No Skill Academy<br />";
			
			$this->sendgrid_email->sendEmail($trainee->login, ['name' => 'NWNS RS', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
		}

		// oznacime trening za zruseny
		if ($cancelSingleTraining) 
		{
			$this->find()->wherePrimary($training_id)->update(array(
				'zruseny' => true,
				'zrusena_instancia' => true
			));
		} else {
			$this->find()->wherePrimary($training_id)->update(array(
				'zruseny' => true
			));
		}		
	}
	
	public function checkTrainingExists($startdate, $time_id) {
		$sql = "
            SELECT id FROM training t
            where DATE_FORMAT(termin_od, '%Y-%m-%d') = ? and training_times_id = ? and zruseny = 0
            ";
		$exist = $this->db()->fetch($sql, $startdate, $time_id);
		
		return $exist;
	}


	public function renewSchedule(\DateTime $date)
	{
		$start = \Components\AdminKalendarControl::getWeekStart($date);
		$next_start = clone $start;
		$next_start->add(new \DateInterval("P6D"));

		$treningy = $this->db()->fetchAll("
			SELECT *
			FROM training t
			where (zruseny = 0 OR zrusena_instancia = 1) and date_format(termin_od, '%Y-%m-%d') between ? and ?
		", $start->format('Y-m-d'), $next_start->format('Y-m-d'));

		foreach ($treningy as $trening)
		{
			// id nepotrebujeme
			unset($trening->id);

			// posunieme termin treningu o tyzden dopredu v rovnaky cas
			$trening->termin_od->add(new \DateInterval("P7D"));
			$trening->termin_do->add(new \DateInterval("P7D"));

			$exist = $this->db()->fetch("
				SELECT id FROM view_training t
				where zruseny = 0 and termin_od = ?", $trening->termin_od);

			// ak na danom mieste v buducnosti uz mame nieaky trening vytvoreni, tak ho neprepisujeme
			if ($exist != false)
			{
				continue;
			}

			// ulozime novy trening
			$trening->zruseny = false;
			$trening->zrusena_instancia = false;

			$this->table()->insert($trening);
		}

	}
	
	public function getNextTrainings($fromDateTime, $trainings, $trainingCount) {
		$conditions = [];
		foreach ($trainings as $training) {
			$conditions[] = "((DAYOFWEEK(termin_od) = " . ($training->day + 2) . ") AND (training_times_id = " . $training->timeId . "))";
		}
		$query = "SELECT *
			FROM training t
			where (termin_od > '" . $fromDateTime . "') AND (" . implode(" || ", $conditions) . ") AND (zruseny = 0) ORDER BY termin_od ASC LIMIT " . $trainingCount;
		
		//predlzujem kalendar, kym nemam dost treningov na prihlasenie
		$lastCount = 0;
		while ($lastCount < $trainingCount) {
			$treningy = $this->db()->fetchAll($query);
			$lastCount = count($treningy);
			if ($lastCount >= $trainingCount) break;	
			$lastTraining = end($treningy);
			$this->renewSchedule($lastTraining->termin_od);
		}
		return $treningy;
	}
}