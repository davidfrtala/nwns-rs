<?php
namespace App\Models;
use Tracy\Debugger;

/**
 * Created by PhpStorm.
 * User: menthor
 * Date: 9.3.16
 * Time: 16:23
 */
class ConvertKit extends \Nette\Object
{
    /** @var string  */
    private $url;

    /** @var string  */
    private $mode;
	
	/** @var string  */
    private $apiKey;
	
	/** @var string  */
    private $formId;
	
	/** @var string  */
    private $tags;

    const PROD  = 'production';
    const DEVEL = 'development';



    /**
     * @param string $url
     * @param string $apiKey
     * @param string $formId
     * @param string $mode
     */
    public function __construct($url, $apiKey, $mode = NULL)
    {
        $this->url = $url;
		$this->apiKey = $apiKey;
		$this->formId = NULL;
		$this->tags = NULL;
        $this->mode = $mode ? $mode : self::PROD;
    }

	/**
     * @return string
     */
    private function buildUrl()
    {
		if ($this->formId == NULL) {
			throw new \Exception("Nebolo špecifikované ID formuláru");
		}
        return sprintf($this->url, $this->formId, $this->apiKey);
    }
 
    /**
     * @param string $formId
     * @return void
     */
    public function setFormId($formId)
    {
        $this->formId = $formId;
    }
	
	/**
     * @param string $tags
     * @return void
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @param string $email
     * @param string $name
     * @return void
     */
    public function subscribe($email, $name, $formId = NULL)
    {
        if ($this->mode === self::DEVEL)
            return;


        $postfields = array(
            'email' => $email,
            'name' => $name
        );
		
		if ($this->tags != NULL) $postfields['tags'] = $this->tags;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->buildUrl());
        curl_setopt($ch, CURLOPT_POST, count($postfields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = json_decode(curl_exec($ch));

        if (array_key_exists('error', $server_output))
        {
            Debugger::log(array($postfields, $server_output));
        }

        curl_close ($ch);
    }
}