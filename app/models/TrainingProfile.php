<?php
namespace App\Models;

class TrainingProfile extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'training_profile';

	/**
	 * @var Transakcie
	 */
	private $transakcie;

	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(\Nette\Database\Context $db, Transakcie $transakcie)
	{
		parent::__construct($db);
		$this->transakcie = $transakcie;
	}


	public function getTransakcie()
	{
		return $this->transakcie;
	}

	public function isPremiumProfile($user_id) 
	{
		$model = $this->find()->where('user_id = ?', $user_id)->fetch();
		return $model->premium;
	}
	
	public function isDiscountProfile($user_id) 
	{
		$model = $this->find()->where('user_id = ?', $user_id)->fetch();
		return $model->discount;
	}

	public function addKredity($user_id, $kredity)
	{
		$model = $this->find()->where('user_id = ?', $user_id);

		$model->update(array(
			'pocet_kreditov' => ($model->fetch()->pocet_kreditov + $kredity)
		));
	}

	public function minusKredity($user_id, $kredity)
	{
		$model = $this->find()->where('user_id = ?', $user_id);

		$model->update(array(
			'pocet_kreditov' => ($model->fetch()->pocet_kreditov - $kredity)
		));
	}


	/**
	 * Funkcia danemu uzivatelovi predlzi platnost kreditov na dalsich n-mesiacov
	 *
	 * @param $user_id
	 * @param int $next_months
	 */
	public function prolongKredityExpiration($user_id, $next_months = 3)
	{
		$model = $this->find()->where('user_id = ?', $user_id);

		$expiration = new \DateTime("today +$next_months months");
		$model->update(array(
			'platnost_kreditov' => $expiration
		));
	}
}