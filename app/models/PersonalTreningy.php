<?php
namespace App\Models;

use \Exception;

class OsobneTreningy extends BaseModel
{
    /**
     * @var string
     */
    protected $_table = 'personal_training';

    /**
     * @var Profile
     */
    private $profile;

    /**
     * Constructor
     *
     * @param	Profile	$profile
     * @param	\Nette\Database\Connection	$db
     */
    public function __construct(Profile $profile, \Nette\Database\Context $db)
    {
        parent::__construct($db);

        $this->profile = $profile;
    }

}