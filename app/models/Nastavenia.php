<?php
namespace App\Models;

class Nastavenia extends BaseModel
{
    /**
     * @var string
     */
    protected $_table = 'config';


    /**
     * Constructor
     *
     * @param	\Nette\Database\Connection	$db
     */
    public function __construct(\Nette\Database\Context $db)
    {
        parent::__construct($db);
    }
    
}