<?php
namespace App\Models;


class TreningType extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'training_type';

	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(\Nette\Database\Context $db)
	{
		parent::__construct($db);
	}

}