<?php
namespace App\Models;

use \Exception;
/**
 * Created by PhpStorm.
 * User: menthor
 * Date: 30.9.15
 * Time: 19:03
 */
class RememberMe {
    /** @var null|string */
    private $key = null;

    /** @var  Profile */
    private $profile;

    /**
     * @param string $privatekey
     */
    public function __construct(Profile $user, $privatekey) {
        $this->profile = $user;
        $this->key = pack('H*', $privatekey);
    }

    public function get($user)
    {
        $res = $this->profile->find()->where(array(
            'login = ?' => $user
        ))->fetch();

        return ($res) ? $res->token : false;

    }
    public function set($user, $value)
    {
        $this->profile->find()->where(array(
            'login = ?' => $user
        ))->update(array('token' => $value));
    }



    public function auth() {

        // Check if remeber me cookie is present
        if (! isset($_COOKIE["auto"]) || empty($_COOKIE["auto"])) {
            return false;
        }

        // Decode cookie value
        if (! $cookie = @json_decode($_COOKIE["auto"], true)) {
            return false;
        }

        // Check all parameters
        if (! (isset($cookie['user']) || isset($cookie['token']) || isset($cookie['signature']))) {
            return false;
        }

        $var = $cookie['user'] . $cookie['token'];

        // Check Signature
        if (! $this->verify($var, $cookie['signature'])) {
            throw new Exception("Cokies has been tampared with");
        }

        // Check Database
        $info = $this->get($cookie['user']);
        if (! $info) {
            return false; // User must have deleted accout
        }

        // Check User Data
        if (! $info = json_decode($info, true)) {
            throw new Exception("User Data corrupted");
        }

        // Verify Token
        if ($info['token'] !== $cookie['token']) {
            throw new Exception("System Hijacked or User use another browser");
        }

        /**
         * Important
         * To make sure the cookie is always change
         * reset the Token information
         */

        $this->remember($info['user']);
        return $info;
    }

    public function remember($user) {
        $cookie = [
            "user" => $user,
            "token" => $this->getRand(64),
            "signature" => null
        ];
        $cookie['signature'] = $this->hash($cookie['user'] . $cookie['token']);
        $encoded = json_encode($cookie);

        // Add User to database
        $this->set($user, $encoded);

        /**
         * Set Cookies
         * In production enviroment Use
         * setcookie("auto", $encoded, time() + $expiration, "/~root/",
         * "example.com", 1, 1);
         */
        $expiration = time() + (10 * 365 * 24 * 60 * 60);
        setcookie("auto", $encoded, $expiration, '/');
    }


    public function remove($user)
    {
        $this->set($user, null);

        if (isset($_COOKIE['auto']))
        {
            unset($_COOKIE['auto']);
            setcookie("auto", null, -1, '/');
        }
    }

    public function verify($data, $hash) {
        $rand = substr($hash, 0, 4);
        return $this->hash($data, $rand) === $hash;
    }

    private function hash($value, $rand = null) {
        $rand = $rand === null ? $this->getRand(4) : $rand;
        return $rand . bin2hex(hash_hmac('sha256', $value . $rand, $this->key, true));
    }

    private function getRand($length) {
        switch (true) {
            /*case function_exists("mcrypt_create_iv") :
                $r = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
                break;*/
            case function_exists("openssl_random_pseudo_bytes") :
                $r = openssl_random_pseudo_bytes($length);
                break;
            case is_readable('/dev/urandom') : // deceze
                $r = file_get_contents('/dev/urandom', false, null, 0, $length);
                break;
            default :
                $i = 0;
                $r = "";
                while($i ++ < $length) {
                    $r .= chr(mt_rand(0, 255));
                }
                break;
        }
        return substr(bin2hex($r), 0, $length);
    }
}