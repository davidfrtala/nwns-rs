<?php
namespace App\Models;

class Clanky extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'clanky';

	public function add($values)
	{
            
            $insert = array(
                'nazov' => $values['nazov'],
                'image_url' => $values['image_url'],
                'galery_id' => 0,
            );
            $image = $this->db->table('images')->insert($insert);
            $image_id = $this->db->getInsertId();
            unset($insert);    
            
            $category_id = $values['category_id'];
            if($values['category_id'] == 0){
                $category_id = null;
            }
            
            $insert = array(
                'nadpis' => $values['nadpis'],
                'description' => $values['description'],
                'text' => $values['text'],
                'user_id' => $values['user_id'],
                'categories_id' => $category_id,
                'image_id' => $image_id
            );
            
            return $this->table()->insert($insert);
	}
        
        public function fetchClanky(){
            return $this->db->fetchAll("SELECT clanky.*,images.nazov,images.image_url,invoice_detail.meno,invoice_detail.priezvisko FROM clanky 
                    LEFT JOIN images ON clanky.image_id = images.id
                    LEFT JOIN user ON clanky.user_id = user.id
                    LEFT JOIN invoice_detail ON user.id = invoice_detail.user_id 
                    ");
        }
        
        public function fetchClanok($id){
            return $this->find()->where('id',$id);
        }
}