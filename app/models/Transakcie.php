<?php
namespace App\Models;

use Nette\Utils\DateTime;

class Transakcie extends BaseModel
{

	/**
	 * Transaction type pre prihlasenie na trening ma ID 1
	 *
	 * @var integer
	 */
	const TRENING_PRIHLASENIE_TID = 1;

	/**
	 * Transaction type pre odhlasenie z treningu ma ID 2
	 *
	 * @var integer
	 */
	const TRENING_ODHLASENIE_TID = 2;

	/**
	 * Transaction type pre nakup skillierov ma ID 3
	 *
	 * @var integer
	 */
	const NAKUP_SKILLIEROV_TID = 3;

	/**
	 * Transaction type pre pridanie cerveneho bodu ma ID 5
	 *
	 * @var integer
	 */
	const PRIDANIE_CERVENEHO_BODU = 5;
	
	/**
	 * @var string
	 */
	protected $_table = 'transactions';


	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(Kredity $kredity, \Nette\Database\Context $db)
	{
		parent::__construct($db);
	}


	public function getGridData(){
		return $this->db()->table('view_transactions');
	}
        
	/**
	 *
	 *
	 * @param	integer	$user_id
	 * @return	array
	 */
	public function getDataByUser($user_id)
	{
        $result = $this->db()->table("view_transactions")->where('user_id', $user_id);

		return $result;
	}

        
	public function saveData($values){
		$transakcia = $this->table()->insert(array(
		'user_id' 	=> $values['user_id'],
		'target_id' 	=> $values['target_id'],
		'transaction_type_id'	=> $values['transaction_type_id'],
		'date_created'	=> new \DateTime()
	));
		return $transakcia;
	}


	public function addKreditTransaction($user_id, $credits_id)
	{
		$this->addTransaction($user_id, $credits_id, self::NAKUP_SKILLIEROV_TID);
	}


	public function addTransaction($user_id, $target_id, $transaction_type)
	{
		$this->table()->insert(array(
			'date_created' => new DateTime(),
			'user_id' => $user_id,
			'transaction_type_id' => $transaction_type,
			'target_id' => $target_id,
		));
	}


	public function getChartData($userId)
	{
		$sql = 'select date_format(vt.termin_od, "%c") mesiac, count(vt.nazov) pocet from trainees ts
join training_profile tp on tp.id = ts.training_profile_id and tp.user_id = ?
join view_training vt on vt.id = ts.training_id
where year(termin_od) = year(now())
group by date_format(vt.termin_od, "%m")
order by 1 asc';

		$res = $this->db()->fetchAll($sql, $userId);

		$result = array();
		for ($i=1; $i<=12; $i++)
		{
			// vynulujeme vsetky data pre graf
			$result[] = array($i, 0);
		}

		foreach ($res as $row)
		{
			$result[$row['mesiac']-1] = array((int) $row['mesiac'], $row['pocet']);
		}

		return $result;
	}

}