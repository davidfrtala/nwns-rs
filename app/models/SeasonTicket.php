<?php
namespace App\Models;

use \Nette\Database\Table\Selection,
	\Exception;

class SeasonTicket extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'season_tickets';


	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(\Nette\Database\Context $db)
	{
		parent::__construct($db);
	}
}