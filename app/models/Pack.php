<?php
namespace App\Models;

use \Nette\Database\Table\Selection,
	\Exception,
	DateTime;

class Pack extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'packs';

	/**
	 * @var Treningy
	 */
	private $treningy;
	
	/**
	 * @var Kredity
	 */
	private $kredity;

	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(Treningy $tr, Kredity $kr, \Nette\Database\Context $db)
	{
		parent::__construct($db);
		
		$this->treningy = $tr;
		$this->kredity = $kr;
	}
	
	public function applyTrainingPack($userId, $pack) 
	{
		$trainings = json_decode($pack->trainings);
		
		if ($pack->start_date != NULL) {
			$nextTrainingDate = $pack->start_date;
			//najdem najblizsi trening z balicku, pripadne predlzim kalendar
			$treningy = $this->treningy->getNextTrainings($nextTrainingDate, $trainings, $pack->training_count);
			//prihlasim usera na treningy
			$deaktivovatBalicky = false;
			foreach ($treningy as $trening) {
				if ($trening->termin_od < new DateTime()) continue;
				$plnyTrening = $this->treningy->reserveTrening($trening->id, 3, $userId);
				if ($plnyTrening) $deaktivovatBalicky = true;
			}
			
			//ak sa naplnila kapacita treningu, nastavim produkty obsahujuce balicek ako neaktivne
			if ($deaktivovatBalicky) {
				$kredity = $this->kredity->db()->fetchAll("SELECT * FROM credits c where pack_id = " . $pack->id);
				foreach ($kredity as $produkt) {
					$this->kredity->find()->wherePrimary($produkt->id)->update(array('aktivny' => false));
				}
			}
		}
	}
	
	public function disenrollTrainingPack($userId, $pack)
	{
		$trainings = json_decode($pack->trainings);
		if ($pack->start_date != NULL) {
			$nextTrainingDate = $pack->start_date;
			$treningy = $this->treningy->getNextTrainings($nextTrainingDate, $trainings, $pack->training_count);
			foreach ($treningy as $trening) {
				if ($trening->termin_od < new DateTime())
				{
					continue;
				}
				$plnyTrening = $this->treningy->unreserveCourseTrening($trening->id, $userId);
			}
			$kredity = $this->kredity->db()->fetchAll("SELECT * FROM credits c where pack_id = " . $pack->id);
			foreach ($kredity as $produkt) {
				$this->kredity->find()->wherePrimary($produkt->id)->update(array('aktivny' => true));
			}
		}
	}
}