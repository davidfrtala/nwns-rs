<?php
namespace App\Models;

use \Nette\Database\Table\Selection,
	\Exception;

class InvoiceDetail extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'invoice_detail';


	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(\Nette\Database\Context $db)
	{
		parent::__construct($db);
	}
}