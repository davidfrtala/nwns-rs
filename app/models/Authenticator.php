<?php
namespace App\Models;
use Nette\Security as NS;

class Authenticator extends \Nette\Object implements NS\IAuthenticator
{
    /**
     * @var Identity
     */
    private $_identity;

    /**
     * @var \Nette\Database\Context
     */
    private $db;

    /**
     * @var PasswordHelper
     */
    private $password;


    /**
     * Konstruktor
     *
     * @param string $pwsalt
     * @param \Nette\Database\Context $db
     */
    public function __construct(PasswordHelper $pwd, \Nette\Database\Context $db)
    {
        $this->password = $pwd;
        $this->db = $db;
    }



    public function getIdentity()
    {
        return $this->_identity;
    }



    public function setIdentity(NS\Identity $identity)
    {
        $this->_identity = $identity;
    }



    public function authenticate(array $credentials)
    {
        @list($user_email,$user_password) = $credentials;

        $user_password = $this->password->hash($user_password);

        $row = $this->db->table('user')->where('login', $user_email)->fetch();

        if (!$row) {
            throw new NS\AuthenticationException('Užívateľ nebol nájdeny!',404);
        }

        if ($row->aktivny == 0) {
            throw new NS\AuthenticationException('Toto konto ešte nie je aktívne. Klikni na aktivačný link ktorý bol zaslaný na tvoj email!',403);
        }

        if ($user_password !== $row->password && count($credentials) == 2) {
            throw new NS\AuthenticationException('Nesprávne heslo!',403);
        }

        $identity = new NS\Identity($row->id, $row->ref('role')->nazov);
        $identity->login = $row->login;
        $identity->role = $row->ref('role')->nazov;
        $this->setIdentity($identity);

        return $identity;
    }
}