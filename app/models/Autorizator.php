<?php
namespace App\Models;
/**
 * Created by PhpStorm.
 * User: Lord Dave
 * Date: 01/11/2015
 * Time: 15:47
 */
use Nette\Security\IAuthorizator;
use Nette\Security\Permission;

class Autorizator implements IAuthorizator
{
    /** @var Permission */
    private $acl = NULL;

    const ADMIN             = 'Administrátor';
    const TRENER             = 'Tréner';
    const CVICENEC             = 'Cvičenec';

    /**
     * Konstruktor inicializuje ACL list
     */
    public function __construct()
    {
        $this->acl = new Permission();

        $this->acl->addRole(self::ADMIN);
        $this->acl->addRole(self::TRENER);
        $this->acl->addRole(self::CVICENEC);
        $this->acl->addRole('guest');

        // pridame Admin kontrolery do zdrojov
        $this->acl->addResource('Admin:Authenticate');
        $this->acl->addResource('Admin:Config');
        $this->acl->addResource('Admin:Default');
        $this->acl->addResource('Admin:Kredity');
        $this->acl->addResource('Admin:Orders');
        $this->acl->addResource('Admin:PersonalTrainings');
        $this->acl->addResource('Admin:Produkty');
        $this->acl->addResource('Admin:Stats');
        $this->acl->addResource('Admin:Trainings');
        $this->acl->addResource('Admin:Transakcie');
        $this->acl->addResource('Admin:TreningTimes');
        $this->acl->addResource('Admin:TreningType');
        $this->acl->addResource('Admin:Users');

        $this->acl->addResource('Homepage');
        $this->acl->addResource('Authenticate');
        $this->acl->addResource('Error');

        // administrator moze komplet vsetko
//        $this->acl->allow(self::ADMIN);

        // spravca uvidi komplet vsetko
        $this->acl->allow(self::TRENER);
        $this->acl->deny(self::TRENER, Permission::ALL, 'delete');


        // zberac
//        $this->acl->allow(self::TRENER, array('Index', 'Nastavenia', 'Prevadzky', 'Databaza'), array('index'));
//        $this->acl->allow(self::TRENER, 'Prevadzky', 'show');
//        $this->acl->allow(self::TRENER, 'Databaza', array('vsetky', 'prevadzka'));
//
//        // obchodny zastupca
//        $this->acl->allow(self::TRENER, array('Index', 'Nastavenia', 'Prevadzky', 'Databaza'), array('index'));


        // implicitny Guest
        $this->acl->allow('guest', 'Homepage');

        // vsetky role mozu vidiet error page a prihlasenie
        $this->acl->allow(Permission::ALL, array(
            'Admin:Authenticate',
            'Error'
        ));

    }



    /**
     *
     * @param type $role
     * @param IResource $resource
     * @param type $privilege
     * @return boolean
     */
    public function isAllowed($role, $resource, $privilege)
    {
        // specialny pripad, ked je nasim zdrojom custom model
        if (is_object($resource))
        {
            if ($resource instanceof \Nette\Security\IResource)
            {
                return $resource->getResourceId();
            } else {
                throw new \Nette\InvalidArgumentException('Resource musi byt objekt typu IResource');
            }
        }

        return $this->acl->isAllowed($role, $resource, $privilege);
    }


}