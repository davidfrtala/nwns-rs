<?php
namespace App\Models;

use Nette;

class PayPal {

	const METHOD_SET = 'SetExpressCheckout';
	const METHOD_PAY = 'DoExpressCheckoutPayment';

	//public $mode 				= 'sandbox'; // sandbox or live
	//private $api_username 		= 'contact-facilitator_api1.nowillnoskillacademy.com'; //PayPal API Username
	//private $api_password 		= '1391000072'; //Paypal API password
	//private $api_signature 		= 'Aq8NVyrN5n.5L0c511uxttBZ.2wyApMPaYdgvlEExhZOresHCpKQCF50'; //Paypal API Signature
	// live credentials
	public $mode 				= 'live'; // sandbox or live
	private $api_username 		= 'contact_api1.nowillnoskillacademy.com'; //PayPal API Username
	private $api_password 		= '4MM26KMRYGCWKQHK'; //Paypal API password
	private $api_signature 		= 'Ai1PaghZh5FmBLCDCTQpwG8jB264AXouYVIQoxkHO6q9fcxpcinWYGeV'; //Paypal API Signature

	private $currency_code 		= 'EUR'; //Paypal Currency Code
	public $return_url 		= null; //Point to process.php page
	public $cancel_url 		= null; //Cancel URL if user clicks cancel

	public function getApiUsername()
	{
		return $this->api_username;
	}

	public function getApiPasswors()
	{
		return $this->api_password;
	}

	public function getApiSignature()
	{
		return $this->api_signature;
	}

	public function getCurrencyCode()
	{
		return $this->currency_code;
	}
	public function getReturnUrl()
	{
		return $this->return_url;
	}
	public function getCancelUrl()
	{
		return $this->cancel_url;
	}



	public function expressCheckout($method_name, $data_query)
	{
		if ($method_name != self::METHOD_PAY && $method_name != self::METHOD_SET)
		{
			throw new \Exception("Nesprávna konfigurácia PayPal príkazu!");
		}

		// Set up your API credentials, PayPal end point, and API version.
		$API_UserName = urlencode($this->api_username);
		$API_Password = urlencode($this->api_password);
		$API_Signature = urlencode($this->api_signature);

		// live or sandbox
		if($this->mode =='sandbox')
			$paypalmode 	=	'.sandbox';
		else
			$paypalmode 	=	'';

		$API_Endpoint = "https://api-3t".$paypalmode.".paypal.com/nvp";
		$version = urlencode('76.0');

		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=$method_name&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$data_query";

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		// Get response from the server.
		$httpResponse = curl_exec($ch);

		if(!$httpResponse) {
			exit("$method_name failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}

		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}

		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}

		return $httpParsedResponseAr;
	}


	/**
	 * Vysklada paypal data query
	 *
	 * @param integer
	 * @param string
	 * @param float
	 * @param float
	 * @param integer
	 * @return string
	 */
	public function getDataQuery($number, $name, $price, $total_price, $Qty)
	{
        // dataset
        $padata =   '&CURRENCYCODE='.urlencode($this->getCurrencyCode()).
                    '&PAYMENTACTION=Sale'.
                    '&ALLOWNOTE=1'.
                    '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($this->getCurrencyCode()).
                    '&PAYMENTREQUEST_0_AMT='.urlencode($total_price).
                    '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($total_price).
                    '&L_PAYMENTREQUEST_0_QTY0='. urlencode($Qty).
                    '&L_PAYMENTREQUEST_0_AMT0='.urlencode($price).
                    '&L_PAYMENTREQUEST_0_NAME0='.urlencode($name).
                    '&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($number).
                    '&AMT='.urlencode($total_price).
                    '&RETURNURL='.urlencode($this->getReturnURL() ).
                    '&CANCELURL='.urlencode($this->getCancelURL());
		return $padata;
	}
        
        public function getDataQueryExecute($number, $name, $price, $total_price, $Qty, $token, $payer_id)
	{
        // dataset
        $padata =   '&TOKEN='.urlencode($token).
                    '&PAYERID='.urlencode($payer_id).
                    '&ALLOWNOTE=1'.
                    '&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
                    //set item info here, otherwise we won't see product details later  
                    '&L_PAYMENTREQUEST_0_NAME0='.urlencode($name).
                    '&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($number).
                    '&L_PAYMENTREQUEST_0_AMT0='.urlencode($price).
                    '&L_PAYMENTREQUEST_0_QTY0='. urlencode($Qty).
                    '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($total_price).
                    '&PAYMENTREQUEST_0_AMT='.urlencode($total_price).
                    '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($this->getCurrencyCode());
		return $padata;
	}

}
?>