<?php
namespace App\Models;

use	\Nette\Security\User;

class PasswordHelper extends \Nette\Object
{
    /**
     * @var string
     */
    private $salt;

    /**
     * @var \Nette\Database\Context
     */
    private $db;



    /**
     * Konstruktor
     *
     * @param string $pwsalt
     * @param \Nette\Database\Context $db
     */
    public function __construct($pwsalt, \Nette\Database\Context $db)
    {
        $this->salt = $pwsalt;
        $this->db = $db;
    }



    /**
     *
	 * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }



	/**
	 * V pripade strateneho hesla, chceme vygenerovat nove
	 *
	 * @param string
	 * @param integer
	 * @throws \Exception
	 * @return string
	 */
	public function regeneratePassword($email, $length = 8)
	{
		$row = $this->db->table('user')->where('login', $email)->fetch();

		if ($row == false) {
			throw new \Exception("K zadanej emailovej adrese neexistuje žiadne užívateľské konto!");
		}

	    $new_pwd='';
		$moznosti='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

	    while(strlen($new_pwd) < $length) {
	    	$new_pwd .= substr($moznosti, mt_rand(0, strlen($moznosti) - 1), 1);
	    }

		return $new_pwd;
	}



	/**
	 * Zaheshuje osoleny string
	 *
	 * @param string
	 * @return string
	 */
    public function hash($password)
    {
		return md5($this->salt . $password);
    }
}