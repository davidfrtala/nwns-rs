<?php
namespace App\Models;

use Nette\Utils\Html;

class Kredity extends BaseModel
{
	/**
	 * @var string
	 */
	protected $_table = 'credits';


	/**
	 * Constructor
	 *
	 * @param	\Nette\Database\Connection	$db
	 */
	public function __construct(\Nette\Database\Context $db)
	{
		parent::__construct($db);
	}

	public function getCredits($id) {
		$credits = $this->db()->table('view_credits')->where(array(
                'id = ?' => $id
            ))->fetch();
		return $credits;
	}

	/**
	 *
	 *
	 * @return	array
	 */
	public function fetchSkilliers($premium = TRUE, $discount = TRUE)
	{
		$result = array();

		$rows = $this->find()->where('aktivny', 1)->order('nazov ASC');
		if (!$discount) {
			$rows = $rows->where('discount = 0 AND premium_only = 0');
		} else if (!$premium) {
			$rows = $rows->where('premium_only = 0');
		}

		foreach ($rows as $row)
		{
			$result[$row->id] = Html::el('option')->value($row->id)->setHtml($row->nazov . ' (' . $row->cena . '.00 Eur)')->data('price', $row->cena);
			//$result[$row->id] = ;
		}

		return $result;
	}
}