<?php
namespace App\Models;

use \Components\KalendarControl,
	\Exception,
	\Nette\Mail\Message, 
	\Nette\Mail\SendmailMailer,
	Tracy\Debugger;

class SendgridEmail extends BaseModel
{
	
	/** @var string  */
    private $apiKey;

	/** @var bool  */
    private $useSendGrid;
	
	/*
	* @param string $apiKey
	*/
	public function __construct($apiKey, $useSendGrid)
	{
		$this->apiKey = $apiKey;
		$this->useSendGrid = $useSendGrid;
	}
	
	public function sendEmail($recipients, $fromAddress, $subject, $body, $encoding = "text/html") 
	{
		if ($this->useSendGrid) {
			try {
				require("../vendor/sendgrid/sendgrid-php.php");

				$from = new \SendGrid\Email($fromAddress['name'], $fromAddress['email']);
				$to = $recipients;
				$content = new \SendGrid\Content($encoding, $body);
				$mail = new \SendGrid\Mail($from, $subject, $to, $content);
				
				$sg = new \SendGrid($this->apiKey);

				$response = $sg->client->mail()->send()->post($mail);
				if ($response->statusCode() != 202) {
					throw new \Exception('Pri odosielaní emailu došlo k chybe');
				}
			} catch (\Exception $e) {
				Debugger::log($e);
			}
		} else {
			$mail = new Message;
			$mail->setFrom($fromAddress['name'] . ' <' . $fromAddress['email'] . '>')
				->addTo($recipients)
				->setSubject($subject)
				->setHTMLBody($body);

			$mailer = new SendmailMailer;
			$mailer->send($mail);
		}
	}
}