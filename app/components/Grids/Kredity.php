<?php
namespace Components\Grids;

class Kredity extends \Grido\Grid
{

    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->setModel($model);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->addColumnText('nazov', 'Názov')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('kredity', 'Kredity')
            ->setSortable()->setFilterText()->setSuggestion();
        $this->addColumnText('cena', 'Cena')
            ->setSortable()->setFilterText()->setSuggestion();
		// $this->addColumnText('season_ticket', 'Permanentka')
			// ->setReplacement(array(
                // "1" => 'Áno',
                // "0" => 'Nie'
            // ))->setSortable();
			
		// $this->addColumnText('season_ticket_duration', 'Trvanie permanentky (v mesiacoch)')
            // ->setReplacement(array(
                // "0" => ''
            // ))->setSortable();
			
		$this->addColumnText('credits_type_id', 'Typ kreditov')
			->setReplacement(array(
                "1" => 'Kredity',
                "2" => 'Permanentka',
				"3" => 'Tréningový balík'
            ))->setSortable();
			
		$this->addColumnText('premium_only', 'Len pre Skalných Skillerov')
			->setReplacement(array(
                "1" => 'Áno',
                "0" => 'Nie'
            ))->setSortable();

        $this->addColumnText('aktivny', 'Aktívny')
            ->setReplacement(array(
                "1" => 'Aktívny',
                "0" => 'Neaktívny'
            ))->setSortable();

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:Kredity:edit', $row->id) ;
        })->setPrimaryKey('id')->setIcon('wrench');
    }
	
	
}