<?php
namespace Components\Grids;

class TreningType extends \Grido\Grid
{

    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->setModel($model);

        $this->addColumnText('nazov', 'Názov')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('aktivny', 'Aktívny')
            ->setReplacement(array(
                "1" => 'Aktívny',
                "0" => 'Neatívny'
            ))->setSortable();

        $this->addColumnText('color', 'Hex farba')
            ->setSortable()->setFilterText()->setSuggestion();

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:TreningType:edit', $row->id) ;
        })->setPrimaryKey('id')->setIcon('wrench');
    }
}