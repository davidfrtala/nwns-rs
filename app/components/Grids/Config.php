<?php
namespace Components\Grids;

class Config extends \Grido\Grid
{

    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->setModel($model);

        $this->addColumnText('nazov', 'Názov')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('premenna', 'Premenná')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('hodnota', 'Hodnota')
            ->setSortable()->setFilterText()->setSuggestion();

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:Config:edit', $row->id) ;
        })->setPrimaryKey('id')->setIcon('wrench');
    }
}