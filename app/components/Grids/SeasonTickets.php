<?php
namespace Components\Grids;

class SeasonTickets extends \Grido\Grid
{

    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->setModel($model);

        $this->addColumnText('name', 'Názov')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('duration', 'Trvanie')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('date_to', 'Koniec')
            ->setSortable()->setFilterText()->setSuggestion();

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:Tickets:edit', $row->id) ;
        })->setPrimaryKey('id')->setIcon('wrench');
    }
	
	
}