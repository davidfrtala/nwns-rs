<?php
namespace Components\Grids;

class Produkty extends \Grido\Grid
{

    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->setModel($model);

        $this->addColumnText('aktivny', 'Akttívny')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('nazov', 'Názov')
            ->setSortable()->setFilterText()->setSuggestion();
        
        $this->addColumnText('kredity', 'Kredity')
            ->setSortable()->setFilterText()->setSuggestion();

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:Produkty:edit', $row->id) ;
        })->setPrimaryKey('id')->setIcon('wrench');
    }
}