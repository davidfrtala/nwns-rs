<?php
namespace Components\Grids;

class OsobneTreningy extends \Grido\Grid
{
    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->setModel($model);

        $this->addColumnText('id', 'ID')->setSortable();

        $this->addColumnText('trener_profile_id', 'Tréner')
            ->setSortable()
            ->setCustomRender(function($item) {
                return $item['trener'];
            });

        $this->addColumnDate('datum', 'Dátum')
            ->setSortable()->setFilterDate();

        $this->addColumnText('time_od', 'Čas od')
            ->setSortable();

        $this->addColumnText('time_do', 'Čas do')
            ->setSortable();

        $this->addColumnText('cvicenec', 'Cvičenec')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('komentar', 'Komentár');


        $res = $this->presenter->model->db()->fetchAll("
            SELECT id, meno, priezvisko FROM view_user
            where rola = 'Tréner' and aktivny = 1
            order by meno asc
            ");
        $select = array('' => '');
        foreach($res as $row)
        {
            $select[$row['id']] = "{$row['meno']} {$row['priezvisko']}";
        }
        $this->addFilterSelect('trener_profile_id', 'Tréner', $select);



        // Moznosti
        $this->addActionHref('edit', '')
            ->setCustomRender(function ($item, $element) {
                $user = $this->presenter->getUser();
                if ($user->isInRole('Administrátor') || $user->getId() === $item->trener_profile_id)
                {
                    return $element;
                }
            })
            ->setCustomHref(function ($row) {
                return $this->parent->link(':Admin:PersonalTrainings:edit', $row->id) ;
        })->setPrimaryKey('id')->setIcon('edit');

    }
}