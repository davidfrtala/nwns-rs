<?php
namespace Components\Grids;

class Transakcie extends \Grido\Grid
{
    private $transakcie;

    public function __construct(\App\Models\Transakcie $model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL, $userId = NULL)
    {
        parent::__construct($parent, $name);
        $this->transakcie = $model;

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $model = $this->transakcie->db()->table('view_transactions')->select('*')->order('date_created DESC');

        if ($userId !== NULL)
        {
            $model->where('user_id', $userId);
        } else {
            $this->addColumnText('uzivatel', 'Užívateľ')
                ->setSortable()->setFilterText()->setSuggestion();
        }

        $this->addColumnDate('date_created', 'Dátum vytvorenia')
            ->setSortable()->setFilterDate();

        $this->addColumnText('transaction_type_id', 'Typ transakcie')
            ->setCustomRender(function($item) {
                return $item->nazov;
            })->setSortable()->setFilterSelect($this->getTypTransakcieList());

        $this->addColumnText('polozka', 'Položka')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('cena', 'Cena')
            ->setSortable()->setFilterText()->setSuggestion();

        // Moznosti
        $this->addActionHref('void', '')
            ->setCustomRender(function ($item, $element) {
                return '';
            });

        $this->setModel($model);
    }

    public function getTypTransakcieList()
    {
        $db = $this->transakcie->db()->table('transaction_type');

        $result = array('' => '');

        foreach($db as $row)
            $result[$row->id] = $row->nazov;

        return $result;
    }
}