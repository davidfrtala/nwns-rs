<?php
namespace Components\Grids;

class Packs extends \Grido\Grid
{

    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->setModel($model);

        $this->addColumnText('name', 'Názov')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('training_count', 'Počet tréningov')
            ->setSortable()->setFilterText()->setSuggestion();
			
		$this->addColumnDate('start_date', 'Dátum začiatku')
            ->setSortable()->setFilterText()->setSuggestion();

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:Packs:edit', $row->id) ;
        })->setPrimaryKey('id')->setIcon('wrench');
    }
	
	
}