<?php
namespace Components\Grids;

class Users extends \Grido\Grid
{
    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $this->setModel($model);
        
        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->addColumnText('id', 'ID')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('full_name', 'Meno')->setCustomRender(function($item) {return ($item->premium == 1) ? '<i title="Skalný Skiller" class="fa fa-shield"></i> ' . $item->full_name : $item->full_name;})
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('login', 'Login')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('aktivny', 'Aktívny')
            ->setSortable()->setReplacement(array(
                '1' => 'aktívny',
                '0' => 'neaktívny'
            ));

        $this->addColumnDate('date_created', 'Dátum vytvorenia')
            ->setSortable()->setFilterDate();

        $this->addFilterSelect('aktivny', 'Aktívny', array(
            '' => '',
            '1' => 'aktívny',
            '0' => 'neaktívny'
        ));

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:Users:show', $row->id);
        })->setPrimaryKey('id')->setIcon('search');
    }
}