<?php
namespace Components\Grids;

class Orders extends \Grido\Grid
{
    private $orders;

    public function __construct(\App\Models\Orders $model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL, $userId = NULL)
    {
        parent::__construct($parent, $name);
        $this->orders = $model;

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $model = $this->orders->db()->table('view_orders')->order('date_order DESC');

        if ($userId !== NULL)
        {
            $model->where('user_id', $userId);
        } else {
            $this->addColumnText('uzivatel', 'Užívateľ')
                ->setSortable()->setFilterText()->setSuggestion();
        }

        $this->addColumnText('variabilny_symbol', 'VS')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('date_order', 'Dátum objednávky')
            ->setCustomRender(function($item) {
                return $item->date_order->format('d.m.Y H:i:s');
            })
            ->setSortable()->setFilterDate();

        $this->addColumnText('platba', 'Platba')
            ->setSortable()->setFilterSelect(array(
                '' => '',
                'prevod' => 'Prevod',
                'paypal' => 'Paypal',
                'hotovost' => 'Hotovosť'
            ));

        $this->addColumnText('nazov', 'Názov')
            ->setSortable()->setFilterText()->setSuggestion();

        $this->addColumnText('date_confirmed', 'Datum potvrdenia')
            ->setCustomRender(function($item) {
                return ($item->date_confirmed) ? $item->date_confirmed->format('d.m.Y H:i:s') : null;
            })
            ->setSortable()->setFilterDate();

        $this->setModel($model);

        // Moznosti
        $this->addActionHref('confirm', '', 'confirm!')->setCustomRender([$this, 'customActionRender'])->setPrimaryKey('id')->setIcon('check');
        $this->addActionHref('cancel', '', 'cancel!')->setCustomRender([$this, 'customActionRender'])->setPrimaryKey('id')->setIcon('ban')->setConfirm("Naozaj chcete zmazať danú objednávku?");
        $this->addActionHref('disenroll', '', 'disenroll!')->setCustomRender([$this, 'isPackCustomActionRender'])->setPrimaryKey('id')->setIcon('minus-circle')->setConfirm("Naozaj chcete používateľa odhlásiť z kurzu?");
    }

    public function customActionRender($item, \Nette\Utils\Html $el)
    {
        if (!$item->date_confirmed)
        {
            return $el;
        } else {
            return '';
        }
    }
    
    public function isPackCustomActionRender($item, \Nette\Utils\Html $el)
    {
    	if ($item->credits_type_id == 3 && $item->date_confirmed /*&& !$item->cancelled*/)
		{
			return $el;
		} else {
			return '';
		}
	}
    
}