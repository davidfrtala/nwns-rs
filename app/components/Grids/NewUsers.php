<?php
namespace Components\Grids;

class NewUsers extends \Grido\Grid
{
    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->setModel($model);

        $this->addColumnText('full_name', 'Meno');

        $this->addColumnDate('date_created', 'Dátum vytvorenia');
		
		$this->addColumnText('odporucil', 'Odporučil');

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:Users:show', $row->id);
        })->setPrimaryKey('id')->setIcon('search');
    }
}