<?php
namespace Components\Grids;

class TreningTimes extends \Grido\Grid
{

    public function __construct($model, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL)
    {
        parent::__construct($parent, $name);

        $customization = new \Grido\Customization($this);
        $this->setCustomization($customization);
        $this->customization->useTemplateBootstrap();

        $this->setModel($model);

        $this->addColumnDate('cas_od', 'Čas od', 'H:i')
            ->setSortable()->setFilterDate();

        $this->addColumnDate('cas_do', 'Čas do', 'H:i')
            ->setSortable()->setFilterDate();
			
		$this->addColumnText('unreserve_deadline', 'Čas na odhlásenie')
            ->setSortable();

        $this->addColumnText('aktivny', 'Aktívny')
            ->setReplacement(array(
                "1" => 'Aktívny',
                "0" => 'Neaktívny'
            ))->setSortable();

        // Moznosti
        $this->addActionHref('edit', '')->setCustomHref(function ($row) {
            return $this->parent->link(':Admin:TreningTimes:edit', $row->id) ;
        })->setPrimaryKey('id')->setIcon('wrench');
    }
}