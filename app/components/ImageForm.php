<?php
namespace Components;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form;

class ImageForm extends Form
{


    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $galeries = $this->parent->galeries->fetchAll();
        
        $this->addSelect('galery_id', 'Galéria', $galeries)
            ->setRequired("Vyberte galériu!");
        
        $this->addButton('pridaj', 'Pridaj');

        $this->addSubmit('submit', 'Potvrď');
        
        $this->onSuccess[] = [$this, 'imageFormSubmitted'];
    }

    public function imageFormSubmitted(Form $form)
    {
        $values = $form->getValues();
        try {
            $new_image = $this->parent->image->add($values);
            $form->setValues(array(), true);
        } catch (\Exception $e) {
            $code = $e->getCode();
            
            if (!empty($code))
                $this->parent->flashMessage("Nepodarilo sa pridať obrázok", "danger");
        }
    }


}