<?php
namespace Components;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form;
use Tracy\Debugger;

class RegisterForm extends Form
{

	/**
     * @inject
     * @var \App\Models\SendgridEmail
     */
    public $sendgrid_email;

    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $this->addText('meno', 'Meno')
            ->setRequired("Meno nesmie byť prázdne!");

        $this->addText('priezvisko', 'Priezvisko')
            ->setRequired("Priezvisko nesmie byť prázdne!");

        $this->addText('login', 'E-mail')
            ->addRule(Form::EMAIL, 'Neplatný tvar e-mailovej adresy')
            ->setRequired("E-mail nesmie byť prázdny!");

        $this->addText('ulica', 'Ulica')
            ->setRequired("Ulica nesmie byť prázdna!");

        $this->addText('mesto', 'Mesto')
            ->setRequired("Mesto nesmie byť prázdne!");

        $this->addText('psc', 'PSČ')
            ->setRequired("PSČ nesmie byť prázdne!");

        $this->addPassword("password", "Heslo")
            ->addRule(Form::FILLED, "Heslo nesmie byť prázdne!");

        $this->addPassword("password2", "Potvrď heslo")
            ->addRule(Form::FILLED, "Potvrdzovacie heslo nesmie byť prázdne!")
            ->addConditionOn($this["password"], Form::FILLED)
                ->addRule(Form::EQUAL, "Hesla se musia zhodovať!", $this["password"]);

        $this->addCheckbox('podmienky', 'Súhlasím s podmienkami')
            ->setRequired("Musíte súhlasiť s podmienkami!");

        $this->addText('odporucil', 'Kto ti nás odporučil?');

        $this->addSubmit('submit', 'Potvrď');

        $this->onSuccess[] = [$this, 'registerFormSubmitted'];

        $this->onValidate[] = function($form) {
            $rows = $this->parent->profile->db()->fetch('SELECT COUNT(*) AS res FROM user WHERE login = ?', $form['login']->value);
            if ($rows->res != 0) {
                $this->addError('Takýto používateľ už je zaregistrovaný. Pokiaľ si zabudol svoje prihlasovacie údaje, prosím kontaktuj administrátora, alebo si údaje prepošli pomocou funkcie (zabudnuté heslo)!');
            }
        };
    }

    public function registerFormSubmitted(Form $form)
    {
        $values = $form->getValues();
        try {
            $this->parent->profile->db()->beginTransaction();

            $new_user = $this->parent->profile->register($values);
            $fa_detail = $new_user->related('invoice_detail')->fetch();

            // odoslat registračny email
            $subject = "[NWNS Academy] Registrácia do rezervačného systému";

            $link = $this->presenter->link('//Authenticate:activate', $new_user->getPrimary(), md5($new_user->login . $new_user->getPrimary()));
            $body = 'Ahoj '.$fa_detail->meno.' <br /><br />
                        Tvoja emailová adresa bola registrovaná v našom systéme. Na to, aby si sa vedel prihlásiť ju ale musíš najskôr potvrdiť. Kliknutím na nasledovný link sa aktivuje tvoj účet:<br /><br />
                        <a href="'.$link.'">POTVRDIŤ MOJU REGISTRÁCIU</a>
                        <br /><br />
                        V prípade, že si sa v našom systéme neregistroval, nereaguj na tento email.<br /><br />
                        S pozdravom, No Will No Skill Academy';
			
			$mailer = new \App\Models\SendgridEmail($this->parent->context->parameters['sendGrid']['apiKey'], $this->presenter->context->parameters['sendGrid']['useSendGrid']);
			$mailer->sendEmail($new_user->login, ['name' => 'NWNS RS', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);

            $this->parent->flashMessage('Bol si úspešne zaregistrovaný. Na tvoju emailovú adresu sme odoslali aktivačný link k tvojmu účtu. Je možné, že sa tento aktivačný e-mail bude nachádzať v spame. V tom prípade ho treba potvrdiť tam.', "success");

            // resetovat formular
            $form->setValues(array(), true);
            $this->parent->profile->db()->commit();
        } catch (\Exception $e) {
            Debugger::log($e);
            $this->parent->profile->db()->rollback();
            $this->parent->flashMessage("Nepodarilo sa registrovať užívateľa. Opakujte neskôr prosím!", "danger");
        }
    }


}