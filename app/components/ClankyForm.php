<?php
namespace Components;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form;

class ClankyForm extends Form
{


    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);
        
        $this->addText('image_url', 'Titulný obrázok:')
            ->setEmptyValue('Cesta k titulnému obrázku')
            ->getControlPrototype()
            ->onfocus('openKCFinder(this)')
            ->onclick('openKCFinder(this)')
            ->ControlType()->readonly = 'readonly';
        
        $this->addText('nazov', 'Názov obrázku')
            ->setRequired("Nadpis nesmie byť prázdny!");
        
        $this->addText('nadpis', 'Nadpis')
            ->setRequired("Nadpis nesmie byť prázdny!");

        $this->addText('description', 'Popis')
            ->setRequired("Popis nesmie byť prázdny!");
        
        $categories = $this->parent->categories->fetchAll();
        array_unshift($categories, 'Vyberte zo zoznamu');
        
        $this->addSelect('category_id', 'Kategória', $categories)
            ->setRequired("Vyberte kategóriu!");

        $this->addTextArea('text', 'Text')
            ->setRequired("Text nesmie byť prázdny!");

        $this->addSubmit('submit', 'Potvrď');
        $this->onSuccess[] = [$this, 'clankyFormSubmitted'];
    }

    public function clankyFormSubmitted(Form $form)
    {
        $values = $form->getValues();
        $values['user_id'] = $this->parent->user->getId();
        try {
            $new_clanok = $this->parent->clanky->add($values);
            $form->setValues(array(), true);
        } catch (\Exception $e) {
            $code = $e->getCode();
            
            if (!empty($code))
                $this->parent->flashMessage("Nepodarilo sa pridať článok", "danger");
        }
    }


}