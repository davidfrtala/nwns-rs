<?php
namespace Components\Forms;

use Nette,
    Nette\Forms\Controls,
    Nette\Application\UI\Form;

class EmailTrainees extends Form
{

    private $model;

    public function __construct($parent, $name, $training_model)
    {
        parent::__construct($parent, $name);
        $this->model = $training_model;

        $this->addHidden('id');

        $this->addTextArea('email', 'Správa');

        $this->addSubmit('submit', 'Odoslať email cvičencom');

        $this->onSuccess[] = [$this, 'submitCallback'];

        \Components\AdminKalendarControl::bootstrapize($this);
    }

    public function submitCallback($form)
    {
        $values = $form->getValues();

        $this->sendMessageToTrainees($values['id'], $values['email']);
    }


    public function sendMessageToTrainees($training_id, $email)
    {
        try {
            $trainees = $this->model->getTrainees($training_id);

            if (!$trainees) {
                $this->getPresenter()->flashMessage("Na tento tréning nieje nikto prihlásený!", 'danger');
                return;
            }

            // odoslat emailu po aktivácií
            $subject = "[NWNS Academy] Hromadná správa";

            foreach ($trainees as $trainee)
            {
                // pozdrav
                $body = 'Ahoj '.$trainee->meno.'<br /><br />';

                // sprava
                $body .= $email;
				
				$mailer = new \App\Models\SendgridEmail($this->getPresenter()->context->parameters['sendGrid']['apiKey'], $this->getPresenter()->context->parameters['sendGrid']['useSendGrid']);
				$mailer->sendEmail($trainee->login, ['name' => 'NWNS RS', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);
            }
            $this->getPresenter()->flashMessage('Správy boli úspešne rozoslané', 'success');

            // vycistime formular
            $this->setValues(array(), true);
        } catch(\Exception $e) {
            Debugger::log($e);
            $this->getPresenter()->flashMessage('Nepodarilo sa vykonať operáciu! Opakujte neskôr prosím', 'danger');
        }
    }
}