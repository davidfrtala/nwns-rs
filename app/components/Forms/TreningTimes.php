<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form;

class TreningTimes extends Form
{
    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $this->addText('cas_od', 'Čas od')
            ->setRequired("Čas od nesmie byť prázdny!");

        $this->addText('cas_do', 'Čas do')
            ->setRequired("Čas od nesmie byť prázdny!");
			
		$this->addText('unreserve_deadline', 'Čas na odhlásenie (v hodinách)')
            ->setRequired("Čas na odhlásenie nesmie byť prázdny!");

        $this->addCheckbox('aktivny', 'Aktívny');

        $this->addSubmit('submit', 'Uložiť');
    }
}