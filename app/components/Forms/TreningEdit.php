<?php

namespace Components;

use Nette\Application\UI,
    Nette\Application\UI\Form,
    Nette\Forms\Controls,
    Tracy\Debugger,
    App\Models;

class TreningEdit extends UI\Control
{

    /** @var Models\Treningy */
    private $treningy;

    /** @var Models\TrainingProfile */
    private $training_profile;

    /** @var integer */
    private $training_id;


	public function __construct(Models\Treningy $model, Models\TrainingProfile $training_profile)
	{
		parent::__construct();

        $this->training_profile = $training_profile;
        $this->treningy = $model;
	}

    public function render()
    {
        $this->template->setFile(__DIR__.'/TreningEdit.latte');

        $this->template->render();
    }

    public function setTrainingDefaults($training_id)
    {
        $this->template->training_id = $this->training_id = $training_id;

        $trening = $this->treningy->db()->fetch('
          select
            vt.id,
            vt.datum_od as termin_od,
            vt.trener_user_id,
            u.aktivny trener_aktivny,
            vt.asistant_user_id,
            vt.training_type_id,
            vt.training_times_id,
            vt.max_pocet,
            vt.kredity,
            vt.zruseny,
            u.role_id as role_id
          from view_training vt
          join user u on u.id = vt.trener_user_id
          where vt.id = ?', $training_id);

        if ($trening['trener_aktivny'] == 0 || $trening['role_id'] == 3)
        {
            $trener = $this->treningy->db()->fetch('select meno, priezvisko from view_user where id = ?', $trening['trener_user_id']);
            $selectBox = $this->getComponent('form')->getComponent('trener_user_id');
            $options = $selectBox->getItems();
            $options[$trening['trener_user_id']] = "{$trener['meno']} {$trener['priezvisko']} (neaktívny)";
            $selectBox->setItems($options);
        }

        $this->getComponent('form')->setDefaults($trening);
        $this->getComponent('emailTrainees')->setDefaults(array('id' => $training_id));
        $this->getComponent('traineeReservation')->setDefaults(array('id' => $training_id));

        $this->template->cvicenci = $this->treningy->getTrainees($training_id);
        $this->template->zruseny = $trening->zruseny;
    }


    public function createComponentEmailTrainees($name)
    {
        return new \Components\Forms\EmailTrainees($this, $name, $this->treningy);
    }


    public function createComponentTraineeReservation($name)
    {
        $control = new \Components\Forms\TraineeRegister($this, $name, $this->treningy, $this->training_profile);

        return $control;
    }


    public function createComponentForm()
    {
        $form = new Form();


        $this->addTrainingType($form);
        $form->addText('termin_od', 'Termín od')
            ->setRequired("Termín nemsie byť prázdny!");
        //$this['termin_od']->getControlPrototype()->addClass("datepicker");

        $this->addTrainingTimes($form);
        $form->addText('kredity', 'Skillierov')
            ->setRequired("Kredity nesmú byť prázdne!");

        $this->addTrainers($form);

        $form->addText('max_pocet', 'Maximálny počet')
            ->setRequired("Maximálny počet nemsie byť prázdny!");

        $form->addHidden('id');

        $form->addSubmit('submit', 'Uložiť');

        $form->onSuccess[] = [$this, 'editTrainingCallback'];
        $form->onValidate[] = function($form) {
            $values = $form->getValues();
            $trenerAktivny = $this->treningy->db()->fetchField('select aktivny from user where id = ?', $values['trener_user_id']);
            if ($trenerAktivny == 0)
            {
                $this->getPresenter()->flashMessage("Pri úprave tréningu bol zvolený tréner s neaktívnym účtom!", "danger");
            }
        };

        \Components\AdminKalendarControl::bootstrapize($form);

        return $form;
    }



    private function addTrainers(Form $form)
    {
        $result = $this->treningy->db()->fetchAll('select * from view_user where rola = ? and aktivny = ?', 'Tréner', 1);

        $opt = array();

        foreach ($result as $row)
        {
            $opt[$row->id] = $row->meno . ' ' . $row->priezvisko;
        }

        $form->addSelect('trener_user_id', 'Tréner', $opt)
            ->setRequired("Tréner nemsie byť prázdny!");

        $form->addSelect('asistant_user_id', 'Asistent trénera', array(0 => 'Žiadny') + $opt);
    }

    private function addTrainingType(Form $form)
    {
        $result = $this->treningy->db()->fetchAll('select * from training_type');

        $opt = array();

        foreach ($result as $row)
        {
            $opt[$row->id] = $row->nazov;
        }

        $form->addSelect('training_type_id', 'Typ tréningu', $opt)
            ->setRequired("Typ tréningu nemsie byť prázdny!");
    }

    private function addTrainingTimes(Form $form)
    {
        $result = $this->treningy->db()->fetchAll('select * from training_times');

        $opt = array();

        foreach($result as $row)
        {
            $opt[$row->id] = $row->cas_od . ' - ' . $row->cas_do;
        }

        $form->addSelect('training_times_id', 'Čas tréningu', $opt)
            ->setRequired("Čas tréningu nemsie byť prázdny!");
    }




    public function editTrainingCallback(UI\Form $form)
    {
        $values = $form->getValues(true);
        $training_id = array_pop($values);

        if (empty($values['asistant_user_id']))
        {
            $values['asistant_user_id'] = null;
        }

        try {
            $this->treningy->db()->beginTransaction();

            $this->treningy->find()->get($training_id)->update($values);

            $this->treningy->db()->commit();

            $this->getPresenter()->flashMessage('Tréning bol úspešne upravený!', 'success');
            $this->getPresenter()->restoreRequest($this->getPresenter()->backlink);

        } catch (\PDOException $e) {
            Debugger::log($e);
            $this->treningy->db()->rollBack();
            $this->getPresenter()->flashMessage('Nepodarilo sa upraviť tréning! Opakujte neskôr prosím', 'danger');
        }
    }


    public function handleCancelTraining($training_id)
    {
        try {
            $this->treningy->db()->beginTransaction();

            $this->treningy->cancelTraining($training_id);

            $this->treningy->db()->commit();

            $this->getPresenter()->flashMessage('Tréning bol zrušený! Cvicencom boli vrátene skilliere za tréning.', 'success');
            $this->getPresenter()->redirect('Trainings:');

        } catch (\PDOException $e) {
            Debugger::log($e);
            $this->treningy->db()->rollBack();
            $this->getPresenter()->flashMessage('Nepodarilo sa zrušiť tréning! Opakujte neskôr prosím', 'danger');
        }

    }
}

