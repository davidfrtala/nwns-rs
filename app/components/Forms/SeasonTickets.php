<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form;

class SeasonTickets extends Form
{
	/**
     * @var SeasonTicket
     */
    public $model;
	
    public function __construct($parent, $name, $seasonTickets)
    {
    	parent::__construct($parent, $name);
		$this->model = $seasonTickets;
		
        $this->addText('name', 'Názov')
            ->setRequired("Názov nesmie byť prázdny!");

        $this->addText('duration', 'Trvanie v mesiacoch')
            ->setRequired("Trvanie nesmie byť prázdne!")->setDefaultValue(1);

        $this->addText('date_to', 'Trvanie do (ak je trvanie obmedzené trimestrom)')
                    ->getControlPrototype()->class[] = 'datepicker';

        $this->addSubmit('submit', 'Uložiť');
    }
}