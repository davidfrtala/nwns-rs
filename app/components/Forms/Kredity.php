<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form;

class Kredity extends Form
{
	/**
     * @var Credits
     */
    public $model;
	
    public function __construct($parent, $name, $credits)
    {
    	parent::__construct($parent, $name);
		$this->model = $credits;
		
        $this->addText('nazov', 'Názov')
            ->setRequired("Názov nesmie byť prázdny!");

        $this->addText('kredity', 'Kredity')
            ->setRequired("Kredity nesmú byť prázdne!")->setDefaultValue(0);

        $this->addText('cena', 'Cena')
            ->setRequired("Cena nesmie byť prázdna!");
			
		$credits_types = $this->model->db()->fetchAll("
            SELECT * FROM credits_types
            ");

        $opt = array();

        foreach($credits_types as $type)
        {
            $opt[$type->id] = $type->name;
        }

        $this->addSelect('credits_type_id', 'Typ kreditov', $opt)
            ->setRequired("Typ kreditov nesmie byť prázdny!")
			->setAttribute("id", "creditsType");
			
		$packs = $this->model->db()->fetchAll("
            SELECT id, name FROM packs
            ");

        $opt = array();

        foreach($packs as $pack)
        {
            $opt[$pack->id] = $pack->name;
        }

        $this->addSelect('pack_id', 'Balík', $opt)
			->setAttribute("class", "credits_type")
			->setAttribute("id", "pack_id")
			->addConditionOn($this["credits_type_id"], $this::EQUAL, 3)->setRequired("Musíte zvoliť balík");
			
		$season_tickets = $this->model->db()->fetchAll("
            SELECT id, name FROM season_tickets
            ");

        $opt = array();

        foreach($season_tickets as $ticket)
        {
            $opt[$ticket->id] = $ticket->name;
        }

        $this->addSelect('season_ticket_id', 'Permanentka', $opt)
			->setAttribute("class", "credits_type")
			->setAttribute("id", "season_ticket_id")
			->addConditionOn($this["credits_type_id"], $this::EQUAL, 2)->setRequired("Musíte zvoliť permanentku");
		
		$this->addCheckbox('premium_only', 'Len pre Skalných Skillerov');
		
		$this->addCheckbox('discount', 'Len pre zľavnených');

        $this->addCheckbox('aktivny', 'Aktívny');

        $this->addSubmit('submit', 'Uložiť');//->setValidationScope([$this['nazov'],$this['kredity'],$this['cena'],$this['credits_type_id']]);
    }
}