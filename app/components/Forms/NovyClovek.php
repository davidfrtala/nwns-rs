<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form;

class NovyClovek extends Form
{
		
    public function __construct($parent, $name)
    {
        parent::__construct($parent, $name);

        $this->addText('meno', 'Meno')
            ->setRequired("Meno nesmie byť prázdne!");

        $this->addText('email', 'Email')
            ->setRequired("Email nesmie byť prázdny!");

        $this->addText('vek', 'Vek')
            ->addRule(Form::INTEGER, 'Vek musí byť číslo')
            ->setRequired("Vek nesmie byť prázdny!");

        $this->addSelect('pohlavie', 'Pohlavie', array(
            'm' => 'Muž',
            'z' => 'Žena'
        ));
        $this['pohlavie']->getControlPrototype()->class[] = 'chosen-select chosen-transparent';
        
        $this->addTextArea('dalsie', 'Ďalšie informácie');

        $this->onSuccess[] = [$this, 'submitHandler'];

        $this->addSubmit('submit', 'Chcem byť Nový človek');
    }

    public function submitHandler(Form $form)
    {
        try {
            $values = $form->getValues();

            $temp_pohlavie = array(
                'm' => 'Muž',
                'z' => 'Žena'
            );

            // odoslat email
            $subject = "[NWNS Academy] Nová registrácia na kurz Nový človek";

            $body =  'Bola vytvorená nová prihláška na kurz Nového človeka<br />';

            $body .= 'Meno: '.$values['meno'].'<br/>';
            $body .= 'Email: '.$values['email'].'<br/>';
            $body .= 'Vek: '.$values['vek'].'<br/>';
            $body .= 'Pohlavie: '.$temp_pohlavie[$values['pohlavie']].'<br/>';
            $body .= 'Ďalšie: '.$values['dalsie'].'<br/>';
			
			$mailer = new \App\Models\SendgridEmail($this->getPresenter()->context->parameters['sendGrid']['apiKey'], $this->getPresenter()->context->parameters['sendGrid']['useSendGrid']);
			$mailer->sendEmail('kontakt@nowillnoskill.academy', ['name' => 'NWNS RS', 'email' => 'kontakt@nowillnoskill.academy'], $subject, $body);

            $this->parent->flashMessage('Prihláška bola úspešne odoslaná! Čoskoro ťa budeme kontaktovať', 'success');

            // vycistime formular
            $this->setValues(array(), true);
        } catch(\Exception $e) {
            Debugger::log($e);
            $this->parent->flashMessage('Nepodarilo sa odoslať prihlášku! Opakujte neskôr prosím', 'danger');
        }

        $this->parent->redirect('this');
    }
}