<?php
namespace Components\Forms;

use Nette,
    App\Models\Transakcie,
    Nette\Forms\Controls,
    Tracy\Debugger,
    Nette\Application\UI\Form;

class TraineeRegister extends Form
{

    private $model;

    private $training_profile;

    public function __construct($parent, $name, $training_model, $training_profile)
    {
        parent::__construct($parent, $name);
        $this->model = $training_model;
        $this->training_profile = $training_profile;

        $users = $this->model->db()->fetchAll("
            SELECT * FROM view_user
            where rola = 'Cvičenec' and aktivny = 1
            and platnost_kreditov >= now()
            order by meno
            ");

        $opt = array();

        foreach($users as $row)
        {
            $opt[$row->profile_id] = $row->meno . ' ' . $row->priezvisko . ' (' . $row->login . ')';
        }

        $this->addSelect('training_profile_id', 'Prihlásiť cvičenca', $opt)
            ->setRequired("Vyber cvičenca nesmie byť prázdny!");

        $this->addHidden('id');


        $this->addSubmit('submit', 'Pridať');

        $this->onSuccess[] = [$this, 'submitCallback'];

        \Components\AdminKalendarControl::bootstrapize($this);
    }

    public function submitCallback($form)
    {
        $values = $form->getValues();

        try {
            $this->model->db()->beginTransaction();

            // kontrola či už daný cvičenec neni nahodou prihalaseny
            $check = $this->model->db()->table('trainees')->where(array(
                'training_id' => $values['id'],
                'training_profile_id' => $values['training_profile_id']
            ))->fetch();
            if ($check != false) {
                throw new \Exception("Nieje možné prihlásiť cvičenca na tréning lebo už je prihlásený!");
            }

            // selekty
            $tp = $this->training_profile->find()->wherePrimary($values['training_profile_id']);
            $user_id = $tp->fetch()->user_id;
            $kredity = $this->model->find()->wherePrimary($values['id'])->fetch()->kredity;

            $this->model->db()->table('trainees')->insert(array(
                'training_id' => $values['id'],
                'training_profile_id' => $values['training_profile_id']
            ));

            $this->training_profile->minusKredity($user_id, $kredity);

            $this->training_profile->getTransakcie()->addTransaction($user_id, $values['id'], Transakcie::TRENING_PRIHLASENIE_TID);

            $this->model->db()->commit();
            $this->getPresenter()->flashMessage('Cvičenec bol úspešne pridaný na tréning!', 'success');
        } catch (\Exception $e) {
            Debugger::log($e);
            $this->model->db()->rollBack();

            if ($e instanceof \PDOException) {
                $this->getPresenter()->flashMessage('Nepodarilo sa vykonať akciu! Opakujte neskôr prosím', 'danger');
            } else {
                $this->getPresenter()->flashMessage($e->getMessage(), 'danger');
            }
        }
        $this->getPresenter()->redirect('this');

    }
}