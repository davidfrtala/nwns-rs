<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form;

class TreningType extends Form
{
    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $this->addText('nazov', 'Názov')
            ->setRequired("Názov nesmie byť prázdny!");

        $this->addTextArea('popis', 'Popis')
            ->setRequired("Popis nesmie byť prázdny!");

        $this->addText('color', 'Hex farba');

        $this->addCheckbox('aktivny', 'Aktívny');

        $this->addSubmit('submit', 'Uložiť');
    }
}