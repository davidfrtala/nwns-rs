<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form,
    App\Models;

class EditProfile extends Form
{

    public function __construct($parent, $name)
    {
        parent::__construct($parent, $name);


        $this->addText('meno', 'Meno')
            ->setRequired("Meno nesmie byť prázdne!");

        $this->addText('priezvisko', 'Priezvisko')
            ->setRequired("Priezvisko nesmie byť prázdne!");

        $this->addText('login', 'E-mail')
            ->addRule(Form::EMAIL, 'Neplatný tvar e-mailovej adresy')
            ->setRequired("E-mail nesmie byť prázdny!");

        $this->addText('ulica', 'Ulica')
            ->setRequired("Adresa nesmie byť prázdna!");

        $this->addText('mesto', 'Mesto')
            ->setRequired("Mesto nesmie byť prázdne!");

        $this->addText('psc', 'PSČ')
            ->setRequired("PSČ nesmie byť prázdne!");

        $this->addTextArea('o_mne', 'O mne');
        $this->addText('citat', 'Obľúbený citát');
        $this->addText('citat_autor', 'Autor citátu');

        $this->addSubmit('save', 'Uložiť');
    }

}