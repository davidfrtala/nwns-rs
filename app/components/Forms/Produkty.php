<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form;

class Produkty extends Form
{
    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $this->addCheckbox('aktivny', 'Aktívny');
        
        $this->addText('nazov', 'Názov')
            ->setRequired("Názov nesmie byť prázdny!");
        
        $this->addText('kredity', 'Kredity')
            ->setRequired("Kredity nesmú byť prázdne!");

        $this->addSubmit('submit', 'Uložiť');
    }
}