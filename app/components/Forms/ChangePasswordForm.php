<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form,
    App\Models;

class ChangePassword extends Form
{

    public function __construct($parent, $name)
    {
        parent::__construct($parent, $name);

        $this->addPassword("password", "Heslo")
            ->addRule(Form::FILLED, "Heslo nesmie byť prázdne!");

        $this->addPassword("password2", "Potvrď heslo")
            ->addRule(Form::FILLED, "Potvrdzovacie heslo nesmie byť prázdne!")
            ->addConditionOn($this["password"], Form::FILLED)
            ->addRule(Form::EQUAL, "Hesla se musia zhodovať!", $this["password"]);

        $this->addSubmit('change', 'Zmeniť');
    }

}