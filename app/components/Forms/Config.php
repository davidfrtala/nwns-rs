<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form;

class Config extends Form
{
    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $this->addText('nazov', 'Názov')
            ->setRequired("Názov nesmie byť prázdny!");

        $this->addText('premenna', 'Premenná')
            ->setRequired("Premenná nesmia byť prázdna!");

        $this->addText('hodnota', 'Hodnota')
            ->setRequired("Hodnota nesmia byť prázdna!");

        $this->addCheckbox('locked', 'Locked');

        $this->addSubmit('submit', 'Uložiť');
    }
}