<?php
namespace Components\Forms;

use Nette,
    App\Models\Orders,
    App\Models\TrainingProfile,
    App\Models\Kredity,
    Nette\Forms\Controls,
    Tracy\Debugger,
    Nette\Application\UI\Form;

class AddCashOrder extends Form
{
    const HOTOVOST = 'hotovost';
    /**
     * @var Orders
     */
    public $model;

    /**
     * @var TrainingProfile
     */
    public $training_profile;

    /**
     * @var Kredity
     */
    public $kredity;

    public function __construct($parent, $name, $order, $training_profile, $kredity)
    {
        parent::__construct($parent, $name);
        $this->model = $order;
        $this->training_profile = $training_profile;
        $this->kredity = $kredity;

        $users = $this->model->db()->fetchAll("
            SELECT id, meno, priezvisko, login FROM view_user
            where rola = 'Cvičenec' and aktivny = 1
            order by meno asc
            ");

        $opt = array();

        foreach($users as $row)
        {
            $opt[$row->id] = $row->meno . ' ' . $row->priezvisko . ' (' . $row->login . ')';
        }

        $this->addSelect('user_id', 'Cvičenec', $opt)
            ->setRequired("Výber cvičenca nesmie byť prázdny!");

        $this->addSelect('produkt', 'Kúpiť skilliere', $this->kredity->fetchSkilliers())
            ->setRequired('Produkt je povinný!');

		$this->addText('price', 'Cena')
			->setRequired('Cena musí byť vyplnená');

        $this->addSubmit('submit', 'Pridať');

        $this->onSuccess[] = [$this, 'submitCallback'];

        \Components\AdminKalendarControl::bootstrapize($this);
    }

    public function submitCallback($form)
    {
        $values = $form->getValues();

        // ulozime platbu
        $order = $this->model->checkout($values->user_id, self::HOTOVOST, $values->produkt, $values->price);

        $this->getPresenter()->handleConfirm($order->id);


    }
}