<?php
namespace Components\Forms;

use Nette,
    Nette\Application\UI\Form;

class Packs extends Form
{
	/**
     * @var Pack
     */
    public $model;
	
    public function __construct($parent, $name, $packs)
    {
    	parent::__construct($parent, $name);
		$this->model = $packs;
		
        $this->addText('name', 'Názov')
            ->setRequired("Názov nesmie byť prázdny!");

        $this->addText('training_count', 'Počet tréningov')
            ->setRequired("Počet tréningov nesmie byť prázdny!")->setDefaultValue(36);
			
		$this->addText('start_date', 'Dátum začiatku')
			->setAttribute('class', 'daterange');
			
		$this->addHidden('trainings')
			->setAttribute('id', 'trainings')
			->setDefaultValue("[]");

        $this->addSubmit('submit', 'Uložiť');
    }
}