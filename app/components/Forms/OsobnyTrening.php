<?php
namespace Components\Forms;

use Nette,
    Nette\Forms\Controls,
    Nette\Application\UI\Form,
    App\Models;

class OsobnyTrening extends Form
{
    /**
     * @var Models\OsobneTreningy
     */
    public $model;

    public function __construct($parent, $name, $model)
    {
        parent::__construct($parent, $name);
        $this->model = $model;

        if ($this->getPresenter()->getUser()->isInRole('Administrátor'))
        {

            $users = $this->model->db()->fetchAll("
            SELECT id, meno, priezvisko, login FROM view_user
            where rola = 'Tréner' and aktivny = 1
            order by meno asc
            ");

            $opt = array();

            foreach($users as $row)
            {
                $opt[$row->id] = $row->meno . ' ' . $row->priezvisko . ' (' . $row->login . ')';
            }

            $this->addSelect('trener_profile_id', 'Tréner', $opt)
                ->setRequired("Výber trénera nesmie byť prázdny!");
        }

        $this->addText('datum', 'Dátum')
            ->setRequired("Dátum tréningu nesmie byť prázdny!")
            ->controlPrototype->class[]='datepicker';

        $this->addText('time_od', 'Čas od')
            ->setRequired("Čas od nesmie byť prázdny!");

        $this->addText('time_do', 'Čas do')
            ->setRequired("Čas od nesmie byť prázdny!");

        $this->addText('cvicenec', 'Cvičenec')
            ->setRequired("Vypln poličko s menom cvičenca!");

        $this->addTextArea('komentar', 'Komentár');

        $this->addSubmit('submit', 'Uložiť');
    }
}