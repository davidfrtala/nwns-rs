<?php
namespace Components;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form;

class CategoryForm extends Form
{


    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $this->addText('nazov', 'Názov')
            ->setRequired("Názov nesmie byť prázdny!");
        
        $categories = $this->parent->categories->fetchAll();
        array_unshift($categories, 'Vyberte zo zoznamu');
        
        $this->addSelect('parent_category_id', 'Podkategória', $categories)
            ->setRequired("Vyberte podkategóriu!");
        
        $this->addSubmit('submit', 'Potvrď');
        
        $this->onSuccess[] = [$this, 'galeryFormSubmitted'];
    }

    public function galeryFormSubmitted(Form $form)
    {
        $values = $form->getValues();
        try {
            $new_image = $this->parent->categories->add($values);
            $form->setValues(array(), true);
        } catch (\Exception $e) {
            $code = $e->getCode();
            
            if (!empty($code))
                $this->parent->flashMessage("Nepodarilo sa pridať galériu", "danger");
        }
    }


}