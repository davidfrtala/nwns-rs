<?php
namespace Components;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form;

class LoginForm extends Form
{


    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $this->addText('login', 'Prihlasovacie meno')
            ->setRequired("Login nesmie byť prázdny!");

        $this->addPassword('pass', 'Heslo')
            ->setRequired("Heslo nesmie byť prázdne!");
        $this->addCheckbox('remember', 'Zapamätaj si ma');

        $this->addSubmit('submit', 'Prihlásenie');

        $this->onSuccess[] = [$this, 'logInFormSubmitted'];
    }
    public function logInFormSubmitted(Form $form)
    {
        $values = $form->getHttpData();
        try {
            $this->parent->getUser()->login($values['login'], $values['pass']);
            if (isset($this->parent->backlink))
            {
                $this->parent->restoreRequest($this->parent->backlink);
            }

            if (!empty($values['remember']))
            {
                $this->getPresenter()->rememberMe->remember($values['login']);
            }

            $this->parent->redirect("this");
        } catch (NS\AuthenticationException $e) {
            $code = $e->getCode();
            if (!empty($code)) {
                $this->parent->flashMessage($e->getMessage(), "danger");
            }
        }
    }
}