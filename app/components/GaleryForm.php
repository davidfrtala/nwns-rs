<?php
namespace Components;

use Nette,
    Nette\Security as NS,
    Nette\Application\UI\Form;

class GaleryForm extends Form
{


    public function __construct($parent, $name)
    {
    	parent::__construct($parent, $name);

        $this->addText('nazov', 'Názov')
            ->setRequired("Názov nesmie byť prázdny!");
        
        $this->addSubmit('submit', 'Potvrď');
        
        $this->onSuccess[] = [$this, 'galeryFormSubmitted'];
    }

    public function galeryFormSubmitted(Form $form)
    {
        $values = $form->getValues();
        try {
            $new_image = $this->parent->galeries->add($values);
            $form->setValues(array(), true);
        } catch (\Exception $e) {
            $code = $e->getCode();
            
            if (!empty($code))
                $this->parent->flashMessage("Nepodarilo sa pridať galériu", "danger");
        }
    }


}