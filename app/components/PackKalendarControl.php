<?php

namespace Components;

use Nette\Application\UI,
    Nette\Forms\Controls,
    DateTime,
    DateInterval,
    Tracy\Debugger,
    App\Models;
use Nette\Neon\Exception;

class PackKalendarControl extends UI\Control
{
	/** @var \DateTime */
	public $datetime = NULL;

    /** @persistent */
    public $startdate = NULL;

    /** @var Models\Treningy */
    private $treningy;

    /** @var Models\TrainingProfile */
    private $training_profile;


	public function __construct(Models\Treningy $model, Models\TrainingProfile $training_profile)
	{
		parent::__construct();

        $this->treningy = $model;
        $this->training_profile = $training_profile;

	    $this->datetime = new DateTime();
	}

    private function initTimestamps()
    {
        $week_start = self::getWeekStart($this->datetime);

        $week_end = clone $week_start;
        $week_end = $week_end->add(new DateInterval("P6D"));

        $previous_week = clone $week_start;
        $next_week = clone $week_start;

        $this->template->previous_week  = $previous_week->modify('-1 week');
        $this->template->next_week      = $next_week->modify('+1 week');
        $this->template->range_data = $week_start->format('d.m.Y') . " - ".$week_end->format('d.m.Y');
        $this->template->week_number = (substr($week_end->format('W'), 0, 1) == '0') ? substr($week_end->format('W'), 1) : $week_end->format('W');

        $this->datetime = $week_start;
    }

    /**
     * @return \DateTime
     */
    public function getActualWeek()
    {
        return $this->startdate;
    }

    public static function getWeekStart(DateTime $datetime)
    {
        // podla toho aky je den v tyzdni, o tolko dni sa budeme posuvat dozadu a dostaneme pondelek
        $days = $datetime->format('N') - 1;

        // zaciatok tyzdna (pondelok)
        $week_start = clone $datetime;
        $week_start->modify("-$days days");

        return $week_start;
    }


    public function render()
    {
        $this->template->setFile(__DIR__.'/PackKalendarControl.latte');

        $this->initTimestamps();

        $this->template->training_times = $this->getTrainingTimes();
        $this->template->training_types = $this->getTrainingTypes();

        $this->template->data = $this->treningy->getData($this->datetime, $this->startdate, 7);


        $this->template->render();
    }


    private function getTrainingTimes()
    {
        $times = $this->treningy->db()->fetchAll("
            SELECT tt.* FROM training t
            join training_times tt on tt.id = t.training_times_id
            where termin_od BETWEEN ? and DATE_ADD(?, INTERVAL 7 DAY)
            group by training_times_id
            union
            select * from training_times where aktivny = 1
            order by cas_od", $this->datetime->format('Y-m-d'), $this->datetime->format('Y-m-d'));

        return $times;
    }


    /**
     * @return array|\Nette\Database\IRow[]
     */
    private function getTrainingTypes()
    {
        $types = $this->treningy->db()->fetchAll("
            SELECT tt.* FROM training t
            join training_type tt on tt.id = t.training_type_id
            where termin_od BETWEEN ? and DATE_ADD(?, INTERVAL 7 DAY)
            group by training_type_id
            union
            select * from training_type where aktivny = 1
            ", $this->datetime->format('Y-m-d'), $this->datetime->format('Y-m-d'));

        return $types;
    }


    public function createComponentTreningEdit()
    {
        $control = new TreningEdit($this->treningy, $this->training_profile);

        return $control;
    }

    public function createComponentEmailTrainees($name)
    {
        return new \Components\Forms\EmailTrainees($this, $name, $this->treningy);
    }


    public function handleCancelTraining($training_id)
    {
        try {
            $this->treningy->db()->beginTransaction();

            $this->treningy->cancelTraining($training_id);

            $this->treningy->db()->commit();

            $this->parent->flashMessage('Tréning bol zrušený! Cvicencom boli vrátene skilliere za tréning.', 'success');
            $this->parent->redirect('this');

        } catch (\PDOException $e) {
            Debugger::log($e);
            $this->treningy->db()->rollBack();
            $this->parent->flashMessage('Nepodarilo sa zrušiť tréning! Opakujte neskôr prosím', 'danger');
        }

    }


	public function handleCancelSingleTraining($training_id)
    {
        try {
            $this->treningy->db()->beginTransaction();

            $this->treningy->cancelTraining($training_id, true);

            $this->treningy->db()->commit();

            $this->parent->flashMessage('Tréning bol zrušený! Cvicencom boli vrátene skilliere za tréning.', 'success');
            $this->parent->redirect('this');

        } catch (\PDOException $e) {
            Debugger::log($e);
            $this->treningy->db()->rollBack();
            $this->parent->flashMessage('Nepodarilo sa zrušiť tréning! Opakujte neskôr prosím', 'danger');
        }

    }


    /**
     * Funkcia ktora zamkne kalendar pre navštevnikov rezervačneho systemu. Rezervovat sa da iba ako registrovany user
     */
    public function setReadOnly($status)
    {
    	$this->read_only = (boolean) $status;
    }



    /**
     * Cestujeme v čase
     */
    public function handleChange($startdate)
    {
        $this->startdate = $startdate;
    	$this->datetime = new DateTime($this->startdate);
    }



    public function handleReadTraining($training_id)
    {
        $this->template->show_trening_edit = true;
        $this->getComponent('treningEdit')->setTrainingDefaults($training_id);
    }


    /**
     * Ulozime dany trening na zadany cas
     */
    public function handleSaveNewTraining($day, $time_id, $trening_nazov)
    {
        try {
            $this->treningy->db()->beginTransaction();

            if ($day == '' || $time_id == '' || $trening_nazov == '')
            {
                throw new \Exception('Nesprávne vstupné dáta');
            }
			
			if ($this->startdate == null)
			{
				$this->initTimestamps();
				$this->startdate = $this->datetime->format("Y-m-d");
			}

			$date = date('Y-m-d', strtotime($this->startdate. " + $day days"));
			
			if ($trening_nazov == '...') {
				$exist = $this->treningy->checkTrainingExists($date, $time_id);
				if ($exist != false) {
					$this->treningy->cancelTraining($exist->id);
					$this->presenter->flashMessage('Tréning bol zrušený!', 'success');
				} 
			} else {
				$this->treningy->saveNewTraining($date, $time_id, $trening_nazov);
				$this->presenter->flashMessage('Tréning bol úspešne vytvorený!', 'success');
			}        

            $this->treningy->db()->commit();
        } catch (\Exception $e) {
            Debugger::log($e);
            $this->treningy->db()->rollBack();
            $this->presenter->flashMessage('Nepodarilo sa pridať tréning! Opakujte neskôr prosím', 'danger');
        }
    }


    public function pluralizeSkilliers($count)
    {
		if ( $count == 1 )
        	return $count.' skillier';
		else if (  $count < 5 )
        	return $count.' skilliere';
		else
        	return $count.' skillierov';
    }



    /**
     * Bootstrapizes the form.
     * @param Nette\Application\UI\Form
     * @return void
     */
    public static function bootstrapize($form)
    {
        // setup form rendering
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=col-sm-9';
        $renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
        // make form and controls compatible with Twitter Bootstrap
        $form->getElementPrototype()->class('form-horizontal');
        foreach ($form->getControls() as $control) {
            if ($control instanceof Controls\Button) {
                $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
                $usedPrimary = TRUE;
            } elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox) {
                if ($control instanceof Controls\SelectBox) {
                    $control->getControlPrototype()->addClass('chosen-select chosen-transparent ');
                }
                $control->getControlPrototype()->addClass('form-control');
            } elseif ($control instanceof Controls\Checkbox) {
                $control->getControlPrototype()->addClass('toggle');
                //$control->getSeparatorPrototype()->setName('div')->addClass('checkbox check-transparent');

            } elseif ($control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }
        $form->setRenderer($renderer);
    }
}

