<?php

namespace Components;

use Nette\Application\UI,
    Exception,
    DateTime,
    DateInterval,
    App\Models;

class KalendarControl extends UI\Control
{
	/** deadline na rezervovanie treningu (v minutach) */
	const DEADLINE_PRIHLASENIE = 15;
	/** deadline na odhlasenie z treningu (v hodinach) */
	const DEADLINE_ODHLASENIE = 12;

    /** Kolko tyzdnov viem dopredu pozerat */
    const LIMIT_FUTURE_WEEK = 2;

	/** @var \DateTime */
	public $datetime = NULL;

    /** @persistent */
    public $startdate = NULL;

    /** @var Models\Treningy */
    private $treningy;

	/** @var Models\Profile */
	private $profile;


	public function __construct(Models\Profile $profile, Models\Treningy $model)
	{
		parent::__construct();

        $this->treningy = $model;
		$this->profile = $profile;

	    $this->datetime = new DateTime();
	}


    /**
     * @param $termin
     * @return bool true ak
     */
    public static function checkDeadline(DateTime $termin, $deadline)
    {
        $d = clone $termin;
        return ($d->modify('-'. $deadline . ' hours') < new DateTime()) ? true : false;
    }

    /**
     * Funkcia ktora vrati vypočitany posledny možny týžden, ktorý mozeme v kalendari zobrazit
     *
     * @return \DateTime
     */
    public function getMaxWeekstart()
    {
        $now = new DateTime();
        $i = 7 * self::LIMIT_FUTURE_WEEK;

        $date_weekstart = AdminKalendarControl::getWeekStart($now);
        $date_weekstart->modify("+$i days");

        return $date_weekstart;
    }
    private function initTimestamps()
    {
        // zapametame si aktualny datetime ktory zobrazujeme. Ak je vyssi ako dovoleny limit posuvania
        // vpred, tak ho zastavime na nastavenom limite
        $datetime = ($this->datetime > $this->getMaxWeekstart()) ? $this->getMaxWeekstart() : $this->datetime;

        // podla toho aky je den v tyzdni, o tolko dni sa budeme posuvat dozadu a dostaneme pondelek
        $days = $datetime->format('N') - 1;


        // zaciatok tyzdna (pondelok)
        $week_start = clone $datetime;
        $week_start->modify("-$days days");

        // koniec tyzdna (nedela)
        $week_end = clone $week_start;
        $week_end = $week_end->add(new DateInterval("P6D"));

        $previous_week = clone $week_start;
        $next_week = clone $week_start;

        $this->template->previous_week  = $previous_week->modify('-1 week');
        $this->template->next_week      = $next_week->modify('+1 week');
        $this->template->range_data_week = $week_start->format('d.m.Y') . " - ".$week_end->format('d.m.Y');
        
        if ($this->startdate == null) {
            $today = new DateTime();
            $this->startdate = $today->format('Y-m-d');
        }
        
        
        $firstDayDatetime = new DateTime($this->startdate);
        $this->template->previous_3days = $firstDayDatetime->modify('-3 day')->format('Y-m-d');
        
        $firstDayDatetime = new DateTime($this->startdate);
        $this->template->next_3days = $firstDayDatetime->modify('+3 day')->format('Y-m-d');
        
        $firstDayDatetime = new DateTime($this->startdate);
        $this->template->range_data_3days = $firstDayDatetime->format('d.m.Y') . " - " . $firstDayDatetime->modify('+2 day')->format('d.m.Y');
        
        $firstDayDatetime = new DateTime($this->startdate);
        $this->template->previous_day = $firstDayDatetime->modify('-1 day')->format('Y-m-d');
        
        $firstDayDatetime = new DateTime($this->startdate);
        $this->template->next_day = $firstDayDatetime->modify('+1 day')->format('Y-m-d');
        
        $firstDayDatetime = new DateTime($this->startdate);
        $this->template->range_data_day = $firstDayDatetime->format('d.m.Y');

        $this->datetime = $week_start;
    }



    public function render()
    {
        $this->template->setFile(__DIR__.'/KalendarControl.latte');

        $this->initTimestamps();

        $this->template->training_times = $this->getTrainingTimes();

        $this->template->data = $this->treningy->getData($this->datetime, $this->startdate);
        $this->template->deadline_prihlasenie = self::DEADLINE_PRIHLASENIE;
        $this->template->deadline_odhlasenie = self::DEADLINE_ODHLASENIE;

        $this->template->render();
    }

    private function getTrainingTimes()
    {
        $times = $this->treningy->db()->fetchAll("
            SELECT tt.* FROM training t
            join training_times tt on tt.id = t.training_times_id
            where termin_od BETWEEN ? and DATE_ADD(?, INTERVAL 7 DAY) and tt.aktivny = 1
            group by training_times_id
            union
            select * from training_times where aktivny = 1
            order by cas_od", $this->datetime->format('Y-m-d'), $this->datetime->format('Y-m-d'));

        return $times;
    }


    /**
     * Funkcia ktora zamkne kalendar pre navštevnikov rezervačneho systemu. Rezervovat sa da iba ako registrovany user
     */
    public function setReadOnly($status)
    {
    	$this->read_only = (boolean) $status;
    }



    /**
     * Cestujeme v čase
     */
    public function handleChange($startdate)
    {
        $this->startdate = $startdate;
    	$this->datetime = new DateTime($this->startdate);
    }

    
    public function handleLogin()
    {
        $this->presenter->redirect(':Front:Authenticate:default');
    }

    public function handleReserve($trening_id)
    {
    	try {
    		$this->treningy->reserveTrening($trening_id);

    		$this->presenter->flashMessage("Úspešne si si rezervoval tréning", 'success');
    	} catch (Exception $e) {
    		$this->presenter->flashMessage($e->getMessage(), 'danger');
    	}
    }



    public function handleUnreserve($trening_id)
    {
    	try {
    		$this->treningy->unreserveTrening($trening_id);

    		$this->presenter->flashMessage("Úspešne si sa odhlásil z tréningu", 'success');
    	} catch (Exception $e) {
    		$this->presenter->flashMessage($e->getMessage(), 'danger');
    	}
    }



    public function createLinkLabel(\Nette\Database\Row $trening)
    {
        // ak je trening zruseny
        if ( $trening['zruseny'])
            return 'danger';

        $id = $this->profile->getId();

        $trening_time = new DateTime($trening['termin_od']);
        $diff = $trening_time->diff(new DateTime(date('H:i')));

        // ak sa idem prihlasit na trening
        if (!$trening['prihlaseny_na_trening'])
        {
            // ale mame plno
            if ($trening['cvicencov'] > $trening['max_pocet'])
                return "danger";

            // alebo trening bol vcera
            if (date("Y-m-d") > $trening['termin_od']->format('Y-m-d'))
                return "default";

            // alebo som dnes nestihol deadline na prihlasenie
            if (strtotime($trening['termin_od']) === strtotime('today') && $diff->h < self::DEADLINE_PRIHLASENIE)
                return "default";

            // alebo mam nizsiu uroven nez trening pozaduje
            /*
            $user_level =   $this->profile->table()
                                    ->get($this->profile->getId())
                                        ->ref('skill_level', 'skill_level_id')->level;
            if ($user_level < $trening['level'] && $trening['level'] != '0') {
                return 'default';
            }*/

            // ak niesom prihlaseny
            if (!$this->profile->getId())
                return 'default';


            // ak je vsetko ólrájt, vygenerujeme link
            return 'primary';

        } else {
            if (date("Y-m-d") <= $trening['termin_od'] && $diff->h >= self::DEADLINE_ODHLASENIE)
            {
                // este sa mozem odhlasit
                return 'warning';
            } else {
                // tu uz len zobrazim info ze som prihlaseny
                return 'success';
            }
        }

    	return "default";
    }



    public function pluralizeSkilliers($count)
    {
		if ( $count == 1 )
        	return $count.' skillier';
		else if (  $count < 5 )
        	return $count.' skilliere';
		else
        	return $count.' skillierov';
    }
}

