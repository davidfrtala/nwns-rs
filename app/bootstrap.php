<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/models/GetResponseAPI.php';

$configurator = new Nette\Configurator;


\Tracy\Debugger::$email = 'david.frtala@gmail.com';
\Tracy\Debugger::enable(\Tracy\Debugger::PRODUCTION);
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

// globalny config
$configurator->addConfig(__DIR__ . '/config/config.neon');

// lokalny konfig. Prepisuje globalny konfig. Nemusi sa verziovat do git repozitara
$config_local = __DIR__ . '/config/config.local.neon';
if (file_exists($config_local)) {
	$configurator->addConfig($config_local);
}

$container = $configurator->createContainer();
$container->getService('application')->errorPresenter = "Front:Error";

RadioListExtension::register();
return $container;