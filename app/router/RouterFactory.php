<?php

namespace App;

use Nette,
Nette\Application\Routers\RouteList,
Nette\Application\Routers\Route,
Nette\Application\Routers\SimpleRouter;


/**
 * Router factory.
 */
class RouterFactory
{

	/**
     * @return \Nette\Application\IRouter
     */
	public function createRouter()
	{
		if (function_exists('apache_get_modules') && in_array('mod_rewrite', apache_get_modules())) {
			$router = new RouteList();
			            
            // Admin Module
            $adminRouter =  new RouteList("Admin");
            $adminRouter[] = new Route('admin/<presenter>/<action>[/<id>]', 'Dashboard:default');

            $userRouter =  new RouteList("User");
			$userRouter[] = new Route('nakup', 'Coins:default');
            $userRouter[] = new Route('user/<presenter>/<action>[/<id>]', 'Default:default');

			// Front Module
            $frontRouter = new RouteList("Front");
            $frontRouter[] = new Route('kontakt', 'Homepage:contact');
            $frontRouter[] = new Route('novy-clovek', 'Homepage:novyclovek');
            $frontRouter[] = new Route('o-nas', 'Homepage:aboutus');
            $frontRouter[] = new Route('ponukame', 'Homepage:offer');
			$frontRouter[] = new Route('vyucba', 'Homepage:vyucba');
			$frontRouter[] = new Route('koncept', 'Homepage:koncept');
            $frontRouter[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');

            // Add Routers
            $router[] = $adminRouter;
            $router[] = $userRouter;
            $router[] = $frontRouter;
            
		} else {
			$router = SimpleRouter('Front:Homepage:default');
        }
		return $router;
    }

}
