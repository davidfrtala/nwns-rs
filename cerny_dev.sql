-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Hostiteľ: 127.0.0.1:3320
-- Čas generovania: Pi 24.Jún 2016, 18:05
-- Verzia serveru: 5.5.50-MariaDB-1~wheezy
-- Verzia PHP: 5.6.22-1~dotdeb+7.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `cerny_dev`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `nazov` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `categories`
--

INSERT INTO `categories` VALUES(1, NULL, 'kone');
INSERT INTO `categories` VALUES(2, NULL, 'Soplik');
INSERT INTO `categories` VALUES(3, 1, 'Opilec');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `clanky`
--

CREATE TABLE `clanky` (
  `id` int(11) NOT NULL,
  `nadpis` varchar(45) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(200) DEFAULT NULL,
  `text` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `categories_id` int(11) DEFAULT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `premenna` varchar(20) NOT NULL,
  `nazov` varchar(45) NOT NULL,
  `hodnota` varchar(100) NOT NULL,
  `locked` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `config`
--

INSERT INTO `config` VALUES(1, 'nazov', 'nazov', 'No Will No Skill Academy', 0);
INSERT INTO `config` VALUES(2, 'Test', 'test', 'Hodnota', 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `credits`
--

CREATE TABLE `credits` (
  `id` int(11) NOT NULL,
  `nazov` varchar(45) DEFAULT NULL,
  `kredity` int(11) DEFAULT NULL,
  `cena` int(11) DEFAULT NULL,
  `aktivny` tinyint(4) NOT NULL DEFAULT '1',
  `season_ticket` tinyint(1) NOT NULL DEFAULT '0',
  `season_ticket_duration` int(11) NOT NULL DEFAULT '0',
  `premium_only` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `credits`
--

INSERT INTO `credits` VALUES(1, '50 Skillierov', 50, 20, 0, 0, 0, 0);
INSERT INTO `credits` VALUES(2, '100 Skillierov', 100, 40, 0, 0, 0, 0);
INSERT INTO `credits` VALUES(3, '200 Skillierov', 200, 80, 0, 0, 0, 0);
INSERT INTO `credits` VALUES(4, 'BASIC 50 SC', 50, 25, 0, 0, 0, 0);
INSERT INTO `credits` VALUES(5, 'INTERMEDIATE 100 SC', 100, 50, 0, 0, 0, 0);
INSERT INTO `credits` VALUES(6, 'ADVANCED 200 SC', 200, 100, 0, 0, 0, 0);
INSERT INTO `credits` VALUES(7, 'BASIC 50SC', 50, 30, 1, 0, 0, 0);
INSERT INTO `credits` VALUES(8, 'INTERMEDIATE 100SC', 100, 60, 1, 0, 0, 0);
INSERT INTO `credits` VALUES(9, 'ADVANCED + BONUS (230SC)', 230, 120, 1, 0, 0, 0);
INSERT INTO `credits` VALUES(10, 'INTERMEDIATE + BONUS (110 SC)', 110, 60, 0, 0, 0, 0);
INSERT INTO `credits` VALUES(11, 'ADVANCED + BONUS (240SC)', 240, 120, 0, 0, 0, 0);
INSERT INTO `credits` VALUES(12, 'ADVANCED (200SC) + TRIČKO', 200, 120, 1, 0, 0, 0);
INSERT INTO `credits` VALUES(13, 'PERMANENTKA 1 MESIAC', 0, 50, 1, 1, 1, 0);
INSERT INTO `credits` VALUES(14, 'PREMIUM skuska', 100, 10, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `galeries`
--

CREATE TABLE `galeries` (
  `id` int(11) NOT NULL,
  `nazov` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `galeries`
--

INSERT INTO `galeries` VALUES(1, 'Staré zagrcané spotené slipy');
INSERT INTO `galeries` VALUES(2, 'Ciciak najebaný');
INSERT INTO `galeries` VALUES(3, 'Opité lajná');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `nazov` varchar(30) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  `galery_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `images`
--

INSERT INTO `images` VALUES(1, 'Karolova univerzita', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 1);
INSERT INTO `images` VALUES(2, 'Ovracane staré tričká', '/nwns-rs/www/js/kcfinder/upload/images/sople.jpg', 1);
INSERT INTO `images` VALUES(3, 'Karolova univerzita', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 1);
INSERT INTO `images` VALUES(4, 'Ovracane staré tričká', '/nwns-rs/www/js/kcfinder/upload/images/sople.jpg', 1);
INSERT INTO `images` VALUES(5, 'Karolova univerzita', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 1);
INSERT INTO `images` VALUES(6, 'Ovracane staré tričká', '/nwns-rs/www/js/kcfinder/upload/images/sople.jpg', 1);
INSERT INTO `images` VALUES(7, 'Karolova univerzita', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 1);
INSERT INTO `images` VALUES(8, 'Ovracane staré tričká', '/nwns-rs/www/js/kcfinder/upload/images/sople.jpg', 1);
INSERT INTO `images` VALUES(9, 'Karolova univerzita', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 1);
INSERT INTO `images` VALUES(10, 'Ovracane staré tričká', '/nwns-rs/www/js/kcfinder/upload/images/sople.jpg', 1);
INSERT INTO `images` VALUES(11, 'Karolova univerzita', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 1);
INSERT INTO `images` VALUES(12, 'Ovracane staré tričká', '/nwns-rs/www/js/kcfinder/upload/images/sople.jpg', 1);
INSERT INTO `images` VALUES(13, 'Karolko', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(3).jpg', 3);
INSERT INTO `images` VALUES(14, 'Ovracane staré tričká', '/nwns-rs/www/js/kcfinder/upload/images/%C4%8Di%C4%8Dma%C5%A5.jpg', 3);
INSERT INTO `images` VALUES(15, 'Karolove fety', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(16, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(17, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(18, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(19, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(20, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(21, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(22, 'Kasdlkj', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(3).jpg', 0);
INSERT INTO `images` VALUES(23, 'sadsad', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(24, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(25, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(26, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(27, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);
INSERT INTO `images` VALUES(28, 'asdas', '/nwns-rs/www/js/kcfinder/upload/images/530714_4361183477174_1448271881_n(2).jpg', 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `invoice_detail`
--

CREATE TABLE `invoice_detail` (
  `id` int(11) NOT NULL,
  `meno` varchar(45) NOT NULL,
  `priezvisko` varchar(50) NOT NULL,
  `ulica` varchar(50) NOT NULL,
  `mesto` varchar(50) NOT NULL,
  `psc` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `variabilny_symbol` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `invoice_detail`
--

INSERT INTO `invoice_detail` VALUES(11, 'Marián', 'Černý', 'Koceľova 21', 'Bratislava', 82108, 11, '');
INSERT INTO `invoice_detail` VALUES(1087, 'Jan', 'Hledik', 'Haburská 16', 'Bratislava', 82101, 1088, '');
INSERT INTO `invoice_detail` VALUES(1088, 'Skusko', 'Skuskovic', 'Haburská 16', 'Bratislava', 82101, 1089, '');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `platba` varchar(45) NOT NULL,
  `date_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_confirmed` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `credits_id` int(11) NOT NULL,
  `variabilny_symbol` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `orders`
--

INSERT INTO `orders` VALUES(1, 'prevod', '2016-04-15 12:57:00', '2016-04-15 22:00:00', 1088, 2, '');
INSERT INTO `orders` VALUES(12, 'hotovost', '2016-04-20 13:46:54', '2016-04-20 13:46:54', 1089, 13, '');
INSERT INTO `orders` VALUES(13, 'hotovost', '2016-04-20 13:47:49', '2016-04-20 13:47:49', 1089, 13, '');
INSERT INTO `orders` VALUES(19, 'hotovost', '2016-04-20 13:52:11', '2016-04-20 13:52:11', 1089, 13, '');
INSERT INTO `orders` VALUES(20, 'hotovost', '2016-04-20 13:52:59', '2016-04-20 13:52:59', 1089, 13, '');
INSERT INTO `orders` VALUES(21, 'hotovost', '2016-04-20 13:53:24', '2016-04-20 13:53:24', 1089, 13, '');
INSERT INTO `orders` VALUES(22, 'hotovost', '2016-04-20 13:54:30', '2016-04-20 13:54:30', 1089, 13, '');
INSERT INTO `orders` VALUES(23, 'hotovost', '2016-04-20 14:26:42', '2016-04-20 14:26:42', 1089, 13, '');
INSERT INTO `orders` VALUES(24, 'hotovost', '2016-04-20 14:27:11', '2016-04-20 14:27:11', 1089, 13, '');
INSERT INTO `orders` VALUES(25, 'hotovost', '2016-04-20 20:15:29', '2016-04-20 20:15:29', 1088, 13, '');
INSERT INTO `orders` VALUES(26, 'hotovost', '2016-04-20 20:17:44', '2016-04-20 20:17:44', 1088, 13, '');
INSERT INTO `orders` VALUES(27, 'hotovost', '2016-04-20 20:17:52', '2016-04-20 20:17:52', 1088, 13, '');
INSERT INTO `orders` VALUES(28, 'hotovost', '2016-04-20 20:18:09', NULL, 1088, 13, '');
INSERT INTO `orders` VALUES(29, 'hotovost', '2016-04-20 20:18:36', NULL, 1088, 13, '');
INSERT INTO `orders` VALUES(30, 'hotovost', '2016-04-20 20:22:38', '2016-04-20 20:22:38', 1088, 13, '');
INSERT INTO `orders` VALUES(31, 'hotovost', '2016-04-22 13:05:55', '2016-04-22 13:05:55', 1089, 13, '');
INSERT INTO `orders` VALUES(32, 'hotovost', '2016-04-22 14:42:17', '2016-04-22 14:42:17', 1089, 13, '');
INSERT INTO `orders` VALUES(33, 'hotovost', '2016-04-23 12:44:39', '2016-04-23 12:44:39', 1089, 13, '');
INSERT INTO `orders` VALUES(34, 'hotovost', '2016-05-12 16:23:50', '2016-05-12 16:23:50', 1089, 13, '');
INSERT INTO `orders` VALUES(35, 'hotovost', '2016-05-12 16:34:55', '2016-05-12 16:34:55', 1089, 13, '');
INSERT INTO `orders` VALUES(36, 'hotovost', '2016-05-19 09:04:49', '2016-05-19 09:04:49', 1089, 13, '');
INSERT INTO `orders` VALUES(38, 'hotovost', '2016-05-19 12:57:08', '2016-05-19 12:57:08', 1089, 13, '');
INSERT INTO `orders` VALUES(40, 'prevod', '2016-06-17 19:48:45', NULL, 1089, 14, '20401397');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `personal_training`
--

CREATE TABLE `personal_training` (
  `id` int(11) NOT NULL,
  `trener_profile_id` int(11) NOT NULL,
  `datum` date NOT NULL,
  `time_od` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `time_do` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cvicenec` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `komentar` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `aktivny` tinyint(1) NOT NULL DEFAULT '1',
  `nazov` varchar(45) NOT NULL,
  `kredity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `nazov` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `role`
--

INSERT INTO `role` VALUES(1, 'Administrátor');
INSERT INTO `role` VALUES(2, 'Tréner');
INSERT INTO `role` VALUES(3, 'Cvičenec');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `season_tickets`
--

CREATE TABLE `season_tickets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `valid_until` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `penalties` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

--
-- Sťahujem dáta pre tabuľku `season_tickets`
--

INSERT INTO `season_tickets` VALUES(2, 1088, '2016-05-19 22:00:00', 0);
INSERT INTO `season_tickets` VALUES(4, 1089, '2016-05-19 14:04:34', 4);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `skill_level`
--

CREATE TABLE `skill_level` (
  `id` int(11) NOT NULL,
  `nazov` varchar(45) NOT NULL,
  `level` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `skill_level`
--

INSERT INTO `skill_level` VALUES(1, 'Newbie', '1');
INSERT INTO `skill_level` VALUES(2, 'Nový človek', '2');
INSERT INTO `skill_level` VALUES(3, 'Cvičenec', '3');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `trainees`
--

CREATE TABLE `trainees` (
  `training_id` int(11) NOT NULL,
  `training_profile_id` int(11) NOT NULL,
  `season_ticket_reservation` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `trainees`
--

INSERT INTO `trainees` VALUES(6, 1088, 0);
INSERT INTO `trainees` VALUES(10, 1088, 1);
INSERT INTO `trainees` VALUES(11, 1088, 1);
INSERT INTO `trainees` VALUES(13, 1088, 1);
INSERT INTO `trainees` VALUES(18, 1088, 0);
INSERT INTO `trainees` VALUES(47, 1088, 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `training`
--

CREATE TABLE `training` (
  `id` int(11) NOT NULL,
  `training_type_id` int(11) NOT NULL,
  `max_pocet` int(11) NOT NULL,
  `termin_od` timestamp NULL DEFAULT NULL,
  `termin_do` timestamp NULL DEFAULT NULL,
  `training_times_id` smallint(6) DEFAULT NULL,
  `kredity` int(11) NOT NULL DEFAULT '10',
  `zruseny` tinyint(1) NOT NULL DEFAULT '0',
  `trener_user_id` int(11) NOT NULL,
  `asistant_user_id` int(11) DEFAULT NULL,
  `zrusena_instancia` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `training`
--

INSERT INTO `training` VALUES(1, 7, 10, '2016-04-18 15:30:00', '2016-04-18 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(2, 2, 10, '2016-04-18 16:30:00', '2016-04-18 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(3, 2, 10, '2016-04-19 15:30:00', '2016-04-19 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(4, 28, 10, '2016-04-19 16:30:00', '2016-04-19 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(5, 2, 10, '2016-04-20 15:30:00', '2016-04-20 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(6, 7, 10, '2016-04-23 15:30:00', '2016-04-23 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(7, 2, 10, '2016-04-23 16:30:00', '2016-04-23 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(8, 31, 10, '2016-04-24 15:30:00', '2016-04-24 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(9, 28, 10, '2016-04-24 16:30:00', '2016-04-24 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(10, 28, 10, '2016-05-13 15:30:00', '2016-05-13 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(11, 7, 10, '2016-05-13 16:30:00', '2016-05-13 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(12, 2, 10, '2016-05-14 15:30:00', '2016-05-14 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(13, 31, 10, '2016-05-14 16:30:00', '2016-05-14 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(14, 7, 10, '2016-05-20 15:30:00', '2016-05-20 16:30:00', 21, 10, 1, 11, NULL, 0);
INSERT INTO `training` VALUES(15, 30, 10, '2016-05-20 16:30:00', '2016-05-20 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(16, 30, 10, '2016-05-21 15:30:00', '2016-05-21 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(17, 2, 10, '2016-05-20 15:30:00', '2016-05-20 16:30:00', 21, 10, 1, 11, NULL, 0);
INSERT INTO `training` VALUES(18, 7, 10, '2016-06-05 15:30:00', '2016-06-05 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(19, 30, 10, '2016-06-05 16:30:00', '2016-06-05 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(45, 28, 10, '2016-06-07 15:30:00', '2016-06-07 16:30:00', 21, 10, 1, 11, NULL, 0);
INSERT INTO `training` VALUES(46, 7, 10, '2016-06-12 15:30:00', '2016-06-12 16:30:00', 21, 10, 1, 11, NULL, 1);
INSERT INTO `training` VALUES(47, 30, 10, '2016-06-12 16:30:00', '2016-06-12 17:30:00', 24, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(48, 30, 10, '2016-06-20 15:30:00', '2016-06-20 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(49, 2, 10, '2016-06-21 15:30:00', '2016-06-21 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(50, 2, 10, '2016-06-23 15:30:00', '2016-06-23 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(51, 30, 10, '2016-06-22 15:30:00', '2016-06-22 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(52, 30, 10, '2016-06-24 15:30:00', '2016-06-24 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(53, 30, 10, '2016-06-27 15:30:00', '2016-06-27 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(54, 2, 10, '2016-06-28 15:30:00', '2016-06-28 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(55, 2, 10, '2016-06-30 15:30:00', '2016-06-30 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(56, 30, 10, '2016-06-29 15:30:00', '2016-06-29 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(57, 30, 10, '2016-07-01 15:30:00', '2016-07-01 16:30:00', 21, 10, 0, 11, NULL, 0);
INSERT INTO `training` VALUES(58, 32, 10, '2016-06-20 05:30:00', '2016-06-20 06:30:00', 1, 10, 0, 11, NULL, 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `training_profile`
--

CREATE TABLE `training_profile` (
  `id` int(11) NOT NULL,
  `pocet_kreditov` int(11) NOT NULL DEFAULT '10',
  `platnost_kreditov` timestamp NULL DEFAULT NULL,
  `odporucil` varchar(45) NOT NULL,
  `skill_level_id` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `citat` varchar(255) DEFAULT NULL,
  `citat_autor` varchar(45) DEFAULT NULL,
  `o_mne` text,
  `premium` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `training_profile`
--

INSERT INTO `training_profile` VALUES(11, 350, '2014-07-31 22:00:00', '', 1, 11, 'Bravčové teliesko žije v každom z nás.', 'Al Bravčo', 'Nohy mám do X a žlté zuby. Na xichte jebáky, smrdí mi z huby. ', 0);
INSERT INTO `training_profile` VALUES(1087, 10, '2016-07-19 22:00:00', '', 1, 1088, NULL, NULL, NULL, 0);
INSERT INTO `training_profile` VALUES(1088, -10, '2016-08-18 22:00:00', 'niekto', 1, 1089, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `training_times`
--

CREATE TABLE `training_times` (
  `id` smallint(6) NOT NULL,
  `aktivny` tinyint(4) NOT NULL DEFAULT '1',
  `cas_od` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `cas_do` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `unreserve_deadline` int(2) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Sťahujem dáta pre tabuľku `training_times`
--

INSERT INTO `training_times` VALUES(1, 1, '07:30', '08:30', 0);
INSERT INTO `training_times` VALUES(2, 0, '08:00', '08:50', 0);
INSERT INTO `training_times` VALUES(3, 0, '08:30', '09:20', 0);
INSERT INTO `training_times` VALUES(4, 0, '09:30', '10:50', 0);
INSERT INTO `training_times` VALUES(5, 0, '10:00', '11:00', 0);
INSERT INTO `training_times` VALUES(6, 0, '10:00', '11:20', 0);
INSERT INTO `training_times` VALUES(7, 0, '10:00', '11:30', 0);
INSERT INTO `training_times` VALUES(8, 0, '10:00', '12:00', 0);
INSERT INTO `training_times` VALUES(9, 0, '14:00', '14:50', 0);
INSERT INTO `training_times` VALUES(10, 0, '15:00', '15:50', 0);
INSERT INTO `training_times` VALUES(11, 0, '15:30', '16:20', 0);
INSERT INTO `training_times` VALUES(12, 0, '15:30', '16:30', 0);
INSERT INTO `training_times` VALUES(13, 0, '15:30', '16:50', 0);
INSERT INTO `training_times` VALUES(14, 0, '16:00', '16:50', 0);
INSERT INTO `training_times` VALUES(15, 0, '16:00', '17:00', 0);
INSERT INTO `training_times` VALUES(16, 0, '16:00', '17:20', 0);
INSERT INTO `training_times` VALUES(17, 0, '16:30', '17:20', 0);
INSERT INTO `training_times` VALUES(18, 0, '16:30', '17:50', 0);
INSERT INTO `training_times` VALUES(19, 0, '18:00', '20:00', 0);
INSERT INTO `training_times` VALUES(20, 0, '17:00', '18:20', 0);
INSERT INTO `training_times` VALUES(21, 1, '17:30', '18:30', 8);
INSERT INTO `training_times` VALUES(22, 0, '17:30', '19:20', 0);
INSERT INTO `training_times` VALUES(23, 0, '18:20', '19:20', 0);
INSERT INTO `training_times` VALUES(24, 1, '18:30', '19:30', 4);
INSERT INTO `training_times` VALUES(25, 0, '19:30', '20:20', 0);
INSERT INTO `training_times` VALUES(26, 0, '19:30', '20:30', 0);
INSERT INTO `training_times` VALUES(27, 0, '19:30', '20:50', 0);
INSERT INTO `training_times` VALUES(28, 0, '19:30', '21:00', 0);
INSERT INTO `training_times` VALUES(29, 0, '18:30', '20:30', 0);
INSERT INTO `training_times` VALUES(30, 0, '07:00', '07:50', 0);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `training_type`
--

CREATE TABLE `training_type` (
  `id` int(11) NOT NULL,
  `nazov` varchar(45) NOT NULL,
  `popis` text NOT NULL,
  `skill_level_id` int(11) NOT NULL,
  `color` varchar(6) DEFAULT NULL,
  `aktivny` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `training_type`
--

INSERT INTO `training_type` VALUES(2, 'Movement', 'Movement tréning je o niečo náročnejší na koordináciu a mobilitu ako ostatné. Jeho zámerom je naučiť ťa nové pohyby a tým rozvíjať neuroplasticitu tvojho mozgu. Obsahuje menej drillov a viac plynulých pohybov a hier ako Basics. Ak už máš základy zvládnuté a hľadáš inšpiráciu do tvojho pohybového sveta, Movement je pre teba ako stvorený.', 2, '064E71', 1);
INSERT INTO `training_type` VALUES(3, 'Strength KB', 'Ak chceš nadobudnúť hrubú ruskú silu, tento tréning je pre teba. Venujeme sa tu najmä práci s kettlebellmi, z ktorých si vyberieš vždy ten, ktorý zodpovedá tvojej aktuálnej úrovni sily. Technika je u nás na prvom mieste a nič ako kipping, skipping, cheating, alebo iné nekontrolované pohyby tu neuvidíš. Kettlebell ti nič neodpustí. ', 3, '990000', 0);
INSERT INTO `training_type` VALUES(4, 'Muay Thai', 'Toto tradičné thajské bojové umenie je dnes jedným z najpopulárnejších bojových umení na svete. Je tiež známe ako umenie ôsmich končatín, keďže využíva údery päsťami a lakťami, kopy nohou a kolenom. Ak sa chceš stať tvrdým bojovníkom, skús Muay Thai.', 2, 'FAC0C4', 0);
INSERT INTO `training_type` VALUES(6, 'Fit-N-Sexy (Ž)', 'Ženy to chcú tiež a u nás v Akadémii to dostanú. Hovoríme o vysoko intenzívnom tréningu, ktorý ti pomôže rýchlo spáliť tuk a esteticky vyformovať postavu. Ide o oveľa efektívnejší spôsob cvičenia ako bežné stereotypické kardio, a...', 1, '975cc1', 0);
INSERT INTO `training_type` VALUES(7, 'Beast Mode', 'Sila, výbušnosť, dynamika. To sú kľúčové slová tréningu Beast Mode, ktorý je ako stvorený pre nakopnutie tvojho testosterónu na maximum. Okrem silového tréningu s vlastným telom sa priprav na intenzívnu záťaž v podobe intervalového tréningu, zápasenia z kolien, či pretláčania na rôzne spôsoby. Ak si chceš poriadne zamakať, príď na Beast Mode a prebuď v sebe spiacu beštiu.', 3, 'e03c52', 1);
INSERT INTO `training_type` VALUES(8, 'Capoeira', 'Capoeira je brazílske tanečno-akrobatické bojové umenie. Je neskutočne estetické a ide skôr o umenie ako o boj. Vyžaduje si obrovskú dávku koordinácie, flexibility a v neposlednom rade aj sily. Ak sa chceš v týchto ohľadoch zlepšiť a nau...', 2, '666666', 0);
INSERT INTO `training_type` VALUES(9, 'Hatha Yoga', 'Chceš sa uvoľniť a na chvíľu sa vymaniť z každodenného stresu moderného sveta? Príď s nami posilniť svoje telo aj ducha na tréning Jogy. Hatha Yoga spája fyzické cvičenie (asana), prácu s dychom (pranayama) a meditáciu pre maximálnu fy...', 2, '666666', 0);
INSERT INTO `training_type` VALUES(10, 'Daj sa do Pohybu', 'Pohyb lieči. Či už ťa trápia skrátené svaly, boľavé šľachy, či kĺby, alebo chceš zlepšiť svoju koordináciu, či rovnováhu, tento tréning je pre teba. Naučíme ťa lepšie sa hýbať a venujeme veľkú pozornosť mobilizačným a kompe...', 1, '38a4d6', 0);
INSERT INTO `training_type` VALUES(11, 'Open Gym', 'Voľný tréning, počas ktorého máš k dispozícií všetko náradie a náčinie v Akadémií.', 2, '666666', 0);
INSERT INTO `training_type` VALUES(12, 'Desatoro Moderného Pračloveka', 'Na workshope s názvom Desatoro Prikázaní Moderného Pračloveka sa dozvieš čo sú najdôležitejšie aspekty životosprávy, ktorým by si mal/a venovať zvýšenú pozornosť, ak chceš dosiahnuť svoje vysnívané zdravé a vyrysované telo. Či u...', 1, '666666', 0);
INSERT INTO `training_type` VALUES(13, 'Ako Získať Nadľudskú Silu', 'Planche, front/back lever, vlajka, či muscle up sú základnou výbavou každého správneho workoutera. Sú to naozaj efektné prvky, ktoré sa vie naučiť každý, no niekedy to trvá dlhšie ako by si chcel.\r \r Príď na workshop s názvom \\&quot;Ak...', 2, '666666', 0);
INSERT INTO `training_type` VALUES(14, 'Workshop: Daj sa Dokopy', 'Sila rastie, no bolesti pribúdajú? Máš problémy s kĺbami, šlachami, či úponmi?\r \r Príď na workshop s názvom Daj sa Dokopy už túto nedeľu o 10:00 v No Will No Skill Academy. Workshop vedie osobný tréner Viktor Kozubek.\r \r Viktor ti vysvet...', 3, '666666', 0);
INSERT INTO `training_type` VALUES(15, 'Daj sa Dokopy Zvnútra', 'Snažíš sa zdravo stravovať, poctivo a pravidelne trénuješ, konzumuješ množstvo zeleniny a leješ do seba litre farebných super-štiav, no stále máš zdravotné problémy, alergie, málo energie, výkyvy nálady, chuť na nezdravé jedlo a z tu...', 1, '666666', 0);
INSERT INTO `training_type` VALUES(16, 'Ako Sa Zbaviť Parazitov', 'Vedel  si, že v tomto momente sa v tvojom tele nachádza niekoľko rôznych druhov parazitov?\n \n Tieto parazity oslabujú tvoje telo, ničia ti orgány, pojedajú vitamíny a minerály, ktoré prijmeš a spôsobujú rôzne zdravotné problémy od alergi...', 1, '666666', 0);
INSERT INTO `training_type` VALUES(17, 'Acro Yoga', 'Vieš lietať? A chceš sa naučiť? Acro yoga sa nazýva aj umenie lietania. Ide o párovú akrobatickú formu jogy, kde je veľmi dôležitá súhra medzi partnermi. Výborne rozvíja tvoju koncentráciu, silu a flexibilitu a je vhodná aj pre začiato...', 2, '666666', 0);
INSERT INTO `training_type` VALUES(18, 'Movement (Basic)', 'Alebo základy pohybu. Ak sa chceš naučiť hýbať bezbolestne a s ľahkosťou od úplných základov, toto je ten správny tréning pre teba. Naučíme ťa ako správne dýchať a zlepšiť vnímanie svojho tela. Popracujeme na tvojej mobilite a pohybovej inteligencií pomocou pohybových hier. Odstrániš bolesti, zlepšíš pohybový rozsah svojich kĺbov a vybuduješ si základ pre ďalší silový a pohybový rozvoj. Tréning je vhodný aj pre starších a pre ľudí so sedavým zamestnaním.', 2, 'defaul', 0);
INSERT INTO `training_type` VALUES(19, 'Awareness', 'Množstvo vonkajších podnetov, hluk a pracovný stres často spôsobujú, že postupne strácame citlivosť našich zmyslov. Tréningy sú zamerané na zlepšenie schopnosti sústrediť sa ako aj na zvýšenie efektivity, koordinácie a elegancie vášho pohybu. Cieľom je dosahovať pohybové ciele s ľahkosťou a bezpečne. ', 2, '191946', 0);
INSERT INTO `training_type` VALUES(20, 'Mobility', 'Na tréningoch mobility, ktorá je druhým pilierom nášho pohybového konceptu, sa zaoberáme hlavne zlepšovaním funkcie chrbtice a zväčšovaním pohybového rozsahu bedier, ramien, členkov, zápästí a posilňovaniu svalov v krajných polohách. Zmobilizuj sa s nami, zbav sa bolestí a objav nové možnosti pohybu!', 2, '3366FF', 0);
INSERT INTO `training_type` VALUES(21, 'Balance', 'Bez toho, aby si vnímal a ovládal svoje telo v priestore, budeš v nekonečnom vesmíre pohybu iba tápať. Preto je rovnováha tretím pilierom nášho pohybového konceptu. Rovnováha nie je len schopnosť udržať sa v stojke alebo lastovičke, je to aj dynamická práca so svojím ťažiskom, nevyhnutná pre všetky druhy pohybu, či už je to parkour, bojové umenia alebo rôzne športy.', 2, '4D94B8', 0);
INSERT INTO `training_type` VALUES(23, 'Strength W', 'Silné ženy sú sexi. Zabudni na diéty, odopieranie si jedla a nudné kardio cvičenia v posilke. Ak ťa nebavia stroje a hopsanie do rytmu po dlhé hodiny, no chceš si vybudovať krásne krivky formou zábavných cvičení, toto je ten správny tréning pre teba. ', 1, '975cc1', 0);
INSERT INTO `training_type` VALUES(24, 'Strength 1', 'Sila je základ akéhokoľvek pohybu, od obyčajného behu až po náročné gymnastické kúsky. Zahŕňa prácu s vlastným telom aj s voľnými váhami. Na tréningoch kladieme dôraz na vybudovanie kvalitného silového základu, ktorý vám umožní hýbať sa s ľahkosťou. ', 1, 'e03c52', 0);
INSERT INTO `training_type` VALUES(25, 'Strength 2', 'Sila je základ akéhokoľvek pohybu, od obyčajného behu až po náročné gymnastické kúsky. Zahŕňa prácu s vlastným telom aj s voľnými váhami. Na tréningoch kladieme dôraz na vybudovanie kvalitného silového základu, ktorý vám umožní hýbať sa s ľahkosťou. ', 1, '990000', 0);
INSERT INTO `training_type` VALUES(26, 'Dynamics', 'Dynamická časť pohybu zahŕňa výskoky, zdolávanie prekážok, rýchlu orientáciu v priestore alebo balistické pohybové drily, ktoré pripravia vaše telo na reálnu záťaž.  Čím lepšie máš zvládnuté predchádzajúce pohybové piliere, tým sa v dynamických pohyboch budeš cítiť istejšie a bezpečnejšie.', 1, '', 0);
INSERT INTO `training_type` VALUES(27, 'Nový Človek', 'Tento tréning je rezervovaný pre účastníkov pohybového kurzu Nový Človek. Viac o kurze tu.', 1, '', 0);
INSERT INTO `training_type` VALUES(28, 'Movement Games', 'Movement Jam je spoločenská akcia pre tých, ktorí sa radšej ako vysedávaním v bare bavia pohybom. Program Jamu spočíva v predĺženom 80 minútovom vedenom tréningu zameranom na určitú oblasť pohybu (tentokrát na pohybové hry) a následnom voľnom tréningu / diskusií všetkých zúčastnených. Príď sa s nami zabaviť a rozšíriť svoje obzory v kolektíve nadšencov pohybu.', 1, '38a4d6', 1);
INSERT INTO `training_type` VALUES(29, 'MetCon', 'Metabolic Conditioning, alebo po našom metabolický tréning. V našom ponímaní to je komplexný silovo-dynamický tréning, v ktorom sa spája sila, koordinácia a výbušnosť. Na rad sa dostanú skoky, údery, hody, ťahanie, tlačenie, plazenie ale aj práca so železom. Prebuď v sebe spiacu beštiu a príď si s nami poriadne zamakať.', 1, '', 0);
INSERT INTO `training_type` VALUES(30, 'Basics', 'Hlavným zámerom tréningu je vybudovať základy pre tvoj ďalší pohybový rozvoj na ostatných tréningoch. Ak potrebuješ popracovať na svojej mobilite, základných pohybových vzoroch či vnímaní svojho tela, Basics je ten správny tréning pre teba.', 1, 'FAC0C4', 1);
INSERT INTO `training_type` VALUES(31, 'Calisthenics', 'Kalistenika je silový tréning s vlastným telom a jednoduchými nástrojmi ako sú gymnastické kruhy. Pracujeme na ňom takmer výlučne bez závaží. Zámer tohto tréningu je zlepšiť tvoju relatívnu silu, ktorú využiješ aj v reálnom živote mimo telocvične.  Ak chceš budovať silu a získať kontrolu nad svojim telom aj v náročných polohách, Calisthenics je tréning pre teba.', 1, '191946', 1);
INSERT INTO `training_type` VALUES(32, 'Animals', 'popis', 1, '123456', 1);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `target_id` int(11) NOT NULL,
  `transaction_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `transactions`
--

INSERT INTO `transactions` VALUES(1, '2015-05-13 06:58:55', 2, 3, 11);
INSERT INTO `transactions` VALUES(2, '2015-05-13 06:59:49', 1092, 1, 11);
INSERT INTO `transactions` VALUES(3, '2015-05-13 07:00:22', 1092, 2, 11);
INSERT INTO `transactions` VALUES(4, '2016-04-20 13:46:54', 13, 3, 1089);
INSERT INTO `transactions` VALUES(5, '2016-04-20 13:47:49', 13, 3, 1089);
INSERT INTO `transactions` VALUES(6, '2016-04-20 13:52:11', 13, 3, 1089);
INSERT INTO `transactions` VALUES(7, '2016-04-20 13:52:59', 13, 3, 1089);
INSERT INTO `transactions` VALUES(8, '2016-04-20 13:53:24', 13, 3, 1089);
INSERT INTO `transactions` VALUES(9, '2016-04-20 13:54:30', 13, 3, 1089);
INSERT INTO `transactions` VALUES(10, '2016-04-20 14:26:42', 13, 3, 1089);
INSERT INTO `transactions` VALUES(11, '2016-04-20 14:27:11', 13, 3, 1089);
INSERT INTO `transactions` VALUES(12, '2016-04-20 20:15:29', 13, 3, 1088);
INSERT INTO `transactions` VALUES(13, '2016-04-20 20:17:44', 13, 3, 1088);
INSERT INTO `transactions` VALUES(14, '2016-04-20 20:17:52', 13, 3, 1088);
INSERT INTO `transactions` VALUES(15, '2016-04-20 20:22:38', 13, 3, 1088);
INSERT INTO `transactions` VALUES(16, '2016-04-22 12:07:05', 6, 1, 1089);
INSERT INTO `transactions` VALUES(17, '2016-04-22 12:53:30', 6, 1, 1089);
INSERT INTO `transactions` VALUES(18, '2016-04-22 12:54:19', 6, 2, 1089);
INSERT INTO `transactions` VALUES(19, '2016-04-22 12:54:23', 6, 1, 1089);
INSERT INTO `transactions` VALUES(20, '2016-04-22 12:59:27', 6, 2, 1089);
INSERT INTO `transactions` VALUES(21, '2016-04-22 12:59:34', 6, 1, 1089);
INSERT INTO `transactions` VALUES(22, '2016-04-22 12:59:40', 6, 2, 1089);
INSERT INTO `transactions` VALUES(23, '2016-04-22 13:01:16', 6, 1, 1089);
INSERT INTO `transactions` VALUES(24, '2016-04-22 13:01:22', 6, 2, 1089);
INSERT INTO `transactions` VALUES(25, '2016-04-22 13:01:40', 6, 1, 1089);
INSERT INTO `transactions` VALUES(26, '2016-04-22 13:01:44', 6, 2, 1089);
INSERT INTO `transactions` VALUES(27, '2016-04-22 13:05:55', 13, 3, 1089);
INSERT INTO `transactions` VALUES(28, '2016-04-22 13:06:26', 6, 1, 1089);
INSERT INTO `transactions` VALUES(29, '2016-04-22 13:06:37', 6, 2, 1089);
INSERT INTO `transactions` VALUES(30, '2016-04-22 13:11:38', 6, 1, 1089);
INSERT INTO `transactions` VALUES(31, '2016-04-22 13:11:54', 6, 2, 1089);
INSERT INTO `transactions` VALUES(32, '2016-04-22 13:12:00', 6, 1, 1089);
INSERT INTO `transactions` VALUES(33, '2016-04-22 13:12:27', 6, 2, 1089);
INSERT INTO `transactions` VALUES(34, '2016-04-22 13:12:31', 6, 1, 1089);
INSERT INTO `transactions` VALUES(35, '2016-04-22 13:13:23', 6, 2, 1089);
INSERT INTO `transactions` VALUES(36, '2016-04-22 13:14:41', 6, 1, 1089);
INSERT INTO `transactions` VALUES(37, '2016-04-22 13:14:49', 6, 2, 1089);
INSERT INTO `transactions` VALUES(38, '2016-04-22 13:15:04', 6, 1, 1089);
INSERT INTO `transactions` VALUES(39, '2016-04-22 13:15:11', 6, 2, 1089);
INSERT INTO `transactions` VALUES(40, '2016-04-22 13:17:55', 6, 1, 1089);
INSERT INTO `transactions` VALUES(41, '2016-04-22 13:18:01', 7, 1, 1089);
INSERT INTO `transactions` VALUES(42, '2016-04-22 13:21:19', 7, 2, 1089);
INSERT INTO `transactions` VALUES(43, '2016-04-22 13:29:37', 7, 1, 1089);
INSERT INTO `transactions` VALUES(44, '2016-04-22 13:33:46', 8, 1, 1089);
INSERT INTO `transactions` VALUES(45, '2016-04-22 13:35:22', 8, 2, 1089);
INSERT INTO `transactions` VALUES(46, '2016-04-22 13:35:26', 9, 1, 1089);
INSERT INTO `transactions` VALUES(47, '2016-04-22 13:35:30', 7, 2, 1089);
INSERT INTO `transactions` VALUES(48, '2016-04-22 13:35:34', 8, 1, 1089);
INSERT INTO `transactions` VALUES(49, '2016-04-22 13:36:14', 8, 2, 1089);
INSERT INTO `transactions` VALUES(50, '2016-04-22 13:36:18', 7, 1, 1089);
INSERT INTO `transactions` VALUES(53, '2016-04-22 14:39:42', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(54, '2016-04-22 14:42:17', 13, 3, 1089);
INSERT INTO `transactions` VALUES(55, '2016-04-22 14:42:47', 9, 2, 1089);
INSERT INTO `transactions` VALUES(56, '2016-04-22 14:42:50', 7, 2, 1089);
INSERT INTO `transactions` VALUES(57, '2016-04-22 14:42:55', 6, 2, 1089);
INSERT INTO `transactions` VALUES(58, '2016-04-23 12:36:41', 6, 1, 1089);
INSERT INTO `transactions` VALUES(59, '2016-04-23 12:44:39', 13, 3, 1089);
INSERT INTO `transactions` VALUES(60, '2016-04-23 12:47:05', 8, 1, 1089);
INSERT INTO `transactions` VALUES(61, '2016-04-23 12:47:32', 8, 2, 1089);
INSERT INTO `transactions` VALUES(62, '2016-05-12 16:23:50', 13, 3, 1089);
INSERT INTO `transactions` VALUES(63, '2016-05-12 16:26:03', 10, 1, 1089);
INSERT INTO `transactions` VALUES(64, '2016-05-12 16:26:11', 11, 1, 1089);
INSERT INTO `transactions` VALUES(65, '2016-05-12 16:26:14', 12, 1, 1089);
INSERT INTO `transactions` VALUES(66, '2016-05-12 16:26:27', 10, 2, 1089);
INSERT INTO `transactions` VALUES(67, '2016-05-12 16:26:30', 13, 1, 1089);
INSERT INTO `transactions` VALUES(68, '2016-05-12 16:28:40', 12, 2, 1089);
INSERT INTO `transactions` VALUES(69, '2016-05-12 16:28:50', 10, 1, 1089);
INSERT INTO `transactions` VALUES(70, '2016-05-12 16:28:56', 10, 2, 1089);
INSERT INTO `transactions` VALUES(71, '2016-05-12 16:29:01', 12, 1, 1089);
INSERT INTO `transactions` VALUES(72, '2016-05-12 16:29:06', 12, 2, 1089);
INSERT INTO `transactions` VALUES(73, '2016-05-12 16:29:09', 10, 1, 1089);
INSERT INTO `transactions` VALUES(74, '2016-05-12 16:29:13', 12, 1, 1089);
INSERT INTO `transactions` VALUES(75, '2016-05-12 16:29:18', 12, 2, 1089);
INSERT INTO `transactions` VALUES(76, '2016-05-12 16:29:47', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(77, '2016-05-12 16:30:29', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(78, '2016-05-12 16:30:57', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(79, '2016-05-12 16:34:55', 13, 3, 1089);
INSERT INTO `transactions` VALUES(80, '2016-05-19 09:04:49', 13, 3, 1089);
INSERT INTO `transactions` VALUES(81, '2016-05-19 12:32:11', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(82, '2016-05-19 12:57:08', 13, 3, 1089);
INSERT INTO `transactions` VALUES(83, '2016-05-19 14:03:39', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(84, '2016-05-19 14:03:50', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(85, '2016-05-19 14:04:19', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(86, '2016-05-19 14:04:34', 1089, 5, 1089);
INSERT INTO `transactions` VALUES(87, '2016-06-05 08:39:02', 18, 1, 1089);
INSERT INTO `transactions` VALUES(88, '2016-06-21 17:20:16', 47, 1, 1089);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `transaction_type`
--

CREATE TABLE `transaction_type` (
  `id` int(11) NOT NULL,
  `nazov` varchar(45) NOT NULL,
  `target_table` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `transaction_type`
--

INSERT INTO `transaction_type` VALUES(1, 'Prihlásenie na tréning', 'training');
INSERT INTO `transaction_type` VALUES(2, 'Odhlásenie sa z tréningu', 'training');
INSERT INTO `transaction_type` VALUES(3, 'Nákup skillierov', 'credits');
INSERT INTO `transaction_type` VALUES(4, 'Dodatočné prihlásenie na tréning', 'training');
INSERT INTO `transaction_type` VALUES(5, 'Pridanie červeného bodu', 'training');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `aktivny` tinyint(1) NOT NULL DEFAULT '0',
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `token` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sťahujem dáta pre tabuľku `user`
--

INSERT INTO `user` VALUES(1, 1, 1, 'admin', 'd7c52b8eaf803e114031314d63905d4f', NULL, '2015-03-23 20:49:11', '2015-03-22 23:00:00');
INSERT INTO `user` VALUES(11, 2, 1, 'marian@nowillnoskillacademy.com', 'fa658a245b4d5d2b88f6e51dff1e8ffd', '{"user":"marian@nowillnoskillacademy.com","token":"59ba12de8d4cb3b48e234c760e82c77e8037b91ab47d6c4e43054f3b1a2ed4cb","signature":"978447566bfbde372932d75c8326b8d935f3a59740254288bab441ba84db3bbef66d"}', '2014-06-10 19:06:28', NULL);
INSERT INTO `user` VALUES(1088, 3, 1, 'jan.hledik@gmail.com', '260527d57dab6307b805c5e46647ee7e', NULL, '2016-04-13 18:44:31', NULL);
INSERT INTO `user` VALUES(1089, 3, 1, 'hledik.jan@gmail.com', '260527d57dab6307b805c5e46647ee7e', '{"user":"hledik.jan@gmail.com","token":"0e689d544e023fd5ce6339ce2b84590c60e88b300708e620253684be1eccf310","signature":"37fb9dec76f272bb6c085c09a795fd80aac473fef4b38491a74c5ae828b260169f04"}', '2016-05-20 12:31:59', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_orders`
-- (See below for the actual view)
--
CREATE TABLE `view_orders` (
`id` int(11)
,`user_id` int(11)
,`uzivatel` varchar(96)
,`variabilny_symbol` varchar(8)
,`credits_id` int(11)
,`date_order` timestamp
,`platba` varchar(45)
,`nazov` varchar(45)
,`kredity` int(11)
,`season_ticket` tinyint(1)
,`season_ticket_duration` int(11)
,`date_confirmed` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_personal_training`
-- (See below for the actual view)
--
CREATE TABLE `view_personal_training` (
`id` int(11)
,`trener_profile_id` int(11)
,`datum` date
,`time_od` varchar(45)
,`time_do` varchar(45)
,`cvicenec` varchar(100)
,`komentar` text
,`trener` varchar(96)
,`login` varchar(45)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_trainees`
-- (See below for the actual view)
--
CREATE TABLE `view_trainees` (
`training_id` int(11)
,`cvicencov` bigint(21)
,`cvicenci` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_training`
-- (See below for the actual view)
--
CREATE TABLE `view_training` (
`id` int(11)
,`nazov` varchar(45)
,`kredity` int(11)
,`level` varchar(45)
,`uroven` varchar(45)
,`popis` text
,`color` varchar(6)
,`max_pocet` int(11)
,`cvicencov` bigint(21)
,`cvicenci` text
,`termin_od` datetime
,`termin_do` datetime
,`datum_od` timestamp
,`datum_do` timestamp
,`cas_od` varchar(5)
,`cas_do` varchar(5)
,`training_type_id` int(11)
,`training_times_id` smallint(6)
,`unreserve_deadline` int(2)
,`trener` varchar(96)
,`trener_email` varchar(45)
,`trener_user_id` int(11)
,`asistent` varchar(96)
,`asistent_email` varchar(45)
,`asistant_user_id` int(11)
,`zruseny` tinyint(1)
,`zrusena_instancia` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_training_type`
-- (See below for the actual view)
--
CREATE TABLE `view_training_type` (
`id` int(11)
,`nazov` varchar(45)
,`level` varchar(45)
,`uroven` varchar(45)
,`popis` text
,`color` varchar(6)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_transactions`
-- (See below for the actual view)
--
CREATE TABLE `view_transactions` (
`id` int(11)
,`uzivatel` varchar(96)
,`nazov` varchar(45)
,`target_table` varchar(45)
,`target_id` int(11)
,`transaction_type_id` int(11)
,`user_id` int(11)
,`date_created` timestamp
,`polozka` varchar(45)
,`cena` varchar(15)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_user`
-- (See below for the actual view)
--
CREATE TABLE `view_user` (
`id` int(11)
,`login` varchar(45)
,`aktivny` tinyint(1)
,`date_created` timestamp
,`last_login` timestamp
,`rola` varchar(45)
,`invoice_id` int(11)
,`meno` varchar(45)
,`priezvisko` varchar(50)
,`ulica` varchar(50)
,`mesto` varchar(50)
,`psc` int(11)
,`profile_id` int(11)
,`pocet_kreditov` int(11)
,`platnost_kreditov` timestamp
,`o_mne` text
,`citat` varchar(255)
,`citat_autor` varchar(45)
,`odporucil` varchar(45)
,`skill` varchar(45)
,`season_ticket_id` int(11)
,`season_ticket_valid_until` timestamp
,`season_ticket_penalties` tinyint(4)
,`premium` tinyint(1)
);

-- --------------------------------------------------------

--
-- Štruktúra pre zobrazenie `view_orders`
--
DROP TABLE IF EXISTS `view_orders`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cerny_dev`@`localhost` SQL SECURITY DEFINER VIEW `view_orders`  AS  select `o`.`id` AS `id`,`o`.`user_id` AS `user_id`,concat(`u`.`meno`,' ',`u`.`priezvisko`) AS `uzivatel`,`o`.`variabilny_symbol` AS `variabilny_symbol`,`o`.`credits_id` AS `credits_id`,`o`.`date_order` AS `date_order`,`o`.`platba` AS `platba`,`c`.`nazov` AS `nazov`,`c`.`kredity` AS `kredity`,`c`.`season_ticket` AS `season_ticket`,`c`.`season_ticket_duration` AS `season_ticket_duration`,`o`.`date_confirmed` AS `date_confirmed` from ((`orders` `o` left join `view_user` `u` on((`u`.`id` = `o`.`user_id`))) join `credits` `c` on((`c`.`id` = `o`.`credits_id`))) ;

-- --------------------------------------------------------

--
-- Štruktúra pre zobrazenie `view_personal_training`
--
DROP TABLE IF EXISTS `view_personal_training`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cerny_dev`@`localhost` SQL SECURITY DEFINER VIEW `view_personal_training`  AS  select `pt`.`id` AS `id`,`pt`.`trener_profile_id` AS `trener_profile_id`,`pt`.`datum` AS `datum`,`pt`.`time_od` AS `time_od`,`pt`.`time_do` AS `time_do`,`pt`.`cvicenec` AS `cvicenec`,`pt`.`komentar` AS `komentar`,concat(`u`.`meno`,' ',`u`.`priezvisko`) AS `trener`,`u`.`login` AS `login` from (`personal_training` `pt` join `view_user` `u` on((`u`.`id` = `pt`.`trener_profile_id`))) order by `pt`.`datum` desc,`pt`.`time_od` desc ;

-- --------------------------------------------------------

--
-- Štruktúra pre zobrazenie `view_trainees`
--
DROP TABLE IF EXISTS `view_trainees`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cerny_dev`@`localhost` SQL SECURITY DEFINER VIEW `view_trainees`  AS  select `tc`.`training_id` AS `training_id`,count(`i`.`user_id`) AS `cvicencov`,group_concat(concat(convert(if((`tp`.`premium` = 1),'<i title="Skalný Skiller" class="fa fa-shield"></i> ','') using utf8),`i`.`meno`,' ',`i`.`priezvisko`) separator ', ') AS `cvicenci` from ((`trainees` `tc` join `training_profile` `tp` on((`tp`.`id` = `tc`.`training_profile_id`))) join `invoice_detail` `i` on((`i`.`user_id` = `tp`.`user_id`))) group by `tc`.`training_id` ;

-- --------------------------------------------------------

--
-- Štruktúra pre zobrazenie `view_training`
--
DROP TABLE IF EXISTS `view_training`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cerny_dev`@`localhost` SQL SECURITY DEFINER VIEW `view_training`  AS  select `t`.`id` AS `id`,`tt`.`nazov` AS `nazov`,`t`.`kredity` AS `kredity`,`tt`.`level` AS `level`,`tt`.`uroven` AS `uroven`,`tt`.`popis` AS `popis`,`tt`.`color` AS `color`,`t`.`max_pocet` AS `max_pocet`,`ts`.`cvicencov` AS `cvicencov`,`ts`.`cvicenci` AS `cvicenci`,cast(concat(cast(`t`.`termin_od` as date),' ',cast((case when isnull(`ttime`.`cas_od`) then '00:00:00' else `ttime`.`cas_od` end) as time)) as datetime) AS `termin_od`,cast(concat(cast(`t`.`termin_do` as date),' ',cast((case when isnull(`ttime`.`cas_do`) then '00:00:00' else `ttime`.`cas_do` end) as time)) as datetime) AS `termin_do`,`t`.`termin_od` AS `datum_od`,`t`.`termin_do` AS `datum_do`,`ttime`.`cas_od` AS `cas_od`,`ttime`.`cas_do` AS `cas_do`,`t`.`training_type_id` AS `training_type_id`,`t`.`training_times_id` AS `training_times_id`,`ttime`.`unreserve_deadline` AS `unreserve_deadline`,concat(`u`.`meno`,' ',`u`.`priezvisko`) AS `trener`,`ut`.`login` AS `trener_email`,`u`.`user_id` AS `trener_user_id`,concat(`ua`.`meno`,' ',`ua`.`priezvisko`) AS `asistent`,`uat`.`login` AS `asistent_email`,`ua`.`user_id` AS `asistant_user_id`,`t`.`zruseny` AS `zruseny`,`t`.`zrusena_instancia` AS `zrusena_instancia` from (((((((`training` `t` join `view_training_type` `tt` on((`tt`.`id` = `t`.`training_type_id`))) left join `training_times` `ttime` on((`ttime`.`id` = `t`.`training_times_id`))) left join `view_trainees` `ts` on((`ts`.`training_id` = `t`.`id`))) join `invoice_detail` `u` on((`u`.`user_id` = `t`.`trener_user_id`))) join `user` `ut` on((`ut`.`id` = `t`.`trener_user_id`))) left join `user` `uat` on((`uat`.`id` = `t`.`asistant_user_id`))) left join `invoice_detail` `ua` on((`ua`.`id` = `t`.`asistant_user_id`))) order by `t`.`termin_od`,hour(`t`.`termin_od`) ;

-- --------------------------------------------------------

--
-- Štruktúra pre zobrazenie `view_training_type`
--
DROP TABLE IF EXISTS `view_training_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cerny_dev`@`localhost` SQL SECURITY DEFINER VIEW `view_training_type`  AS  select `t`.`id` AS `id`,`t`.`nazov` AS `nazov`,`l`.`level` AS `level`,`l`.`nazov` AS `uroven`,`t`.`popis` AS `popis`,`t`.`color` AS `color` from (`training_type` `t` join `skill_level` `l` on((`l`.`id` = `t`.`skill_level_id`))) ;

-- --------------------------------------------------------

--
-- Štruktúra pre zobrazenie `view_transactions`
--
DROP TABLE IF EXISTS `view_transactions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cerny_dev`@`localhost` SQL SECURITY DEFINER VIEW `view_transactions`  AS  select `t`.`id` AS `id`,concat(`u`.`meno`,' ',`u`.`priezvisko`) AS `uzivatel`,`tp`.`nazov` AS `nazov`,`tp`.`target_table` AS `target_table`,`t`.`target_id` AS `target_id`,`t`.`transaction_type_id` AS `transaction_type_id`,`t`.`user_id` AS `user_id`,`t`.`date_created` AS `date_created`,(case when (`tp`.`target_table` = 'training') then `tr`.`nazov` when (`tp`.`target_table` = 'credits') then `c`.`nazov` else NULL end) AS `polozka`,(case when (`tp`.`target_table` = 'training') then concat(`tr`.`kredity`,' SC') when (`tp`.`target_table` = 'credits') then concat(`c`.`cena`,' EUR') else NULL end) AS `cena` from ((((`transactions` `t` join `transaction_type` `tp` on((`tp`.`id` = `t`.`transaction_type_id`))) left join `view_training` `tr` on((`tr`.`id` = `t`.`target_id`))) left join `view_user` `u` on((`u`.`id` = `t`.`user_id`))) left join `credits` `c` on((`c`.`id` = `t`.`target_id`))) ;

-- --------------------------------------------------------

--
-- Štruktúra pre zobrazenie `view_user`
--
DROP TABLE IF EXISTS `view_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`cerny_dev`@`localhost` SQL SECURITY DEFINER VIEW `view_user`  AS  select `u`.`id` AS `id`,`u`.`login` AS `login`,`u`.`aktivny` AS `aktivny`,`u`.`date_created` AS `date_created`,`u`.`last_login` AS `last_login`,`r`.`nazov` AS `rola`,`i`.`id` AS `invoice_id`,`i`.`meno` AS `meno`,`i`.`priezvisko` AS `priezvisko`,`i`.`ulica` AS `ulica`,`i`.`mesto` AS `mesto`,`i`.`psc` AS `psc`,`p`.`id` AS `profile_id`,`p`.`pocet_kreditov` AS `pocet_kreditov`,`p`.`platnost_kreditov` AS `platnost_kreditov`,`p`.`o_mne` AS `o_mne`,`p`.`citat` AS `citat`,`p`.`citat_autor` AS `citat_autor`,`p`.`odporucil` AS `odporucil`,`s`.`nazov` AS `skill`,`st`.`id` AS `season_ticket_id`,`st`.`valid_until` AS `season_ticket_valid_until`,`st`.`penalties` AS `season_ticket_penalties`,`p`.`premium` AS `premium` from (((((`user` `u` join `role` `r` on((`r`.`id` = `u`.`role_id`))) left join `invoice_detail` `i` on((`i`.`user_id` = `u`.`id`))) left join `training_profile` `p` on((`p`.`user_id` = `u`.`id`))) left join `skill_level` `s` on((`s`.`id` = `p`.`skill_level_id`))) left join `season_tickets` `st` on((`st`.`user_id` = `u`.`id`))) ;

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_categories_categories1_idx` (`parent_category_id`);

--
-- Indexy pre tabuľku `clanky`
--
ALTER TABLE `clanky`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_clanky_user1_idx` (`user_id`),
  ADD KEY `fk_clanky_categories1_idx` (`categories_id`);

--
-- Indexy pre tabuľku `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `credits`
--
ALTER TABLE `credits`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `galeries`
--
ALTER TABLE `galeries`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `invoice_detail`
--
ALTER TABLE `invoice_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexy pre tabuľku `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_orders_user1_idx` (`user_id`),
  ADD KEY `fk_orders_credits1_idx` (`credits_id`);

--
-- Indexy pre tabuľku `personal_training`
--
ALTER TABLE `personal_training`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trener_idx` (`trener_profile_id`);

--
-- Indexy pre tabuľku `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `season_tickets`
--
ALTER TABLE `season_tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_season_tickets_user_Idx` (`user_id`);

--
-- Indexy pre tabuľku `skill_level`
--
ALTER TABLE `skill_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `trainees`
--
ALTER TABLE `trainees`
  ADD PRIMARY KEY (`training_id`,`training_profile_id`),
  ADD KEY `fk_training_has_training_profile_training_profile1_idx` (`training_profile_id`),
  ADD KEY `fk_training_has_training_profile_training1_idx` (`training_id`);

--
-- Indexy pre tabuľku `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_training_training_type1_idx` (`training_type_id`),
  ADD KEY `fk_training_user1_idx` (`trener_user_id`),
  ADD KEY `fk_training_user1_idx1` (`asistant_user_id`),
  ADD KEY `training_times_id` (`training_times_id`);

--
-- Indexy pre tabuľku `training_profile`
--
ALTER TABLE `training_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_training_profile_skill_level1_idx` (`skill_level_id`),
  ADD KEY `fk_training_profile_user1_idx` (`user_id`);

--
-- Indexy pre tabuľku `training_times`
--
ALTER TABLE `training_times`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `training_type`
--
ALTER TABLE `training_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_training_type_skill_level1_idx` (`skill_level_id`);

--
-- Indexy pre tabuľku `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transactions_transacation_type1_idx` (`transaction_type_id`),
  ADD KEY `fk_transactions_user1_idx` (`user_id`);

--
-- Indexy pre tabuľku `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pre tabuľku `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_role1_idx` (`role_id`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pre tabuľku `clanky`
--
ALTER TABLE `clanky`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pre tabuľku `credits`
--
ALTER TABLE `credits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pre tabuľku `galeries`
--
ALTER TABLE `galeries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pre tabuľku `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pre tabuľku `invoice_detail`
--
ALTER TABLE `invoice_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1089;
--
-- AUTO_INCREMENT pre tabuľku `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pre tabuľku `personal_training`
--
ALTER TABLE `personal_training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pre tabuľku `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pre tabuľku `season_tickets`
--
ALTER TABLE `season_tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pre tabuľku `skill_level`
--
ALTER TABLE `skill_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pre tabuľku `training`
--
ALTER TABLE `training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT pre tabuľku `training_profile`
--
ALTER TABLE `training_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1089;
--
-- AUTO_INCREMENT pre tabuľku `training_times`
--
ALTER TABLE `training_times`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pre tabuľku `training_type`
--
ALTER TABLE `training_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pre tabuľku `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT pre tabuľku `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pre tabuľku `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1090;
--
-- Obmedzenie pre exportované tabuľky
--

--
-- Obmedzenie pre tabuľku `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `fk_categories_categories1` FOREIGN KEY (`parent_category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `clanky`
--
ALTER TABLE `clanky`
  ADD CONSTRAINT `fk_clanky_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_clanky_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `invoice_detail`
--
ALTER TABLE `invoice_detail`
  ADD CONSTRAINT `fk_invoice_detail_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_orders_credits1` FOREIGN KEY (`credits_id`) REFERENCES `credits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_orders_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `personal_training`
--
ALTER TABLE `personal_training`
  ADD CONSTRAINT `trener` FOREIGN KEY (`trener_profile_id`) REFERENCES `training_profile` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `season_tickets`
--
ALTER TABLE `season_tickets`
  ADD CONSTRAINT `fk_season_tickets_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `trainees`
--
ALTER TABLE `trainees`
  ADD CONSTRAINT `fk_training_has_training_profile_training1` FOREIGN KEY (`training_id`) REFERENCES `training` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_training_has_training_profile_training_profile1` FOREIGN KEY (`training_profile_id`) REFERENCES `training_profile` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `training`
--
ALTER TABLE `training`
  ADD CONSTRAINT `fk_training_asistant_user1` FOREIGN KEY (`asistant_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_training_training_type1` FOREIGN KEY (`training_type_id`) REFERENCES `training_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_training_trener_user1` FOREIGN KEY (`trener_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `training_profile`
--
ALTER TABLE `training_profile`
  ADD CONSTRAINT `fk_training_profile_skill_level1` FOREIGN KEY (`skill_level_id`) REFERENCES `skill_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_training_profile_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `training_type`
--
ALTER TABLE `training_type`
  ADD CONSTRAINT `fk_training_type_skill_level1` FOREIGN KEY (`skill_level_id`) REFERENCES `skill_level` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Obmedzenie pre tabuľku `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `fk_transactions_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_transaction_type1` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`);

--
-- Obmedzenie pre tabuľku `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
