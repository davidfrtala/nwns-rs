/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'sk';
	// config.uiColor = '#AADC6E';
        config.filebrowserBrowseUrl = '../js/kcfinder/browse.php?opener=ckeditor&type=files&lang=sk';
        config.filebrowserImageBrowseUrl = '../js/kcfinder/browse.php?opener=ckeditor&type=images&lang=sk';
        config.filebrowserFlashBrowseUrl = '../js/kcfinder/browse.php?opener=ckeditor&type=flash&lang=sk';
        config.filebrowserUploadUrl = '../js/kcfinder/upload.php?opener=ckeditor&type=files&lang=sk';
        config.filebrowserImageUploadUrl = '../js/kcfinder/upload.php?opener=ckeditor&type=images&lang=sk';
        config.filebrowserFlashUploadUrl = '../js/kcfinder/upload.php?opener=ckeditor&type=flash&lang=sk';
};
