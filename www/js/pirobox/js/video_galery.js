$(document).ready(function () {
    $.get(
       "https://www.googleapis.com/youtube/v3/channels", {
           part: 'contentDetails',
           forUsername: 'NoWillNoSkill',
           key: 'AIzaSyAOOSMcKCjEz9MvjkCtDCivb7mhQ3nLANY',
       },
       function (data) {
           $.each(data.items, function (i, item) {
               pid = item.contentDetails.relatedPlaylists.uploads;
               getVids(pid);
           });
       }
    );

    function getVids(pid) {
        $.get(
            "https://www.googleapis.com/youtube/v3/playlistItems", {
                part: 'snippet',
                maxResults: 50,
                playlistId: pid,
                key: 'AIzaSyAOOSMcKCjEz9MvjkCtDCivb7mhQ3nLANY'
            },
            function (data) {
                $.each(data.items, function (i, item) {
                    var elem = '<div class="superbox-list img-view yt-video">';
                    elem += '<a href="http://www.youtube-nocookie.com/v/' + item.snippet.resourceId.videoId + '" class="pirobox_gall1" rel="iframe-640-505"><div class="superbox-list-overlay"><i class="fa fa-play-circle"></i></div><img alt="' + item.snippet.title + '" src="' + item.snippet.thumbnails.high.url + '" class="galery_photo" data-img="' + item.snippet.thumbnails.high.url + '"></a>';
                    elem += '</div>';
                    $("#gallery-container").append(elem);
                });
            }
        ).done(function () {
            $('#gallery-container a.pirobox_gall1').piroBox_ext({
                piro_speed: 700,
                bg_alpha: 0.5,
                piro_scroll: true // pirobox always positioned at the center of the page
            });
        });
    }
});
